$files = Get-ChildItem ap*.*
$filearray = @()
foreach ($file in $files) {
    $text = Get-Content $file
    foreach ($line in $text) {
        if (($line -match "CashinInfo") -or ($line -match "CashoutInfo")) {
            $linearray = $line.split(" ",".",",",";",":","[","]","-")
            if ($linearray[5].Equals("INF1")) {
                [int]$sum=0
                for ($i=0;$i -lt $linearray.count;$i++) {
                    if ($linearray[$i].Equals("810")) {
                            $sum+=($linearray[$i + 1]/100) * $linearray[$i + 2]
                    }
                }
                if ($sum -eq 0) {Write-Host $line -fore Red}
                $linehash=[ordered]@{
                    Date=$linearray[0];
                    Time=("{0}:{1}:{2}" -f $linearray[1],$linearray[2],$linearray[3]);
                    OperationType=$linearray[15];
                    Sum=$sum;
                    Card=$linearray[-3]
                    CashPeriod="";
                }
                $filearray+=New-Object PSObject -Property $linehash
            }
        } elseif (($line -match "ttc=9:") -or ($line -match "ttc=10:")) {
            $line >> $null 
            #.\dispense.txt
            $linearray = $line.split(" ",".",",",";",":") 
            $linehash=[ordered]@{
                Date=$linearray[0];
                Time=("{0}:{1}:{2}" -f $linearray[1],$linearray[2],$linearray[3]);
                OperationType="Incas";
                Sum="";
                CashPeriod=("{0} {1}" -f $linearray[14],$linearray[8]);
            }
            $filearray+=New-Object PSObject -Property $linehash
        } else {}
    }   
}
return $filearray