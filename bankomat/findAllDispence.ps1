$files = Get-ChildItem ap*.*
$filearray = @()
foreach ($file in $files) {
    $text = Get-Content $file
    foreach ($line in $text) {
        if ($line -match "DispenserStatus") {
            $line >> $null 
            #.\dispense.txt
            $linearray = $line.split(" ",".",",",";",":") 
            $linehash=[ordered]@{
                Date=$linearray[0];
                Time=("{0}:{1}:{2}" -f $linearray[1],$linearray[2],$linearray[3]);
                Cas1ClientAsk=$linearray[12];
                Cas2ClientAsk=$linearray[14];
                Cas3ClientAsk=$linearray[16];
                Cas4ClientAsk=$linearray[18];
                ExtractFromCas1=$linearray[21];
                ExtractFromCas2=$linearray[25];
                ExtractFromCas3=$linearray[29];
                ExtractFromCas4=$linearray[33];
                DroppedToRejectCas1=$linearray[22];
                DroppedToRejectCas2=$linearray[26];
                DroppedToRejectCas3=$linearray[30];
                DroppedToRejectCas4=$linearray[34];
                ListsInReject=$linearray[37];
                CashPeriod="";
            }
            $filearray+=New-Object PSObject -Property $linehash
        } elseif (($line -match "ttc=9:") -or ($line -match "ttc=10:")) {
            $line >> $null 
            #.\dispense.txt
            $linearray = $line.split(" ",".",",",";",":") 
            $linehash=[ordered]@{
                Date=$linearray[0];
                Time=("{0}:{1}:{2}" -f $linearray[1],$linearray[2],$linearray[3]);
                Cas1ClientAsk="";
                Cas2ClientAsk="";
                Cas3ClientAsk="";
                Cas4ClientAsk="";
                ExtractFromCas1="";
                ExtractFromCas2="";
                ExtractFromCas3="";
                ExtractFromCas4="";
                DroppedToRejectCas1="";
                DroppedToRejectCas2="";
                DroppedToRejectCas3="";
                DroppedToRejectCas4="";
                ListsInReject="";
                CashPeriod=("{0} {1}" -f $linearray[14],$linearray[8]);
            }
            $filearray+=New-Object PSObject -Property $linehash
        } else {}
    }   
}
return $filearray