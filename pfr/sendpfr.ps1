$workdir = "C:\Work\exchange\OUT"
$userEmail="pfr_exchange@sovbank.ru"
$notifReceiverEmail = "_pfr_ex_notif@sovbank.ru"
set-location $workdir
$year = get-date -f yyyy
$month = get-date -f MM
$day = get-date -f dd
$utildir = "$workdir\work"
$tempdir = "$workdir\temp"
$currYearDir = "$workdir\ARC\$year"
$currArcDir = "$workdir\ARC\$year\$month\$day"
$currErrDir = "$workdir\ERR\$year\$month\$day"
$currLogFile = ($currArcDir+"\log.txt")
$fileIDsCSV = ($utildir+"\TypeIDs.csv")
$script:loggingState = $false
$spbDepPfrId = "202"
$ourPfrId = "838"

#������� ������������ ID ������ � ��������� ����������� �������.
$script:packetIDHash = @{} 
Import-CSV $fileIDsCSV | foreach {$PacketIDHash[$_.FileID]=$_.PacketChar}

$filesRegExpMask = "OUT-700-Y-\p{Nd}{4}-ORG-\p{Nd}{3}-\p{Nd}{3}-\p{Nd}{6}-DIS-\p{Nd}{3}-DCK-\p{Nd}{5}-\p{Nd}{3}-DOC-\p{Ll}{4}-FSB-\p{Nd}{4}-OUTNMB-\p{Nd}{10}.XML"

#��������� ������������:
# OUT-700-Y-     9999-ORG-      999-      999-   999999-DIS-      999-DCK-    99999-      999-DOC-     ����-FSB-     9999-OUTNMB-9999999999.XML
# OUT-700-Y-\p{Nd}{4}-ORG-\p{Nd}{3}-\p{Nd}{3}-\p{Nd}{6}-DIS-\p{Nd}{3}-DCK-\p{Nd}{5}-\p{Nd}{3}-DOC-\p{Ll}{4}-FSB-\p{Nd}{4}-OUTNMB-\p{Nd}{10}.XML
#��� ������� � ������� ����� �����, �������� �� �9� ��� �ջ, �������� �����������.
#��������� ����� �����, ������������ � ������� �������������������� �������� �9�,
#������ ����� ��������� � ������� ����� ��������.

$packetRegExpMask = '^'+$spbDepPfrId+'\p{Ll}\p{Nd}{4}\.'+$ourPfrId+'$'

#������������ ������������� ������ ��� �������� �� ����� ����� �������� ��������� �������:
# XXXaNNNN.YYY
#���:
#XXX � ������ ���������, ����������� ����� � ������� ������������ ����������������;
#YYY � ������ ���������, ����������� ����� � ������� ������������ ����������������;
#a � ������� zip-����� (��� ������ � ����������� ������ �a� ����������� ������ �p�);
#NNNN � ���������������� ����������  ����� ������������� ������; 
# '       XXX     a      NNNN.      YYY '
# '^\p{Nd}{3}\p{Ll}\p{Nd}{4}\.\p{Nd}{3}$'

function Get-ArcName($packetLetter, $packetId) {
    return "{0}{1}{2:D4}.{3}" -f $spbDepPfrId,$packetLetter,$packetId,$ourPfrId
}

function script:Copy-Arc($sourceName, $destination)
{
    $destination+="\"+$sourceName
    if ((Test-Path $destination) -and ((Get-FileHash $sourceName).hash -ne (Get-FileHash $destination).hash))
        {
            Copy-Arc $sourceName $destination".new"
        } elseif (-not (Test-Path $destination)){
            Copy-Item -Path $sourceName -Destination $destination
        } else {
            Write-Output "���� ��� ��� ��������� � ����� �����"
        }
}

function script:GetPacketId($packetLetter) {
    $idsFile = ($currYearDir+"\"+$packetLetter+"id.txt")
    if (Test-Path $idsFile) {
        [int]$lastId = Get-Content $idsFile -last 1
        $curId = $lastId+1
        Add-Content $idsFile $curId
        return $curId
    } else {
        $curId = 1
        #����� ��������� ������� ������� � return, Out-Null ��������� ��� �������������� ������ ���������
        New-Item $idsFile -ItemType file | Out-Null
        Add-Content $idsFile $curId
        return $curId
    }
}

#������� �������� ���������� ��������, ���� ��� �� ����������.
#���� ������� ������������ ��������� �������, ����������� � ������������
#��� ��������, ��� �������������� ������ ���������� ������ �������
function script:Create-TempDir {
if (-not (Test-Path $tempdir)) {
    Write-Output "�������� ���������� ��� ��������� ������"
    New-Item $tempdir -ItemType Directory |Out-Null
} else {
    $popupText = "������� ��������� ���������� ���������� ��������� ������ `
������� �������� ��������� �����������. ������ ����� ����� �������."
    Write-Output $popupText
    $ans = 1
    switch ($ans) {
        1 {
            Write-Output "�������� ��������� ���������� ����������� ��������"
            Remove-Item $tempdir -Recurse -Force
            Write-Output "�������� ��������� ���������� ��� �������� ��������"
            New-Item $tempdir -ItemType Directory |Out-Null
        }
        2 {
            Write-Output "����������� ���������� �������"
            exit 3
        }
    }  
}    
}

function script:New-ArcFolder($arcDirPath) {
    if (-not (Test-Path $arcDirPath)) {
    Write-Output "�������� ���������� ������ �� ����������� ����"
    New-Item $arcDirPath -ItemType Directory |Out-Null
    }
}

#begin Script
Set-Location $workdir
if (Test-Path *.*) {
    Start-Wait -s 5
} else {
    exit
}
Create-TempDir
   
################### �������� ������ ������� �� �������� #####################



Move-Item *.* $tempdir
Set-Location $tempdir
$typeHash=@{}
$files=Get-ChildItem -file | Where-Object {$_ -match $filesRegExpMask}

foreach ($file in $files) {
    $fileID=($file.Name).Substring(60,4)
    if ($packetIDHash.Contains($fileID)) {
        $key = [string]$packetIDHash[$fileID][0]
        $fileType = [string]$packetIDHash[$fileID][1]
        if ($fileType -eq "f") {continue}
        if (-not $typeHash.Contains($key)) {
            $typeHash[$key]=New-Object PsObject -Property @{PacketID=GetPacketId($key);Table=[System.Collections.ArrayList]@()}
        }
        switch ($key) {
            "p" {
                $typeHash[$key].Table.Add($file) | Out-Null
            }
            "a" {
                [System.Collections.ArrayList]$opFileNamesList = @()
                [xml]$regXml = Get-Content $file
                $fileNamesList = $regXml.�������.������������������������.�����_������������_������_���_����������.����������������.��������
                $fileNamesListRep = $regXml.�������.������������������������.�����_������������_������_���_��������.����������������.��������
                #��� ����� ����� ��� ��� � ������ 
                #������ ��� XML ��� � OPVF - "�����_������������_������_���_����������"
                #� �                  OPVZ - "�����_������������_������_���_��������"
                #��� ���� � �� � ������ ����� ����� � ������� "XXXaNNNN.YYY"
                $fileNamesList += $fileNamesListRep
                $err = 0
                foreach ($filename in $fileNamesList) {
                    if ($files.Name -contains $filename) {
                        $opFileNamesList.Add($filename) | Out-Null
                    } else {
                        $err+=1
                    }
                }
                if ($err -eq 0) {
                    $typeHash[$key].Table.Add($file) | Out-Null
                    foreach ($fileName in $opFileNamesList) {
                        $typeHash[$key].Table.Add((Get-ChildItem $fileName)) | Out-Null
                    }
                }
            }
        }
    }
}

$typeHash.GetEnumerator() | foreach {
    $arcName = Get-ArcName $_.Key $_.Value.PacketID
    Compress-Archive $_.Value.Table.Name "$arcName.zip" 
    Get-ChildItem -filter "*.zip" | Rename-Item -NewName {$_.Name -replace '.zip',''}
    New-ArcFolder $currArcDir
    Copy-Arc $arcName $currArcDir
    Move-Item $arcName $workdir
    Remove-Item $_.Value.Table
}

if (Test-Path $tempdir\*.*) {
    New-ArcFolder $currErrDir
    $popupText = "������� ����� �� ��������������� �������, ���������� �� �����"
    Write-Host $popupText
    Set-Location $tempdir
    $mess=Get-ChildItem
    foreach ($mes in $mess) {
        Write-Host $mes.name "����������� � �������������"
        Copy-Arc $mes.name $currErrDir
        Remove-Item $mes
        }
    $strmess= $mess -join "`n"
    $body = "������ ����!`n`n������� ����� �� ��������������� �������, ���������� �� �����:`n`n"+$strmess+`
    "`n`n ���������� � \\812-VVIPNET01\pfrexchange\OUT\ERR "
    $priority = "High"
    Send-MailMessage -To $notifReceiverEmail `
    -from $userEmail `
    -priority $priority `
    -Subject "�������� � ���: ���������� �������������� ��� �������� ������ �� ����������" `
    -Body $body `
    -SmtpServer caslb.sovbank.ru `
    -Encoding UTF8
    Set-Location $workdir

}

Write-Output "������ �������� ������ ��� �������� ���������"

Set-Location $workdir
Remove-Item $tempdir
#end