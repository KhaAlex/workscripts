$workdir = "C:\Work\exchange\IN"
$userEmail = "pfr_exchange@sovbank.ru"
$notifReceiverEmail = "_pfr_ex_notif@sovbank.ru"
set-location $workdir
$year = get-date -f yyyy
$month = get-date -f MM
$day = get-date -f dd
$utildir = "$workdir\work"
$tempdir = "$workdir\temp"
$currArcDir = "$workdir\ARC\$year\$month\$day"
$currErrDir = "$workdir\ERR\$year\$month\$day"

$archMask = "838*.202"

#�������� ������������(������� ��������� ��� ������������ � ���������� ����������):
# PFR-700-Y-     9999-ORG-      999-      999-   999999-DIS-      999-DCK-    99999-      999-DOC-     ����-FSB-     9999.XML
# PFR-700-Y-\p{Nd}{4}-ORG-\p{Nd}{3}-\p{Nd}{3}-\p{Nd}{6}-DIS-\p{Nd}{3}-DCK-\p{Nd}{5}-\p{Nd}{3}-DOC-\p{Ll}{4}-FSB-\p{Nd}{4}.XML'
#��������� ������������:
# OUT-700-Y-     9999-ORG-      999-      999-   999999-DIS-      999-DCK-    99999-      999-DOC-     ����-FSB-     9999-OUTNMB-9999999999.XML
# OUT-700-Y-\p{Nd}{4}-ORG-\p{Nd}{3}-\p{Nd}{3}-\p{Nd}{6}-DIS-\p{Nd}{3}-DCK-\p{Nd}{5}-\p{Nd}{3}-DOC-\p{Ll}{4}-FSB-\p{Nd}{4}-OUTNMB-\p{Nd}{10}.XML
#��� ������� � ������� ����� �����, �������� �� �9� ��� �ջ, �������� �����������.
#��������� ����� �����, ������������ � ������� �������������������� �������� �9�,
#������ ����� ��������� � ������� ����� ��������.

#������� ����������� � �����. ���� ����(����������� �� ����) ��� ���� � ������,
#������ �� ����������, ���� ��� �� ����������, ���� ���� ���� � ��� �� ������
#�� ������ ����� ��������� .new
function script:Copy-Arc($source, $destination)
{
    if ((Test-Path $destination) -and ((Get-FileHash $source).hash -ne (Get-FileHash $destination).hash))
        {
            Copy-Arc $source $destination".new"
        } elseif (-not (Test-Path $destination)){
            Copy-Item -Path $source -Destination $destination
        } else {
            Write-Host "���� ��� ��� ��������� � ����� �����"
        }
}

#������� �������� ���������� ��������, ���� ��� �� ����������.
#���� ������� ������������ ��������� �������, ����������� � ������������
#��� ��������, ��� �������������� ������ ���������� ������ �������
function script:Create-TempDir {
if (-not (Test-Path $tempdir)) {
    Write-Host "�������� ���������� ��� ��������� ������"
    New-Item $tempdir -ItemType Directory |Out-Null
} else {
    $popupText = "������� ��������� ���������� ���������� ��������� ������ `
������� �������� ��������� �����������. ������ ����� ����� �������."
    Write-Host $popupText
    $ans = 1
    switch ($ans) {
        1 {
            Write-Host "�������� ��������� ���������� ����������� ��������"
            Remove-Item $tempdir -Recurse -Force
            Write-Host "�������� ��������� ���������� ��� �������� ��������"
            New-Item $tempdir -ItemType Directory |Out-Null
        }
        2 {
            Write-Host "����������� ���������� �������"
            exit 3
        }
    }  
}    
}

function script:New-ArcFolder($arcDirPath) {
    if (-not (Test-Path $arcDirPath)) {
    Write-Host "�������� ���������� ������ �� ����������� ����"
    New-Item $arcDirPath -ItemType Directory |Out-Null
    }
}

#������� �������� ��� ����� � ��������������� ��������� $mask �� $workdir
#�� ��������� ����������, �������� ����� � �������� �����,
#����� ������������� �� ���� �� ����������� 7z
#� ����� ������� �������� ����� ������ ��������������� $mask
#���������� $true ���� � $work ���� $mask ����� $false
function script:Receive-7zPackage($mask) {
    Set-Location $workdir
    if (Test-Path $mask) {
        New-ArcFolder $currArcDir
        Write-Host "������� �������� ������� �� ���"
        $script:mess = Get-ChildItem $mask 
        foreach ($mes in $mess) {
            Write-Host $mes.name "����������� � �����"
            $dest= $currArcDir+"\"+$mes.name
            Copy-Arc $mes $dest
        }
    } else {
        return $false
    }
    
    foreach ($mes in $mess) {
        Copy-Item -Path $mes -Destination $tempdir
    }
    Set-Location $tempdir
    Write-Host "����������� ��� $mask ������ �� ��������� ����������"
    &$utildir\7z e $mask
    Write-Host "��������� ��� $mask ����� �� ��������� ����������"
    Remove-Item $mask
    Set-Location $workdir
    return $true
}

#begin Script

Create-TempDir
   
###################������ � 7z*#####################
    
if (Receive-7zPackage $archMask) {
    Move-Item -Path $tempdir\*.* -Destination $workdir\
    Remove-Item $archMask
    .\work\checkanspfr.ps1 $userEmail $notifReceiverEmail $workdir -wait
    if (Test-Path $workdir\*.*) {
        Send-MailMessage -To $notifReceiverEmail `
        -from $userEmail `
        -Subject "�������� �� ���" `
        -Body "������ ����!`n�������� � ����������� ������� �� ���, �������� � `n\\812-vvipnet01\pfrexchange\IN" `
        -SmtpServer caslb.sovbank.ru `
        -Encoding UTF8    
    }
    
}

    
#############################
if (Test-Path $tempdir\*.*) {
    $popupText = "������� ����� �� ��������������� �������, ���������� �� �����"
    Write-Host $popupText
    Set-Location $tempdir
    if (-not(Test-Path $currErrDir)) {
        Write-Host "�������� ���������� ��� �������������"
        New-Item $currErrDir -ItemType Directory | Out-Null
    }
    $mess=Get-ChildItem
    foreach ($mes in $mess) {
        Write-Host $mes.name "����������� � �������������"
        $dest= $currErrDir+"\"+$mes.name
        Copy-Arc $mes $dest
        Remove-Item $mes
        }
    Set-Location $workdir
}

Write-Host "������ ���������� ������ ���������"
Set-Location $workdir
Remove-Item $tempdir

#end