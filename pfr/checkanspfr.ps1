param (
    [string]$userEmail="pfr_exchange@sovbank.ru",
    [string]$notifReceiverEmail="a.kharkovskij@sovbank.ru",
    [string]$workdir="C:\work\exchange\TEST\IN"
    )

$startdir=Get-Location

function script:Parse-Kvits($KvitNames) {
    if (-not $KvitNames.Count) {
        return
    }
    $globalErrReps=$false
    $emailMessageSubject = "������������ � ��� ��������� ������� �������."
    $priority = "Normal"
    [System.Collections.ArrayList]$ansList = @()
    if (Test-Path $KvitNames) {
        Write-Host "`n����������� ������ �� ���"
        $isErrReps = $false
        foreach ($rep in $KvitNames) {
            $ansList.Add("������ ������ "+$rep) | Out-Null
            [xml]$xmlc = Get-Content $rep
            $ansz = $xmlc.�������.�����������������������.�������������_�_���������_�������_�_�����������_���_����������.�������������������������
            for ($i=0;$i -lt $ansz.count-1;$i++) {
                $ansList.Add($ansz.��������[$i])| Out-Null
                $ansList.Add($ansz[$i].�����������������������)| Out-Null
                $ansList.Add($ansz[$i].�����������������)| Out-Null
            }
            $ansList.Add("��������� ��������� ���������: "+$xmlc.�������.�����������������������.�������������_�_���������_�������_�_�����������_���_����������.�������+"`n")| Out-Null
            if (($xmlc.�������.�����������������������.�������������_�_���������_�������_�_�����������_���_����������.�������).CompareTo("�������") -ne 0) {
                $isErrReps = $true
            }
        }
        if ($isErrReps) {
            $globalErrReps = $true
            $emailMessageSubject = "��������!!! ���� ��� ��������� ��������� �� ���� ������� ���"
            $priority = "High"
        }
    } else {
        Write-Host "������� � ������ �� ��� �� �������"
    }
    $ansList
    $ansString = $ansList -join "`n"
    $body = "������ ����!`n"+$ansString
    Send-MailMessage -To $notifReceiverEmail `
    -from $userEmail `
    -priority $priority `
    -Subject $emailMessageSubject `
    -Body $body `
    -SmtpServer caslb.sovbank.ru `
    -Encoding UTF8
    return $globalErrReps
}

#BEGIN SCRIPT

Set-Location $workdir
$files=Get-ChildItem -file
$typeHash=@{}
foreach ($file in $files) {$typeHash[$file.Name] = ($file.Name).Substring(60,4)}
$Kvits = ($typeHash.GetEnumerator() | Where-Object {$_.Value -eq "POOD"}).Key
Parse-Kvits $Kvits
Remove-Item $Kvits
Set-Location $startdir

#END SCRIPT