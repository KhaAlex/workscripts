param(
    [Parameter(Mandatory=$False,Position=0)]$Path = (Join-Path (Split-Path -Path $MyInvocation.MyCommand.Definition -Parent) "*.xml"),
    [Parameter(Mandatory=$False,Position=1,ParameterSetName="SignCrypt")][switch]$Sign,
    [Parameter(Mandatory=$False,Position=2,ParameterSetName="SignCrypt")][string]$Crypt = $null,
    [Parameter(Mandatory=$False,Position=1,ParameterSetName="UncryptUnsign")][switch]$Uncrypt,
    [Parameter(Mandatory=$False,Position=2,ParameterSetName="UncryptUnsign")][switch]$Unsign,
    [Parameter(Mandatory=$False,Position=3)][switch]$Recurse
    )

#init block
$workdir = Split-Path $Path -Parent
$files = Split-Path $Path -Leaf
$temp = Join-Path $env:TEMP "crypttemp"
$askPopup=New-Object -ComObject Wscript.Shell
$fcolse32_exe= "C:\Program Files\MDPREI\��� �����-OW\FColseOW.exe"
$fcolse64_exe ="C:\Program Files (x86)\MDPREI\��� �����-OW\FColseOW.exe"
if (Test-Path $fcolse32_exe) {
    $script:fcolse_exe = $fcolse32_exe
} elseif (Test-Path $fcolse64_exe) {
    $script:fcolse_exe = $fcolse64_exe
} else {
    $errtext="����� �� �����������"
    Write-Host $errtext -fore red
    $askPopup.Popup($errtext,0,"������",0x10) | Out-Null
    exit 3
}

$popupText=$null
$VerbaScriptContent=$null
$Commands=@()
switch ($PsCmdlet.ParameterSetName) {
    SignCrypt {
        if ($Sign) {
            $commandHash = [ordered]@{
                VerbaScriptContent = "sign $temp\$files`nstart`nexit"
                popupText = "���������� �������� � ���"
            }
            $Commands+=New-Object PSObject -Property $commandHash
        }
        if ($Crypt) {
            $commandHash = [ordered]@{
                VerbaScriptContent = "TO $Crypt`ncrypt $temp\$files`nstart`nexit"
                popupText = "���������� �������� � ������ ����������"
            }
            $Commands+=New-Object PSObject -Property $commandHash
        }
    }
    UncryptUnsign {
        if ($Unsign) {
            $commandHash = [ordered]@{
                $VerbaScriptContent = "unsign $temp\$files`nstart`nexit"
                $popupText = "���������� �������� � ���"
            }
            $Commands+=New-Object PSObject -Property $commandHash    
        }
        if ($Uncrypt) {
            $commandHash = [ordered]@{
                $VerbaScriptContent = "decrypt $temp\$files`nstart`nexit"
                $popupText = "���������� �������� � ������ ����������"
            }
            $Commands+=New-Object PSObject -Property $commandHash    
        }
        
    }
    default {
        Write-Host "No command" -Foreground red
        exit
    }
}
#init block end

#functions block
function script:Create-TempDir ($tempdir) {
if (-not (Test-Path $tempdir)) {
        Write-Host "�������� ���������� ��� ��������� ������"
        New-Item $tempdir -ItemType Directory |Out-Null
    } else {
        $popupText = "������� ��������� ���������� ���������� ��������� ������ `
������� �������� ��������� �����������. ���������� ���������� ������ �`
��������� ���� ��������� ������ ���������� ������?"
        Write-Host $popupText
        $ans = $askPopup.Popup($popupText,0,"������� ��������� ����������",0x121)
        switch ($ans) {
            1 {
                Write-Host "�������� ��������� ���������� ����������� ��������"
                Remove-Item $tempdir -Recurse -Force
                Write-Host "�������� ��������� ���������� ��� �������� ��������"
                New-Item $tempdir -ItemType Directory |Out-Null
            }
            2 {
                Write-Host "����������� ���������� �������"
                exit 3
            }
        }  
    }    
}

function isFileCrypted {
    param (
        $file
        )
    $firstLine = Get-Content $file | Select-Object -first 1
    if ($firstLine -match "^a\d{6}") {
        return $True
    } else {
        return $False
    }
}

#functions block end

#Begin Script
$filesForProcess = @()
if ($Recurse) {
        $filesForProcess = Get-ChildItem (Join-Path $workdir $files) -Recurse
        #�������� ���� ������ ��� ��� Recurse ������
        if (-not $filesForProcess) {
            return
        }
        Create-TempDir $temp
        foreach ($ffp in $filesForProcess) {
            $ffp | Add-Member -NotePropertyName InTemp -NotePropertyValue (Move-Item $ffp $temp -PassThru)
        }
    } else {
        #�������� ���� ������ ���
        if (-not (Test-Path (Join-Path $workdir $files))) {
            return
        }
        Create-TempDir $temp
        Move-Item (Join-Path $workdir $files) $temp
    }
do {
    $notCrypted=@()
    foreach ($command in $Commands) {
        Set-Content -Path "$temp\crypt.vs" -Value $command.VerbaScriptContent
        $askPopup.Popup($command.popupText,0,"���������� �������� Rutoken",0x40) | Out-Null
        Start-Process $fcolse_exe /@"$temp\crypt.vs" -wait
        Remove-Item "$temp\crypt.vs"    
    }
    if ($Recurse) {
            foreach ($ffp in $filesForProcess) {
                if ((-not $Crypt) -or (isFileCrypted($ffp.InTemp))) {
                    Move-item $ffp.InTemp $ffp
                } else {
                    $notCrypted+=$ffp
                }
            }
        } else {
            Get-ChildItem (Join-Path $temp $files) | ForEach-Object {
                if ((-not $Crypt) -or (isFileCrypted($_))) {
                    Move-Item $_ $workdir\
                } else {
                    $notCrypted+=$_
                }
            }
        }
    $filesForProcess = $notCrypted
    $Commands =@()
    $commandHash = [ordered]@{
                VerbaScriptContent = "TO $Crypt`ncrypt $temp\$files`nstart`nexit"
                popupText = "���������� ������ ����������, ��������� ��������� ���������.`n���������� �������� � ������ ����������"
    }
    $Commands+=New-Object PSObject -Property $commandHash  
} while ($filesForProcess)
Remove-Variable filesForProcess
Remove-Item $temp -recurse -force
#end script