param(
    $Path = (Join-Path (Split-Path -Path $MyInvocation.MyCommand.Definition -Parent) "*.xml")
    )

function isFileCrypted {
    param (
        $file
        )
    $firstLine = Get-Content $file | Select-Object -first 1
    if ($firstLine -match "^a\d{6}") {
        return $True
    } else {
        return $False
    }
}

function Get-Uncrypted {
    param (
        $files
        )
    $uncrypted = @()
    foreach ($file in $files) {
        if (-not (isFileCrypted($file))) {
            $uncrypted+=$file
        }
    }
    return $uncrypted
}

$checklist= Get-ChildItem $Path
Get-Uncrypted $checklist