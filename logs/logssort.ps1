param($mask="*.XML")
Get-ChildItem $mask | ForEach-Object {
    $datemodified= $_.LastWriteTime
    $datepath = Get-Date $datemodified -format .\\yyyy\\MM\\dd
    if (-not (Test-Path $datepath)) {
        New-Item $datepath -ItemType Directory
    }
    Move-Item $_ $datepath
}