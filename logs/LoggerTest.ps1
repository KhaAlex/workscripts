
$executingScriptDir=Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$scriptPath=Join-Path $executingScriptDir "GetLogger.ps1"
$testLogPath=Join-Path $executingScriptDir "TestLog.log"

$TestLoggers=@{
    "OnlyScreenLogger"=(&$scriptPath -OnlyScreen)
    "ScreenAndFileLoggerNoPath"=(&$scriptPath -Screen)
    "FileLoggerNoPath"=(&$scriptPath)
    "ScreenAndFileLogger"=(&$scriptPath -logPath $testLogPath -Screen)
    "FileLogger"=(&$scriptPath -logPath $testLogPath)
}
foreach ($loggerName in $TestLoggers.Keys) {
    $logger=$TestLoggers.($loggerName)
    $logger
    $logger.LogFile
    $logger.LogPath
    $errorLevels=@(
        "INFO",
        "WARNING",
        "DEBUG",
        "ERROR",
        "FATAL"
    )
    $logger.Write("$loggerName writed log message with no Error level")
    foreach ($errorLv in $errorLevels) {
        $logger.Write("$loggerName writed $errorLv log message",$errorLv)
    }
}
return $TestLoggers