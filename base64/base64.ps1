param (
    [Parameter(Mandatory=$False,Position=1,ParameterSetName="Encode")][switch]$Encode,
    [Parameter(Mandatory=$False,Position=1,ParameterSetName="Decode")][switch]$Decode,
    [parameter(Mandatory=$true,position=0)][string]$fileName= $(throw "-fileName is required")
    )

switch ($PsCmdlet.ParameterSetName) {
    Encode {
        $bytecont=Get-Content $fileName -Encoding Byte
        $encoded=[convert]::ToBase64String($bytecont)
        Set-Content $fileName $encoded
    }
    Decode {
        $enccont= Get-Content $fileName -Encoding ASCII
        $deccontbytes= [System.Convert]::FromBase64String($enccont)
        Set-Content $fileName $deccontbytes -Encoding Byte        
    }
    default {
        throw "Command is missing"
    }
}


