param($source, $destination)

Import-Module BITSTransfer

$src = Get-Item $source

if ($src.PSIsContainer) {
    $startfile = New-Item -Name startfile -ItemType File -Force
    Set-Content $startfile "0"
    $script:job = Start-BITSTransfer $startfile $destination\startfile -Asynchronous
    
    Copy-Item $src -Destination $destination -Recurse -Filter {PSIsContainer}
    $filelist = Get-ChildItem $src -file -Recurse
    foreach ($file in $filelist) {
        $filedest = $file.FullName.Replace($src,$destination+"\"+$src.Name)
        Add-BitsFile $job $file.FullName $filedest
    }
} else {
    $script:job = Start-BITSTransfer $src $destination -Asynchronous
}

$continue = $true
Write-Host "i-information, p-pause, r-resume, c-cancel, e-complete"
while ($continue) {
    if ([console]::KeyAvailable) {
        $x= [System.Console]::ReadKey()
        switch ($x.key){
            i {
                Write-Host
                Write-Host $job.FilesTotal
                Write-Host $job.FilesTransferred
                Write-Host $job.BytesTotal
                Write-Host $job.BytesTransferred
                Write-Host $job.JobState
            }
            p {Suspend-BitsTransfer $job}
            r {Resume-BitsTransfer $job}
            c {
                Suspend-BitsTransfer $job
                Remove-BitsTransfer $job
                $continue = $false
            }
            e {
                Complete-BitsTransfer $job
                $continue = $false
            }
        }
        }
    else
    {
        sleep 1
    }  
}
Remove-Item $startfile
Remove-Item $destination\startfile
return $job