param(
    [Parameter(Mandatory=$True,Position=0)][string]$computername
    )

#disable secure desktop
$Reg = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey('LocalMachine', $computername)
$RegKey= $Reg.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System",'ReadWriteSubTree')
$RegKey.setvalue("EnableUIADesktopToggle", 1,"DWord")

Start-Process -FilePath "msra.exe" -Wait -ArgumentList "/offerra $computername"

#enable secure desktop
$RegKey.setvalue("EnableUIADesktopToggle", 0,"DWord")
