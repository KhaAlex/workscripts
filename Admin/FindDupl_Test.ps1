param(
    $TestDir="C:\temp\dupltest",
    $OutTestDir="C:\temp\outdupltest"
    )

$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$findDupl = Join-Path $executingScriptDirectory "FindDupl.ps1"

if (Test-Path $TestDir) {
    Remove-item $TestDir -Recurse -Force
}

if (Test-Path $OutTestDir) {
    Remove-item $OutTestDir -Recurse -Force
}

$FldrGrps=@("DuplNamesNContent","DuplNames","NoDupl")

$FldrGrpsHash=[ordered]@{}

New-Item $TestDir -ItemType Directory
New-Item $OutTestDir -ItemType Directory
$struct=" "
foreach ($FolderGroup in $FldrGrps) {
    $struct=Join-Path $struct $FolderGroup
    for ($i=1;$i -le 5;$i++) {
        $file=New-Item ("{0}\{1}\{2}\{3}.txt" -f $testDir,$struct.Trim(" ","\"),([math]::Ceiling($i/2)),$i) -Force
        if ($FolderGroup.Equals("DuplNamesNContent")) {
            Add-Content -Path $file -Value $file.FullName
        }
    }
    $FldrGrpsHash.Add($FolderGroup,(Get-Item (Join-Path $testDir ($struct.Trim(" ","\")))))
}

foreach ($folder in ($FldrGrpsHash.GetEnumerator() | Sort-Object -Descending)) {
    $result=&$findDupl -CheckPath $folder.Value -OutPath $OutTestDir
    switch (Split-Path $folder.Value -Leaf) {
        DuplNamesNContent {
            if ($result) {
                Write-Host "DuplNamesNContent: Duplicates found" -Fore Green
            } else {
                Write-Host "DuplNamesNContent: Duplicates not found" -Fore Red
            }
        }

        DuplNames {
            if ($result) {
                Write-Host "DuplNames: Duplicates found" -Fore Green
            } else {
                Write-Host "DuplNames: Duplicates not found" -Fore Red
            }
        }

        NoDupl {
            if ($result) {
                Write-Host "NoDupl: Duplicates found" -Fore Red
            } else {
                Write-Host "NoDupl: Duplicates not found" -Fore Green
            }
        }
    }
}

#Remove-item $TestDir -Recurse -Force