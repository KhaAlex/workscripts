<#

.SYNOPSIS
Script for downloading file from ftp.
.DESCRIPTION

usage there.

.EXAMPLE

example there.

.NOTES
Based on https://stackoverflow.com/a/19063042/8119446
Thx Manoj Patil

#>

param(
    $[Parameter(Mandatory=$True,Position=0)][string]FtpServer = "ftp://example.com/" 
    $[Parameter(Mandatory=$False,Position=1)][string]UserName = "anonymous"
    $[Parameter(Mandatory=$False,Position=2)][string]Password = ""
    $[Parameter(Mandatory=$False,Position=3)][string]RemotePath = "/"
    $[Parameter(Mandatory=$False,Position=4)][string]LocalPath = "."
)

$remotePickupDir = Get-ChildItem 'c:\test' -recurse
$webclient = New-Object System.Net.WebClient 

$webclient.Credentials = New-Object System.Net.NetworkCredential($user,$pass)  
foreach($item in $remotePickupDir){ 
    $uri = New-Object System.Uri($ftp+$item.Name) 
    #$webclient.UploadFile($uri,$item.FullName)
    $webclient.DownloadFile($uri,$item.FullName)
}