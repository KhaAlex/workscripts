param (
    [Parameter(Mandatory=$true,
                 ValueFromPipeline=$true,
                 ValueFromPipelineByPropertyName=$true)]
    [string]$ComputerName,
    [string]$OutFile
    )

begin {
    Add-Content $OutFile "<table border=`"1px solid black`">"
}

process {
    $info = Get-WmiObject win32_baseboard -ComputerName $ComputerName | Select-Object -Property manufacturer,product,@{l="procname";e={(Get-WmiObject win32_processor -ComputerName $ComputerName).name}}
    Add-Content $OutFile ("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>" -f $ComputerName,$info.manufacturer,$info.product,$info.procname)
}

end {
    Add-Content $OutFile "</table>"
}
