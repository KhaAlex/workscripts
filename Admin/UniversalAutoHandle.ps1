param (
    [Parameter(Mandatory=$False,Position=0,ParameterSetName="JSON")]$ConfigPath
    )
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent

if (-not $ConfigPath) {
    $ConfigPath = Join-Path $executingScriptDirectory "UAHconfig.json"
}

if (-not (Test-Path $ConfigPath)) {
        $logger.Write("Config file not found. Terminating script")
        exit
}

$host.ui.RawUI.WindowTitle = "���� ��������� �������"
$config = ConvertFrom-JSON (Get-Content $ConfigPath -raw)
$eventTriggerTimeout = $config.EventTriggerTimeout

if (Test-Path (Join-Path $executingScriptDirectory "GetLogger.ps1"))
    {
        $script:logger = &(Join-Path $executingScriptDirectory "GetLogger.ps1") $config.LogPath -Screen
    } else {
        $script:logger = New-object PSObject
        Add-Member -MemberType ScriptMethod -Name Write -Value {param($str,$level);Write-Host $str} -InputObject $logger
        $logger.Write("Logger not found, program would be work without logging")
    }

function Send-Email {
    param(
        $msg,
        [switch]$HighPriority
        )
    $priority="Normal"
    if ($HighPriority) {
        $priority="High"
    }
    Send-MailMessage `
-to $config.NotificationReceiver `
-from $config.NotificationReceiver `
-subject $config.emailsSubject `
-body $msg `
-smtpServer $config.smtpServer `
-priority $priority `
-encoding Unicode
}

class ReconnectTimer {
    [System.Timers.Timer]$Timer
    $ReconnectInterval = $config.ReconnectInterval
    ReconnectTimer() {
        $this.Timer = New-Object System.Timers.Timer
        $this.Timer.Interval = $this.ReconnectInterval
        $this.Timer.AutoReset = $true
    }
    Start() {
        $this.Timer.Start()
    }
    Stop() {
        $this.Timer.Stop()
    }
    RegisterTimer() {
        Register-ObjectEvent $this.Timer Elapsed -SourceIdentifier "Reconnect" -MessageData @{TaskName = "ReconnectTimer";EventName="TryReconnect"}
    }
}

class FSWEvent {
    [string]$Name
    [string]$Command
    [string]$ParamString
    [bool]$HandleOnStartWatching = $true
    FSWEvent($eventData) {
        $this.Name = $eventData.Name
        $this.Command = $eventData.Command
        $this.HandleOnStartWatching = $eventData.HandleOnStartWatching
        $this.ParamString=""
        foreach ($param in $eventData.Params) {
            if ($param.Name) {
                $this.ParamString+=(" -{0} {1}" -f $param.Name,$param.Value)
            } else {
                $this.ParamString+=(" {0}" -f $param.Value)
            }
        }
    }
    Shoot() {
        Invoke-Expression ("{0}{1}" -f $this.Command,$this.ParamString)
    }
}

class WatchTask {
    [string]$Name
    [string]$Path
    [string]$FilesMask
    [bool]$Directory = $false
    [bool]$Online = $false
    [hashtable]$Events
    [System.IO.FileSystemWatcher]$fsw
    [bool]RegisterWatcher() {
        $notifyFilters = 'FileName, LastWrite'
        if ($this.Directory) {
            $notifyFilters = 'DirectoryName'
        }
        try {
            $this.fsw = New-Object IO.FileSystemWatcher $this.Path, $this.FilesMask -Property @{IncludeSubdirectories = $false;NotifyFilter = [IO.NotifyFilters]$notifyFilters} -ErrorAction Stop
            $this.Online = $true
        } catch {
            $this.Online = $false
            return $false
        }
        foreach ($ev in $this.Events.Values) {
            Register-ObjectEvent $this.fsw -EventName $ev.Name -SourceIdentifier "$($this.Name)$($ev.Name)" -MessageData @{TaskName = $this.Name;EventName=$ev.Name}
            if ($ev.HandleOnStartWatching) {$ev.Shoot()}
        }
        Register-ObjectEvent $this.fsw -EventName Error -SourceIdentifier "$($this.Name)Error" -MessageData @{TaskName = $this.Name;EventName="Error"}
        return $true
    }
    UnregisterWatcher() {
        foreach ($ev in $this.Events.Values) {
            Get-EventSubscriber -SourceIdentifier "$($this.Name)$($ev.Name)" |Unregister-Event
        }
        Unregister-Event -SourceIdentifier "$($this.Name)Error"
        $this.fsw = $null
    }
    WatchTask($taskFromConfig) {
        $this.Name = $taskFromConfig.Name
        $this.Path = $taskFromConfig.Path
        $this.FilesMask = $taskFromConfig.FilesMask
        $this.Directory = $taskFromConfig.Directory
        $this.Events = @{}
        foreach ($ev in $taskFromConfig.Events) {
            $this.Events.Add($ev.Name,[FSWEvent]::new($ev))
        }
    }
}

$ReconnectionTimer=[ReconnectTimer]::new()
$ReconnectionTimer.RegisterTimer()

$WatchersList=@{}

foreach ($Task in $config.Tasks) {
    $taskForReg = [WatchTask]::new($task)
    if ($taskForReg.RegisterWatcher()) {
        $WatchersList.add($task.Name,$taskForReg)
    } else {
        $logger.Write(("Directory {0} unreachable, task {1} not registered, and disabled" -f $taskForReg.Path,$taskForReg.Name),"ERROR")
    }
}

$continue = $true
while ($continue) {
    $ev=Wait-Event
    $logger.Write(("Event {1} of task {0} triggered in {2}" -f $ev.MessageData.TaskName,$ev.MessageData.EventName,$ev.TimeGenerated))
    switch ($ev.MessageData.EventName) {
        Error {
            $logger.Write(("Error. Directory {0} is lost. Task name:{1}. Time:{2}." -f $ev.SourceArgs.Path,$ev.MessageData.TaskName,$ev.TimeGenerated),"ERROR")
            Send-Email ("���������� {0} ����������. ������:{1} �����:{2}" -f $ev.SourceArgs.Path,$ev.MessageData.TaskName,$ev.TimeGenerated) -HighPriority
            $WatchersList[$ev.MessageData.TaskName].UnregisterWatcher()
            $WatchersList[$ev.MessageData.TaskName].Online = $false
            $ReconnectionTimer.Start()
            break
        }
        TryReconnect{
            $TimerStop = $true
            foreach ($Task in $WatchersList.Values) {
                if (-not $Task.Online) {
                    $TimerStop=$false
                    if ((Test-Path $Task.Path) -and $Task.RegisterWatcher()) {
                        $logger.Write(("Directory {0} connection restored. Task {1} restarted. Time:{2}" -f $Task.Path,$Task.Name,(Get-Date -f "hh:mm:ss")))
                        Send-Email ("���������� {0} ����� ��������, ������� ��������� �����������. ��� ������:{1}. �����:{2}" -f $Task.Path,$Task.Name,(Get-Date -f "hh:mm:ss"))
                    }
                }
                if ($TimerStop) {$ReconnectionTimer.Stop()}
            }
            break
        }
        default {
            Start-Sleep -Milliseconds $eventTriggerTimeout
            $WatchersList[$ev.MessageData.TaskName].Events[$ev.MessageData.EventName].Shoot()
        }

    }
    Remove-Event -SourceIdentifier $ev.SourceIdentifier
}
