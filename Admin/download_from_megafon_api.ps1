<#

.SYNOPSIS
Script for downloading file from trough megafon CRM API
needed for backup, so no date set implemented for now
.DESCRIPTION

Get-Help command show options :)

.EXAMPLE

.\download_from_megafon_api.ps1 -URI http:\\

.NOTES

#>

param(
    [Parameter(mandatory=$true,position=0)]$URI="",
    [Parameter(mandatory=$true,position=1)]$AuthToken="",
    [Parameter(mandatory=$False,position=2)]$Destination="."
    )

$form = @{
	cmd="history"
    period="yesterday"
	token=$AuthToken
}

$raw_res=Invoke-WebRequest -Uri $URI -Method Post -Body $form

$header='call_id','call_direction','client','manager','manager_number', `
        'datetime','wait','long','record_url'

$res = ConvertFrom-CSV $raw_res.Content -Delimiter ',' -Header $header

foreach ($call in $res) {
    $result_dest=Join-Path $destination (get-date $call.datetime -Format yyyy-MM-dd)
    if (-not (Test-Path $result_dest)) {
        New-Item $result_dest -ItemType Directory
    }

    if ($call.record_url) {
        Invoke-WebRequest -Uri $call.record_url -OutFile (Join-Path $result_dest (Split-Path $call.record_url -leaf))
    }
}