param(
[Parameter(Mandatory=$true)][string]$Source = $(throw "-Source is required"),
[string]$filter = '*.*',
[string]$TaskName
)
if (-not (Test-Path $Source -PathType Container)){
    throw "Bad Source directory"
    exit
}

if (!$TaskName) {
    $eventsRegistered=Get-EventSubscriber
    [string]$TaskName=$eventsRegistered.Length+1
}

$MessData=[PSCustomObject]@{
    Destination=[string]$Destination
    Source=[string]$Source
    CopyScriptPath=[string]$command
    Mask=$filter
}

$fsw = New-Object IO.FileSystemWatcher $Source, $filter -Property @{IncludeSubdirectories = $false;NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'}

Register-ObjectEvent $fsw Error -SourceIdentifier "Error$TaskName" -Action {
    $name = $Event.SourceEventArgs.Name
    $timeStamp = $Event.TimeGenerated
    $Source = $Event.MessageData
    Write-Host "$name. Directory $Source is lost. Time:$timeStamp" -fore red
} -MessageData $MessData.Source