Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

$form = New-Object Windows.Forms.Form

$form.Text = '�������� ����'
$form.Size = New-Object Drawing.Size @(165,210)
$form.StartPosition = 'CenterScreen'
$form.FormBorderStyle = 'FixedSingle'
$form.MinimizeBox=$false
$form.MaximizeBox=$false

$calendar = New-Object System.Windows.Forms.MonthCalendar
$calendar.ShowTodayCircle = $false
$calendar.MaxSelectionCount = 1
$form.Controls.Add($calendar)

$OKButton = New-Object System.Windows.Forms.Button
$OKButton.Location = New-Object System.Drawing.Point(0,160)
$OKButton.Size = New-Object System.Drawing.Size(80,23)
$OKButton.Text = '��'
$OKButton.DialogResult = [System.Windows.Forms.DialogResult]::OK
$form.AcceptButton = $OKButton
$form.Controls.Add($OKButton)

$CancelButton = New-Object System.Windows.Forms.Button
$CancelButton.Location = New-Object System.Drawing.Point(80,160)
$CancelButton.Size = New-Object System.Drawing.Size(80,23)
$CancelButton.Text = '������'
$CancelButton.DialogResult = [System.Windows.Forms.DialogResult]::Cancel
$form.CancelButton = $CancelButton
$form.Controls.Add($CancelButton)

$form.Topmost = $true

$dlgResult = $form.ShowDialog()

$result=$false

if ($DlgResult -eq [System.Windows.Forms.DialogResult]::OK)
{
    $result=$true
    Add-Member -memberType NoteProperty -InputObject $result -Name date -Value $calendar.SelectionStart -Force
    #Write-Host "Date selected: $($result.date.ToShortDateString())"
}

return $result
