param($ComputerName)

$procHash=@{}
foreach ($proc in (Get-WmiObject -Class Win32_process -ComputerName $ComputerName))
    {
        #Write-Host $proc.Name $proc.GetOwner().User $proc.GetOwner()
        if (!($proc.GetOwner().User)) {
            Write-Host "Process have no owner" $proc.ProcessName
            continue
        }
        if (!$procHash.ContainsKey($proc.GetOwner().User)) {
            $procHash[$proc.GetOwner().User]=@()
        }
        $procHash[$proc.GetOwner().User]+=$proc.ProcessName
    }
$procHash