<#

.SYNOPSIS
Find duplicates in folder, copying it in -OutFolder, handle different by hash files.
Requiring SmartCopyFile.ps1 in script directory.

.DESCRIPTION

-CheckPath      Directory for find duplicates.

-OutPath        Directory for founded files.


.EXAMPLE

FindDupl.ps1 -CheckPath $testDir -OutPath $OutTestDir

.NOTES

#>

param(
    $CheckPath=".",
    $OutPath="$(Split-Path (Get-item ".") -Parent)\dupl"
    )

$executingScriptDirectory=Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$smartCopy=Join-Path $executingScriptDirectory "SmartCopyFile.ps1"

$allFileNames=(Get-ChildItem $CheckPath -Recurse -File).Name
$uniqFileNames=(Get-ChildItem $CheckPath -Recurse -File).Name | Select-Object -Unique

$duplNames=(Compare-Object -ReferenceObject $uniqFileNames -DifferenceObject $allFileNames).InputObject

if (-not ($duplNames)) {
    return $false
}

if (-not (Test-Path $OutPath)) {
    $OutPath=New-Item $OutPath -ItemType Directory
}

Get-ChildItem -Path ($duplNames| % {Join-Path $CheckPath $_}) -Recurse | &$smartCopy -Destination $OutPath

return $true