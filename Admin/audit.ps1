
#Event ID	Description
#4624		An account was successfully logged on.
#4625		An account failed to log on.
#4740		A user account was locked out.
$events = Get-EventLog -LogName 'security' -InstanceId 4624 -after (get-date "31.03.20 13:10") -before (get-date "31.03.20 13:20")
$events | select -expand replacementstrings
$events | select -expand message

(Get-EventLog -LogName security -InstanceId 4624 -before (get-date "01.04.20 08:00") -after (get-date "01.04.20 13:10")) | Where-Object {$_.ReplacementStrings[8] -eq 10} | foreach {Write-Host ("{0}{1,20} {2,5}" -f $_.TimeGenerated,$_.ReplacementStrings[5],$_.ReplacementStrings[8])}

(Get-EventLog -LogName security -InstanceId 4624 -after (get-date "01.04.20 08:00") -before (get-date "01.04.20 13:00")) | Where-Object {$_.ReplacementStrings[8] -eq 10} | foreach {Write-Host ("{0}{1,20} {2,5}" -f $_.TimeGenerated,$_.ReplacementStrings[5],$_.ReplacementStrings[8])}

Get-WinEvent -LogName "Microsoft-Windows-TerminalServices-LocalSessionManager/Operational" -MaxEvents 200 -ComputerName server-rdp | foreach {Write-Host ("{0:yyyy.MM.dd HH:mm} {1} {2,20}`n{3}" -f $_.TimeCreated,$_.id,$_.properties.value[0],(($_.message) -split "\n")[0])}

Get-WinEvent -FilterHashtable @{LogName="Microsoft-Windows-TerminalServices-LocalSessionManager/Operational"; StartTime=(Get-Date "07.04.2020 17:00")} -ComputerName server-rdp | Sort-Object $_.TimeCreated -Descending | foreach {Write-Host ("{0:yyyy.MM.dd HH:mm} {1} {2,20}`n{3}" -f $_.TimeCreated,$_.id,$_.properties.value[0],(($_.message) -split "\n")[0])}

#substatus
#Code		Description
#0xC0000064	Account does not exist
#0xC000006A	Incorrect password
#0xC000006D	Incorrect username or password
#0xC000006E	Account restriction
#0xC000006F	Invalid logon hours
#0xC000015B	Logon type not granted
#0xC0000070	Invalid Workstation
#0xC0000071	Password expired
#0xC0000072	Account disabled
#0xC0000133	Time difference at DC
#0xC0000193	Account expired
#0xC0000224	Password must change
#0xC0000234	Account locked out

#Logon-type codes and values
#Numeric Code	Value
#0		System
#2		Interactive
#3		Network
#4		Batch
#5		Service
#6		Proxy
#7		Unlock
#8		NetworkCleartext
#9		NewCredentials
#10		RemoteInteractive
#11		CachedInteractive
#12		CachedRemoteInteractive
#13		CachedUnlock

<#
param (
  $ComputerName = $Env:ComputerName,
  $Records = 10
)
function Get-FailureReason {
  Param($FailureReason)
    switch ($FailureReason) {
      '0xC0000064' {"Account does not exist"; break;}
      '0xC000006A' {"Incorrect password"; break;}
      '0xC000006D' {"Incorrect username or password"; break;}
      '0xC000006E' {"Account restriction"; break;}
      '0xC000006F' {"Invalid logon hours"; break;}
      '0xC000015B' {"Logon type not granted"; break;}
      '0xc0000070' {"Invalid Workstation"; break;}
      '0xC0000071' {"Password expired"; break;}
      '0xC0000072' {"Account disabled"; break;}
      '0xC0000133' {"Time difference at DC"; break;}
      '0xC0000193' {"Account expired"; break;}
      '0xC0000224' {"Password must change"; break;}
      '0xC0000234' {"Account locked out"; break;}
      '0x0' {"0x0"; break;}
      default {"Other"; break;}
  }
}
Get-EventLog -ComputerName $ComputerName -LogName 'security'
   -InstanceId 4625 -Newest $Records |
  select @{Label='Time';Expression={$_.TimeGenerated.ToString('g')}},
    @{Label='User Name';Expression={$_.replacementstrings[5]}},
    @{Label='Client Name';Expression={$_.replacementstrings[13]}},
    @{Label='Client Address';Expression={$_.replacementstrings[19]}},
    @{Label='Server Name';Expression={$_.MachineName}},
    @{Label='Failure Status';Expression={Get-FailureReason
       ($_.replacementstrings[7])}},
    @{Label='Failure Sub Status';Expression={Get-FailureReason
       ($_.replacementstrings[9])}}
#>

# RDS server client disconnect code     Disconnect reason
# 0x00000001        The disconnection was initiated by an administrative tool on the server in another session.
# 0x00000002        The disconnection was due to a forced logoff initiated by an administrative tool on the server in another session.
# 0x00000003        The idle session limit timer on the server has elapsed.
# 0x00000004        The active session limit timer on the server has elapsed.
# 0x00000005        Another user connected to the server, forcing the disconnection of the current connection.
# 0x00000006        The server ran out of available memory resources.
# 0x00000007        The server denied the connection.
# 0x00000009        The user cannot connect to the server due to insufficient access privileges.
# 0x0000000A (10)   The server does not accept saved user credentials and requires that the user enter their credentials for each connection.
# 0x0000000B (11)   The disconnection was initiated by the user disconnecting his or her session on the server or by an administrative tool on the server.
# 0x0000000C (12)   The disconnection was initiated by the user logging off his or her session on the server.

# Windows RDP-Related Event Logs: Identification, Tracking, and Investigation
# https://ponderthebits.com/2018/02/windows-rdp-related-event-logs-identification-tracking-and-investigation/
# !so long as the “Source Network Address” is NOT “LOCAL”

# Event ID: 21
# Indicates successful RDP logon and session instantiation.

# Event ID: 24
# The user has disconnected from an RDP session.

# Event ID: 25
# The user has reconnected to an existing RDP session.

# Event ID: 39
# The user formally disconnected from the RDP session.
