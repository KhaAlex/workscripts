<#

.SYNOPSIS
Script for enable Kerberos Constrained Delegation for Windows Admin Center gateway Single-sign-On.

.DESCRIPTION
Require administrator privileges, and Get-ADComputer, Set-ADComputer commands available.

-CKC parameter try to execute "klist purge -li 0x3e7" on remote machine, you need Invoke-Command possible for remote PC

.EXAMPLE

WACKCDEnable.ps1 -WACServer wac_server_name -TargetHost workstation_name

.EXAMPLE

WACKCDEnable.ps1 -WACServer wac_server_name -TargetHost target_server_name -CKC

.NOTES
Thanks Charbel Nemnom https://charbelnemnom.com/2019/07/how-to-enable-single-sign-on-sso-for-windows-admin-center/.

#>

param(
[Parameter(Mandatory=$True,Position=0)][string]$WACServer,
[Parameter(Mandatory=$True,Position=1)][string]$TargetHost,
[Parameter(Mandatory=$False,Position=2)][switch]$CKC
)

$wac = Get-ADComputer $WACServer
$tgtSys = Get-ADComputer $TargetHost
Set-ADComputer -identity $tgtSys -PrincipalsAllowedToDelegateToAccount $wac
if ($CKC) {Invoke-Command -Computername $tgtSys -ScriptBlock {klist purge -li 0x3e7}}