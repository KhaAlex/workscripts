param(
    [Parameter(Mandatory=$True,Position=0,ParameterSetName="Convert")]`
    [Parameter(Mandatory=$True,Position=0,ParameterSetName="ToUTF8NoBOM")]`
    [Parameter(Mandatory=$True,Position=0,ParameterSetName="FromUTF8NoBOM")]`
    [string]$Path,
    [Parameter(Mandatory=$True,Position=1,ParameterSetName="Convert")]`
    [Parameter(Mandatory=$True,Position=1,ParameterSetName="ToUTF8NoBOM")]`
    $From,
    [Parameter(Mandatory=$True,Position=2,ParameterSetName="Convert")]`
    [Parameter(Mandatory=$True,Position=2,ParameterSetName="FromUTF8NoBOM")]`
    $To,
    [Parameter(Mandatory=$False,Position=1,ParameterSetName="FromUTF8NoBOM")][switch]$FromUTF8NoBOM,
    [Parameter(Mandatory=$False,Position=2,ParameterSetName="ToUTF8NoBOM")][switch]$ToUTF8NoBOM,
    [Parameter(Mandatory=$True,Position=0,ParameterSetName="ListCP")][switch]$List
    )

function Get-Cp {
    [OutputType([System.Object])]
    param (
        $CpOrName
        )
    $found = [System.Text.Encoding]::GetEncodings() |Where-Object -Property CodePage -like $CpOrName
    if (-not $found) {
        $found = [System.Text.Encoding]::GetEncodings() |Where-Object -Property Name -like $CpOrName
    } 
    if (-not $found) {
        throw "Cp $CpOrName not found"
    }
    $res = [System.Text.Encoding]::GetEncoding($found.CodePage)
    return ,$res
}

switch ($PsCmdlet.ParameterSetName) {
    Convert {
        $From = Get-Cp $From
        $To = Get-Cp $To
    }
    FromUTF8NoBOM {
        $From = New-Object System.Text.UTF8Encoding $False
        $To = Get-Cp $To
    }
    ToUTF8NoBOM {
        $From = Get-Cp $From
        $To = New-Object System.Text.UTF8Encoding $False
    }
    ListCP {
        [System.Text.Encoding]::GetEncodings()
        exit
    }
}

$cont = [System.IO.File]::ReadAllText($path,$From)
[System.IO.File]::WriteAllText($path,$cont,$To)
