<#

.SYNOPSIS
Script for show users rdp connection statistic.

.DESCRIPTION

Based on Events 21,24,25,39 from Microsoft-Windows-TerminalServices-LocalSessionManager/Operational.

.EXAMPLE

example there.

.EXAMPLE

another example there.

.NOTES
Thanks JONATHON POLING https://ponderthebits.com/2018/02/windows-rdp-related-event-logs-identification-tracking-and-investigation/.

#>

param(
    [Parameter(Mandatory=$False,Position=0)][string]$Server="server-rdp",
    [Parameter(Mandatory=$True,Position=1)][string]$After,
    [Parameter(Mandatory=$False,Position=2)][string]$Before,
    [Parameter(Mandatory=$false,
                Position=3,
                ValueFromPipeline=$true,
                ValueFromPipelineByPropertyName=$true,ParameterSetName="AuditRequest")]$user,
    [Parameter(Mandatory=$False,Position=4)][switch]$Raw
    )

begin {

    function parseUserName ([string]$username) {
        $aduser = Get-AdUser -Identity $username -ErrorAction Stop
        $duserName, $domainName = ($aduser.UserPrincipalName | Select-String -Pattern '^(.+)@(.+)\.(.+)$').Matches[0].Groups.Value[1..2]
        return "{0}\{1}" -f $domainName, $duserName
    }

    $TimeIntervalString = "Getting audit info from $After"
    try { 
        $strtTime=(Get-Date $After -ErrorAction Stop)
    } catch {
        Write-Host "Can't parse after date"
        exit
    }
    
    if ($Before) {
        try { 
            $ndTime=(Get-Date $Before -ErrorAction Stop)
        } catch {
            Write-Host "Can't parse before date"
            exit
        }
        $TimeIntervalString = "Getting audit info from $After to $Before"
    } else {
        $ndTime=Get-Date
    }
    
    if ($user) {
        $puser=parseUserName($user)
        Write-Host "Getting audit info of $puser"
    }
    Write-Host $TimeIntervalString

    $rawresult = Get-WinEvent -FilterHashtable `
        @{
            LogName="Microsoft-Windows-TerminalServices-LocalSessionManager/Operational"; `
            StartTime=$strtTime; ` #("{0:dd/MM/yyyy}" -f $After)
            EndTime=$ndTime; `
            Id=21,24,25,39 ` # 21,25 - start(logon,recon) ; 24,39 - end(discon,formal discon)
            } `
            -ComputerName $Server | Sort-Object -Property TimeCreated

}

process {}

end {
    if ($user) {
        $rawresult = $rawresult | Where-Object {$_.Properties.Value[0] -eq $puser}
    }

    if ($Raw) {
        $rawresult
    } else {
        foreach ($rec in $rawresult) {Write-Host ("{0:yyyy.MM.dd HH:mm} {1} {2,20}`n{3}" -f $rec.TimeCreated,$rec.id,$rec.properties.value[0],(($rec.message) -split "\n")[0])}
    }

}


