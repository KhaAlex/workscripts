param(
    [Parameter(ValueFromPipeline=$true)]$source,
    $destination,
    [switch]$Move
    )

begin {
    $CurScript = $MyInvocation.MyCommand.Definition
    $temp=$env:temp
    $source = Get-ChildItem $source
}

process {
    foreach ($src in $source){
        $orighash=Get-FileHash $src
        $destFullPath = Join-Path $destination $src.Name
        if (Test-Path $destFullPath) {
            $destDuplicateHash=Get-FileHash $destFullPath
            if ($orighash.Hash -eq $destDuplicateHash.Hash) {
                #file already moved
                if ($Move) {Remove-Item $src -Verbose}
                continue
            } else {
                #Found another file with same name
                $diffFolderName="diff"+(Get-Date -f yyyyMMdd)
                $diffFilesFolder=Join-Path $destination $diffFolderName
                if (-not (Test-Path $diffFilesFolder -PathType Container)) {
                    New-Item $diffFilesFolder -ItemType Directory | Out-Null
                }
                $recursiveStartForDiffFile=$CurScript+" "+$src+" "+$diffFilesFolder
                Invoke-Expression $recursiveStartForDiffFile
                continue
            }
        }
    
        $copying=Copy-Item $src $temp -PassThru -Verbose
        $copyed=Copy-Item $copying $destination -PassThru -Verbose
        Start-Sleep -m 100
        $orighash=Get-FileHash $src
        $endhash=Get-FileHash $copyed
        if ($orighash.Hash -eq $endhash.Hash) {
            if ($Move) {Remove-Item $src -Verbose}
            Remove-Item $copying -Verbose
        } else {
            Remove-Item $copyed -Verbose
            Remove-Item $copying -Verbose
            throw "Copying Failed"
        }
    }
    
}

end{}
