param (
    [Parameter(Mandatory=$true,
                 ValueFromPipeline=$true,
                 ValueFromPipelineByPropertyName=$true)]
    [string]$ComputerName
    )

process {
    $spoolerService = Get-Service -Name spooler -ComputerName $ComputerName
    $spoolerService.Stop()
    $spoolPrintersFolder = ([Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey('LocalMachine', $ComputerName)).OpenSUBKEY("SYSTEM\CurrentControlSet\Control\Print\Printers").GetValue("DefaultSpoolDirectory")
    $path = Join-Path "\\$ComputerName\" $spoolPrintersFolder.Replace(":","$")
    Remove-Item $path\* -Recurse -Force
    $spoolerService.Start()
    
}
