param(
[string]$folder = 'd:\temp\test',# Enter the root path you want to monitor.
[string]$filter = '*.*'  # You can enter a wildcard filter here.
#for monitoring folders $filter must be "*",
#and filesystemwatcher parameter:
# -Property @{IncludeSubdirectories = $false;NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'}
# need add NotifyFilter = [IO.NotifyFilters]'DirectoryName'
)
                         
$fsw = New-Object IO.FileSystemWatcher $folder, $filter -Property @{IncludeSubdirectories = $false;NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'}
$ReconnectTimer = New-Object System.Timers.Timer
$ReconnectTimer.Interval= 10000
$ReconnectTimer.AutoReset= $true
Register-ObjectEvent $ReconnectTimer Elapsed -SourceIdentifier TryReconnect

Register-ObjectEvent $fsw Created -SourceIdentifier FileCreated 
#Register-ObjectEvent $fsw Deleted -SourceIdentifier FileDeleted 
#Register-ObjectEvent $fsw Changed -SourceIdentifier FileChanged
Register-ObjectEvent $fsw Error -SourceIdentifier FolderError 
$continue=$true
while ($continue) {
    $ev=Wait-Event
    $color="white"
    switch ($ev.SourceIdentifier) {
        FileCreated {
            $color="green"
            break
        }
        FileChanged {
            $color="yellow"
            break
        }
        FileDeleted {
            $color="red"
            break
        }
        FolderError {
            Write-Host "FolderError" -Foreground red
            Unregister-Event -SourceIdentifier FileCreated
            Unregister-Event -SourceIdentifier FolderError
            Remove-Variable fsw
            $ReconnectTimer.Start()
            break
        }
        TryReconnect {
            Write-Host "Trying reconnect"
            if (Test-Path $folder) {
                $fsw = New-Object IO.FileSystemWatcher $folder, $filter -Property @{IncludeSubdirectories = $false;NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'}
                Register-ObjectEvent $fsw Created -SourceIdentifier FileCreated 
                Register-ObjectEvent $fsw Error -SourceIdentifier FolderError 
                Write-Host "Reconnection successful subscribers now:"
                Get-EventSubscriber
                $ReconnectTimer.Stop()
            }
            break
        }
    }
    Write-Host ("File {0} {1} {2}" -f $ev.SourceArgs.Name,$ev.SourceArgs.ChangeType,$ev.TimeGenerated) -Foreground $color
    Remove-Event -SourceIdentifier $ev.SourceIdentifier    
}

Remove-Variable fsw
# To stop the monitoring, run the following commands:
# Unregister-Event FileDeleted
# Unregister-Event FileCreated
# Unregister-Event FileChanged