<#

.SYNOPSIS
Script for show users rdp session statistic as intervals. Requre result AuditRDP.ps1 -Raw as input.

.DESCRIPTION

usage there.

.EXAMPLE

example there.

.EXAMPLE

another example there.

.NOTES
Put some notes here.

#>

param(
    [Parameter(Mandatory=$true,
                Position=0,
                ValueFromPipeline=$true,
                ValueFromPipelineByPropertyName=$true)][System.Diagnostics.Eventing.Reader.EventRecord]$AuditRdpResult
    )

begin {
    enum IDs {
        undefined
        logon = 21
        discon = 24
        recon = 25
        formal_discon = 39
    }
    class Interval {
        [IDs]$StartId
        [IDs]$EndId
        [DateTime]$Start
        [DateTime]$End
        [TimeSpan]$Long
        [string]$StartIP
        [string]$EndIP
        
        Interval(
            [IDs]$Id,
            [DateTime]$datTim,
            [string]$Ip
            ){
            $this.addEvent($Id,$datTim,$Ip)
        }
    
        addEvent(
            [IDs]$Id,
            [DateTime]$datTim,
            [string]$Ip
            ){
            if ($Id -in [IDs]::logon,[IDs]::recon) {
                if ($this.StartId) {
                    throw "Can't add event, interval already started"
                }
                $this.StartId=$Id
                $this.Start=$datTim
                $this.StartIP=$Ip
            } elseif ($Id -in [IDs]::discon,[IDs]::formal_discon) {
                if ($this.EndId) {
                    throw "Can't add event, interval already ended"
                }
                $this.EndId=$Id
                $this.End=$datTim
                if (($this.StartId) -and ($this.EndId)) {$this.Long=New-TimeSpan $this.Start $this.End}
                $this.EndIP=$Ip
            } else {
                    Write-Host "Unexpected event id:$Id"
                    exit
            }
        }
    
    }
    
    $intervalTable = [system.collections.arraylist]@()
    
    Add-Member -memberType ScriptMethod -InputObject $intervalTable -Name getEvent -Value {
        param ($winEvent)
        
        $Id=$winEvent.Id
        $TimeCreated=$winEvent.TimeCreated
        $Ip=$winEvent.Properties[2].Value
        if (($this.Count -eq 0) -or ($this[-1].EndId)) {
                $this.Add([Interval]::New($Id,$TimeCreated,$Ip)) | Out-Null
            } else {
                $this[-1].addEvent($Id,$TimeCreated,$Ip)
            }
    }
}

process{
    foreach ($rec in $AuditRdpResult) {
        $intervalTable.getEvent($rec)
    }
}

end{
    $intervalTable | Format-Table
}

