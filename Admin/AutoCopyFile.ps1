param(
[Parameter(Mandatory=$true)][string]$Source = $(throw "-Source is required"),
[Parameter(Mandatory=$true)][string]$Destination = $(throw "-Destination is required"),
[string]$filter = '*.*',
[string]$TaskName = "AutoCopy"
)
if (-not ((Test-Path $Source -PathType Container) -and (Test-Path $Destination -PathType Container))){
    throw "Bad Destination or Source directory"
    exit
}

$host.ui.RawUI.WindowTitle = $TaskName

$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$command = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 

$Files = Join-Path $Source $filter

$fsw = New-Object IO.FileSystemWatcher $Source, $filter -Property @{IncludeSubdirectories = $false;NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'}

Register-ObjectEvent $fsw Created -SourceIdentifier $TaskName
Register-ObjectEvent $fsw Error -SourceIdentifier "Error$TaskName"

$ReconnectTimer = New-Object System.Timers.Timer
$ReconnectTimer.Interval= 10000
$ReconnectTimer.AutoReset= $true

Register-ObjectEvent $ReconnectTimer Elapsed -SourceIdentifier "Reconnect$TaskName"

if (Test-Path $Files) {
    $expr=$command+" "+$Files+" "+$Destination 
    Invoke-Expression $expr    
}

$continue = $true
while ($continue) {
    $ev=Wait-Event
    switch ($ev.SourceIdentifier) {
        $TaskName {
            $name = $ev.SourceEventArgs.Name
            $changeType = $ev.SourceEventArgs.ChangeType
            $timeStamp = $ev.TimeGenerated
            Write-Host "The file '$name' was $changeType at $timeStamp" -fore green
            if (Test-Path ($Files)) {
                Start-Sleep -s 3
                $expr=$command+" "+$Files+" "+$Destination
                Invoke-Expression $expr
            }
            break
        }
        "Error$TaskName" {
            Write-Host ("Error. Directory {0} is lost. Time:{1}" -f $ev.SourceArgs.Path,$ev.TimeGenerated) `
            -fore red
            Unregister-Event -SourceIdentifier $TaskName
            Unregister-Event -SourceIdentifier "Error$TaskName"
            Remove-Variable fsw
            $ReconnectTimer.Start()
            break
        }
        "Reconnect$TaskName" {
            if (Test-Path $Source) {
                if (Test-Path $Files) {
                    $expr=$command+" "+$Files+" "+$Destination 
                    Invoke-Expression $expr    
                }
                $fsw = New-Object IO.FileSystemWatcher $Source, $filter -Property @{IncludeSubdirectories = $false;NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'}
                Register-ObjectEvent $fsw Created -SourceIdentifier $TaskName 
                Register-ObjectEvent $fsw Error -SourceIdentifier "Error$TaskName" 
                Write-Host "Reconnection successful subscribers now:"
                Get-EventSubscriber
                $ReconnectTimer.Stop()
            }
            break
        }
    }
    Remove-Event -SourceIdentifier $ev.SourceIdentifier
}

#Get-EventSubscriber |Unregister-Event
