$excel=New-Object -ComObject excel.application
$excel.Workbooks.Add()
$userlist = Get-ADUser -Filter {(Enabled -eq "True") -and ((telephonenumber -ne "null") -or (mail -ne "null"))} -Properties surname,givenname,cn,mail,telephonenumber | Sort-Object cn
For ($i=0;$i -lt $userlist.length;$i++)
    {
        $excel.Cells.Item($i+1,1)=$userlist[$i].surname
        $excel.Cells.Item($i+1,2)=$userlist[$i].givenname
        $excel.Cells.Item($i+1,3)=$userlist[$i].cn
        $excel.Cells.Item($i+1,4)=$userlist[$i].mail
        $excel.Cells.Item($i+1,5)=$userlist[$i].telephoneNumber
	$excel.Cells.Item($i+1,6)=$userlist[$i].telephoneNumber+" "+$userlist[$i].surname+" "+$userlist[$i].givenname
    }
$excel.visible=$true

