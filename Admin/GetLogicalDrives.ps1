<#

.SYNOPSIS
Returns objects containing win32_logicaldisk information wirth some handful methods
.DESCRIPTION

.EXAMPLE

.NOTES
Put some notes here.

#>

$LogicalDrives=[PSCustomObject]@{
    devices = (Get-WmiObject win32_logicaldisk)
}

Add-Member -memberType ScriptMethod -InputObject $LogicalDrives -Name IsDeviceOnline -Value {
    param(
        [Parameter(Mandatory=$true)]$letter,
        [Parameter(Mandatory=$false)]$VolumeName
        )
    $result = $true
    Add-Member -memberType NoteProperty -InputObject $result -Name description -Value "Drive online"
    $letter = $letter.Trim(" ",":","\")
    $Drive = $this.devices | Where-Object {$_.DeviceId.Equals("$letter`:")}
    if ($Drive) {
        if (($VolumeName) -and -not ($signDisk.VolumeName.Equals("$VolumeName"))) {
            $result = $false
            Add-Member -memberType NoteProperty -InputObject $result -Name description -Value "Volume name of $letter`:\ is $($signDisk.VolumeName), must be $SignDriveLetter"
            return $result
        }
    } else {
        $result=$false
        Add-Member -memberType NoteProperty -InputObject $result -Name description -Value "Drive not found"
        return $result
    }
    if (-not (Test-Path "$Letter`:\")) {
        $result = $false
        $result.description="Drive $letter`:\ found, but no acces to it"
    }
    return $result
}

return $LogicalDrives