param(
    [Parameter(Mandatory=$true,
                 ValueFromPipeline=$true,
                 ValueFromPipelineByPropertyName=$true)]
    [string]$ComputerName,
    [string]$OutFile
    )

begin {
    Add-Content $OutFile "<table border=`"1px solid black`">"
}

process {
    $servshares = Get-WmiObject win32_share -ComputerName $ComputerName | Where-Object {-not ($_.name -match "\$")}
    $servLogicalDrives = Get-WmiObject win32_logicaldisk -ComputerName $ComputerName | Where-Object {$_.DriveType -eq 3} | Select-Object DeviceID,Size,FreeSpace
    Add-Content $OutFile ("<tr><td colspan=`"3`">{0}</td></tr>" -f $ComputerName)
    Add-Content $OutFile "<tr><td colspan=`"3`">Shares</td></tr>"
    Add-Content $OutFile "<tr><td>Share name</td><td>Share path</td><td>Description</td></tr>"
    foreach ($share in $servshares) {
        Add-Content $OutFile ("<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>" -f $share.Name,$share.Path,$share.Description)    
    }
        Add-Content $OutFile "<tr><td colspan=`"3`">Logical Drives</td></tr>"
        Add-Content $OutFile "<tr><td>Drive Letter</td><td>Size</td><td>Free Space</td></tr>"
    foreach ($logdrive in $servLogicalDrives) {
        Add-Content $OutFile ("<tr><td>{0}</td><td>{1:n2} Gb</td><td>{2:n2} Gb</td></tr>" -f $logdrive.DeviceID,($logdrive.Size/1GB),($logdrive.FreeSpace/1GB))
    }
    Add-Content $OutFile "<tr></tr>"
}

end {
    Add-Content $OutFile "</table>"
}
