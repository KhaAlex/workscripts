param(
[Parameter(Mandatory=$true)][string]$Source = $(throw "-Source is required"),
[Parameter(Mandatory=$true)][string]$Destination = $(throw "-Destination is required"),
[string]$filter = '*.*',
[string]$TaskName
)
if (-not ((Test-Path $Source -PathType Container) -and (Test-Path $Destination -PathType Container))){
    throw "Bad Destination or Source directory"
    exit
}

if (!$TaskName) {
    $eventsRegistered=Get-EventSubscriber
    [string]$TaskName=$eventsRegistered.Length+1
}

$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$command = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 

$ExSource = Join-Path $Source $filter
$expr=$command+" "+$ExSource+" "+$Destination

$fsw = New-Object IO.FileSystemWatcher $Source, $filter -Property @{IncludeSubdirectories = $false;NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'}

Register-ObjectEvent $fsw Created -SourceIdentifier $TaskName -Action {
    $name = $Event.SourceEventArgs.Name
    $changeType = $Event.SourceEventArgs.ChangeType
    $timeStamp = $Event.TimeGenerated
    $innerExpr=$Event.MessageData
    Write-Host "The file '$name' was $changeType at $timeStamp" -fore green
    Invoke-Expression $innerExpr
} -MessageData $expr

Register-ObjectEvent $fsw Error -SourceIdentifier "Error$TaskName" -Action {
    $name = $Event.SourceEventArgs.Name
    $timeStamp = $Event.TimeGenerated
    $Source = $Event.MessageData
    Write-Host "$name. Directory $Source is lost. Time:$timeStamp" -fore red
} -MessageData $Source
