
<#
  .SYNOPSIS
  Semi-automated archive to pst fired eplyees in OU script

  .DESCRIPTION
  Requres running in exchange powershell console(EMS - exchange management shell).
  Checks status of current export to pst operations, handle it, then start next export operation

  .PARAMETER OrganizationalUnit
  Organisational unit in AD

  .PARAMETER UNCPath
  UNC path to folder where export pst, pst would be <alias>.pst or <alias>_archive.pst

  .PARAMETER DomainController
  Some commands in script can't find alias without explicitly setted Domain Controller
  
  .EXAMPLE
  auto_fired_archive_pst.ps1 "ou=Dismissed,dc=domain,dc=local" \\emailarchive\pst DC01.domain.local

  .EXAMPLE
  To add as winodws scheduler task.
  Field "Program or script":
  C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe
  Field "Arguments":
  -command ".'c:\Program Files\Microsoft\Exchange Server\V15\bin\RemoteExchange.ps1'; Connect-ExchangeServer -auto; C:\scripts\auto_fired_archive_pst.ps1 -Verbose *>&1 | Tee-Object -FilePath C:\scripts\auto_fired_archive_pst.log -Append"

  Change path to RemoteExchange.ps1 to correct version (in my case \V15\)

#>

param (
  [parameter(Mandatory=$true,position=0)][string]$OrganizationalUnit= $(throw "-OU is required"),
  [parameter(Mandatory=$true,position=1)][string]$UNCPath= $(throw "-UNCPath is required"),
  [parameter(Mandatory=$true,position=2)][string]$DomainController= $(throw "-DomainController is required")
    )

$mailboxExportRequestStatistics=get-mailboxexportRequest | get-mailboxExportRequestStatistics
If (-not (Test-Path $UNCPath)) {
  throw "Wrong UNC path"
}

#exit if found error task or task in progress
if ($mailboxExportRequestStatistics | Where-Object -Property StatusDetail -notmatch Completed) {
  Write-Host "`[$(get-date -f yyyy-MM-ddTHH:mm:ss)`] Found not finished or error export request, script stops."
  exit
}

Set-ADServerSettings -ViewEntireForest $true
$mailbox=Get-Mailbox -OrganizationalUnit $OrganizationalUnit | Sort-Object -Property Alias | Select-Object -first 1

#looking for wip task
if ($finished = $mailboxExportRequestStatistics | Where-Object {($_.SourceAlias -eq $mailbox.Alias) -and ($_.sourceisarchive -eq $false) -and ($mailbox.ArchiveState -eq "Local")}) {
  Write-Verbose "[$(get-date -f yyyy-MM-ddTHH:mm:ss)`] Found completed non archive export request for $($mailbox.alias), start removing this request"
  Remove-MailboxExportRequest $finished.RequestGuid -Confirm:$false
  Write-Verbose "[$(get-date -f yyyy-MM-ddTHH:mm:ss)`] Creating new export request for archive mailbox"
  New-MailboxExportRequest -Name "$($mailbox.Alias)_archive" -Mailbox $mailbox.Alias -FilePath (Join-Path $UNCPath "$($mailbox.Alias)_archive.pst") -IsArchive -ConflictResolutionOption KeepTargetItem -DomainController $DomainController
  exit
} elseif ($finished = $mailboxExportRequestStatistics | Where-Object {($_.SourceAlias -eq $mailbox.Alias) -and (($_.sourceisarchive -eq $true) -or ($mailbox.ArchiveState -eq "None"))}) {
  Write-Verbose "`[$(get-date -f yyyy-MM-ddTHH:mm:ss)`] Found completed archive export request or request no-archive mailbox for $($mailbox.alias), start removing this request"
  Remove-MailboxExportRequest $finished.RequestGuid -Confirm:$false
  Write-Verbose "[$(get-date -f yyyy-MM-ddTHH:mm:ss)`] Disabling mailbox $($mailbox.alias), because it exported"
  Disable-Mailbox $mailbox -Confirm:$false
  Write-Verbose "[$(get-date -f yyyy-MM-ddTHH:mm:ss)`] Waiting 15 min for exchange apply changes"
  Start-Sleep -Seconds 900
  Write-Verbose "[$(get-date -f yyyy-MM-ddTHH:mm:ss)`] Looking for next mailbox in OU for export"
  $mailbox=Get-Mailbox -OrganizationalUnit $OrganizationalUnit | Sort-Object -Property Alias | Select-Object -first 1
}

if ($mailbox) {
  Write-Verbose "[$(get-date -f yyyy-MM-ddTHH:mm:ss)`] No export request in progress found starting handle next mailbox $($mailbox.Alias)"
  New-MailboxExportRequest -Name "$($mailbox.Alias)" -Mailbox $mailbox.Alias -FilePath (Join-Path $UNCPath "$($mailbox.Alias).pst") -ConflictResolutionOption KeepTargetItem -DomainController $DomainController
} else {
  Write-host "`[$(get-date -f yyyy-MM-ddTHH:mm:ss)`]No mailboxes for export"
}

