param(
    [Parameter(Mandatory=$False,Position=0)][string]$Path = (Join-Path (Split-Path -Path $MyInvocation.MyCommand.Definition -Parent) "*.xml"),
    [Parameter(Mandatory=$True,Position=1,ParameterSetName="Sign")][switch]$Sign,
    [Parameter(Mandatory=$False,Position=1,ParameterSetName="Crypt")][string]$Crypt = "7020",
    [Parameter(Mandatory=$True,Position=1,ParameterSetName="Check")][switch]$Check,
    [Parameter(Mandatory=$True,Position=1,ParameterSetName="CheckSeparateKaFile")][string]$CheckSeparateKaFile,
    [Parameter(Mandatory=$True,Position=1,ParameterSetName="Unsign")][switch]$Unsign,
    [Parameter(Mandatory=$True,Position=1,ParameterSetName="Uncrypt")][switch]$Uncrypt,
    [Parameter(Mandatory=$False,Position=2)][string]$LogPath = (Join-Path (Split-Path -Path $MyInvocation.MyCommand.Definition -Parent) "SCSign.log"),
    [Parameter(Mandatory=$False,Position=3)][switch]$Recurse

    )
$scriptDir=Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$scsign="`"C:\Program Files (x86)\SCSignEx\SCSignEx.exe`""
if (-not (Test-Path $scsign.Trim("`""))) {
    throw "No SCSignEx found. Impossible to execute script."
}
$tempdir=$env:temp #$scriptDir
if ($Recurse) {
        $forSend=Get-ChildItem $Path -Recurse
    } elseif (Test-Path $Path) {
        $forSend=Get-ChildItem $Path
    } else {
        Write-Host "No files found" -fore Red
        exit
    }
if ($forSend) {
    $fileList=Join-Path $tempdir "ProcFileList.lst"
    Set-Content $filelist $forSend.Fullname
    $argums = ""
    switch ($PsCmdlet.ParameterSetName) {
        Sign {
            $argums=("-s -l{0} -o{1} -gA:\ -iA:\ -b0" -f $fileList,$LogPath)    
        }
        Crypt {
            $argums=("-e -a{0} -l{1} -o{2} -gB:\ -iB:\ -b0" -f $Crypt,$fileList,$LogPath)
        }
        Uncrypt {
            $argums=("-d -l{0} -o{1} -gB:\ -iB:\ -b0" -f $fileList,$LogPath)
        }
        Unsign {
            $argums=("-r -l{0} -b0" -f $fileList) 
        }
        Check {
            $argums=("-c -l{0} -o{1} -gA:\ -iB:\ -b0" -f $fileList,$LogPath)
        }
        CheckSeparateKaFile {
            if ($forSend.Count -ne 1) {
                throw "No or more than one file for process, while comand is Check KA with separate file!"
            }
            if (Test-Path $CheckSeparateKaFile) {
                $kafile=Get-Item $CheckSeparateKaFile
            } else {
                throw "KA file not found."
            }
            $cfile = $forSend | Select-Object -first 1
            $argums=("-p -f{0} -k{1} -o{2} -b0" -f $cfile.Fullname,$kafile.Fullname,$LogPath)
        }
    }
    Start-Process $scsign $argums -wait 
    
    Remove-Item $filelist
}
