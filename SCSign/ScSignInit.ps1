<#

.SYNOPSIS
ScSign �� ��������� �� ����������� �������� ����� (asrkeyw) ������� �������������
������������� � ��� ���������� ��������� ��������, ��� ������������� ���������� ��������� ������ ������.
.DESCRIPTION

.EXAMPLE

.NOTES
Put some notes here.

#>

param(
    [Parameter(Mandatory=$False,Position=1)][string]$SignDriveLetter="D",
    [Parameter(Mandatory=$False,Position=2)][string]$SignDriveLabel,
    [Parameter(Mandatory=$False,Position=3)][string]$CryptoDriveLetter="E",
    [Parameter(Mandatory=$False,Position=4)][string]$CryptoDriveLabel,
    [Parameter(Mandatory=$False,Position=5)][string]$LogFile
    )
#init start

$executingScriptDirectory=Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1"
$GetLogicalDrives = Join-Path $executingScriptDirectory "GetLogicalDrives.ps1"
$scsign = Join-Path $executingScriptDirectory "SCSignCB.ps1"
$TestFile = Join-Path $executingScriptDirectory "test.txt"

$drives=&$GetLogicalDrives
$logger=&$GetLogger $LogFile -Screen
$lastLogLine=0
try {
    if (Test-Path $logger.LogFile -ErrorAction Stop) {
        $lastLogLine=(Get-Content $logger.LogFile).Count
    }
    } catch {}
#init end

#functions start

# ������� �������� ������� ������� ������ � ����, �� ������� ������
# ������� ���������� ���������� $lastLogLine � �������������
function Check-Log {
    $Errgx=[regex]"CODE=[1-9]\d{0,}"
    $errors=$false
    if (Test-Path $logger.LogFile) {
        $log=Get-Content $logger.LogFile
        $LogFile=$logger.LogFile
        if ($lastLogLine -lt $log.Count) {
            $newLines=$log[$lastLogLine..$log.Count]
            $matchs=$Errgx.Matches($newLines)
            if ($matchs.Success) {
                $logger.Write("���������� ������ �������, ��� ����������.","ERROR")
                $errors = $true
            }
        }
    }
    return $errors
}



#functions end

#script start
$checkSign = $drives.IsDeviceOnline($SignDriveLetter,$SignDriveLabel)
if (-not $checkSign) {
    $logger.Write($checkSign.description,"ERROR")
    return $false
}

$checkCrypt = $drives.IsDeviceOnline($CryptoDriveLetter,$CryptoDriveLabel)
if (-not $checkCrypt) {
    $logger.Write($checkCrypt.description,"ERROR")
    return $false
}

Remove-Item $TestFile -ErrorAction SilentlyContinue
Add-Content -Path $TestFile -Value "test"
for ($i=0;$i -lt 2;$i++) {
    try {
        Invoke-Expression ("$scsign -Path {0} -Sign -LogPath {1}" -f $TestFile,$logger.LogFile)
        Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Check" -f $TestFile,$logger.LogFile)
        Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Unsign" -f $TestFile,$logger.LogFile)
    } catch {
        $logger.Write("�� ������� ��������� ScSignEx","ERROR")
        $logger.Write($_,"ERROR")
        return $false
    }
    if ($i -eq 0) {
        try {
            Remove-Item $logger.LogFile -ErrorAction Stop
        } catch {
            $logger.Write("�������� � ���-������ ������ �������.","ERROR")
            return $false
        } 
        Start-Sleep -s 1
    }
}

if (Check-Log) {
    $logger.Write("�� ������� ���������������� ScSign","ERROR")
    return $false
}

return $true
#script end