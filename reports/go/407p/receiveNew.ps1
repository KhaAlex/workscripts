param (
    $workdir="R:\01_spb\407p\IN"
    $emailsFrom=$env:USERNAME+"@sovbank.ru",
    $emailsTo="_407p@sovbank.ru",
    $smtpServer="812-cas01.sovbank.ru",
    $emailsSubject="��������� � ������ ���������� (��������� 407-�)"
    )
$reglCode="407-�"
$packageMasks=@("oz*.772","fz*.772")
#��������� �������
$fzKvitsMask = "UVDIFM_044030772*.xml"
$fzArchRepMask = "ZRFM_044030772*.ARJ"
$fzRepMask = "PI_RFM_044030772*.xml"

$ozKvitsMask = "UVARHKRFM_044030772*.xml"
$ozArchMask = "RRFM_044030772*.ARJ"
$ozCryptedArchMask = "RFM_044030772*.zip"

#init block
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$verbaScript = Join-Path $executingScriptDirectory "verbaScr.ps1"
$askPopup = New-Object -ComObject Wscript.Shell #����� ��� ������ ������� Show-popup

# ������ ������� ����������� ����� ��-�� ����, ��� ��������� ��� ������������� ���������
# ����������
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path $workdir "arc"
$errdir=Join-Path $workdir "err"
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}
#init block end

#functions block
#������� ���������� ���������� ����, �� �������� ��� �������� $askPopup
function Show-Popup {
    param(
        $Text="",
        $PopupText=$reglCode,
        [switch]$Err
        )
    $pic=0x30
    if ($Err) {
        $pic = 0x10
    }
    $askPopup.Popup($Text,0,$PopupText,$pic) | Out-Null
}

function Send-Email {
    param(
        $msg,
        [switch]$HighPriority
        )
    $priority="Normal"
    if ($HighPriority) {
        $priority="High"
    }
    Send-MailMessage `
-to $emailsTo `
-from $emailsFrom `
-subject $emailsSubject `
-body $msg `
-smtpServer $smtpServer `
-priority $priority `
-encoding Unicode
}

function Extract-IncPackage {
    param(
        [string]$Path
        )
    $arcs=Get-ChildItem $Path -Recurse
    foreach ($arc in $arcs) {
        $arcParentDir=Split-Path $arc.FullName -Parent
        $dst=Join-Path $arcParentDir $arc.BaseName
        if (-not (Test-Path $dst -PathType Container)) {
            New-Item $dst -ItemType Directory -force | Out-Null
        }

        if ($arc.Extension.Equals(".772")) {
            Expand -R $arc.FullName $dst
        }
       
        if ($arc.Extension.ToLower().Equals(".arj")) {
            Start-Process $arj32 ("x {0} {1} -jv" -f $arc.FullName,$dst) -NoNewWindow -Wait
        }

        Remove-Item $arc
    }
}

function Parse-Reports {
    param (
        $reportsPath
    )
    $reps = Get-ChildItem $reportsPath -Recurse
    if ($reps) {
        $logger.Write("����������� ������ �� ��������������� ������")
        $isErrReps = $false
        foreach ($rep in $reps) {
            $logger.Write("������������� ����� $($rep.name)")
            [xml]$xmlc = Get-Content $rep -Encoding UTF8 | Select-String -Pattern '>$'
            $logger.Write($("����" $xmlc.KVIT.ES "������ � �����" $xmlc.KVIT.REZ_ES))
            if (($xmlc.KVIT.REZ_ES).Equals("0")) {
                $isErrReps = $true
            }
        }
        if ($isErrReps) {
            $popupText = "���� ��� ��������� ��������� ����������� �������������� �������"
            $logger.Write($popupText,"ERROR") 
            Show-Popup $popupText -Err
            $globalErrReps = $true
        }
    }
    return $globalErrReps
}