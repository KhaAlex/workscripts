﻿$WorkDir = "R:\01_spb\407p\OUT\"
$senddir= "K:\PTK PSD\Post\Post\ied"
$arcdir=Join-Path $workdir "arc"
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$verbaScript = Join-Path $executingScriptDirectory "verbaScr.ps1"
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1"
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$tempdir=Join-Path $workdir "temp"
$KvitsName = "KRFM*.xml"
$KvitsPath = Join-Path $WorkDir $KvitsName
$bik="044030772"

function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

function Make-ArjPacket {
    param(
        $arcBaseName,
        $files
        )
    $params="m -c -e -y {0}\{1}.arj {2}" -f $tempdir,$arcBaseName,$files
    Start-Process -FilePath $arj32 -ArgumentList $params -NoNewWindow -Wait
    return (Get-Item "$tempdir\$arcBaseName.arj")
}

function Get-PackageNumber {
    $idsFile = Join-Path (Get-CurArcDir) "id.txt"
    [int]$curId = 0
    if (Test-Path $idsFile) {
        [int]$lastId = Get-Content $idsFile -last 1
        $curId = $lastId+1
        Add-Content $idsFile $curId
    } else {
        $curId = 1
        #Вывод следующей команды выходит в return, Out-Null необходим для предотвращения такого поведения
        New-Item $idsFile -ItemType file -force | Out-Null
        Add-Content $idsFile $curId
    }
    return $curId
}

function Get-PacketDirName {
    $packnum = Get-PackageNumber
    if ($packnum -lt 1) {
        throw "Неверный номер посылки"
    }
    $formattedDate = Get-Date -f yyyyMMdd
    return "ARHKRFM_{0}_{1}_{2:D3}" -f $bik,$formattedDate,$packnum
}

$Kvits = Get-ChildItem $KvitsPath
if ($Kvits)
{
    if (-not (Test-Path $tempdir)) {
        New-Item $tempdir -ItemType Directory | Out-Null
    }
    $packetName = Get-PacketDirName
    $packetArcDir = Join-Path (Get-CurArcDir) $packetName
    $currArcDir = Get-CurArcDir
    if (-not (Test-Path $packetArcDir -PathType Container)) {
        New-Item $packetArcDir -ItemType Directory -force | Out-Null
    }
    Invoke-Expression "$SmartCopy $KvitsPath $packetArcDir"
    Move-Item $kvits $tempdir
    $kvitsForSend = Join-Path $tempdir $KvitsName
    Write-Host "Подписываем $kvitsForSend" -fore Yellow
    Invoke-Expression "$verbaScript -Path $kvitsForSend -Sign -Recurse"
    $arcForSend = Make-ArjPacket $packetName $kvitsForSend
    Write-Host "Подписываем $arcForSend" -fore Yellow
    Invoke-Expression "$verbaScript -Path $($arcForSend.FullName) -Sign"
    Copy-Item $arcForSend $currArcDir
    Move-Item $arcForSend $senddir
    Remove-Item $tempdir
}
