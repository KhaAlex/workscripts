$workdir = "R:\01_spb\407p\IN"
$userEmail = $env:USERNAME+"@sovbank.ru"
$notifReceiverEmail = "_407p@sovbank.ru"
set-location $workdir
$year = get-date -f yyyy
$month = get-date -f MM
$day = get-date -f dd
$utildir = "$workdir\work"
$tempdir = "$workdir\temp"
$currArcDir = "$workdir\ARC\$year\$month\$day"
$currErrDir = "$workdir\ERR\$year\$month\$day"
$verba ="c:\Program Files\MDPREI\��� �����-OW\FColseOW.exe"
$script:globalErrReps = $false
#���� �������������� � �������������
$askPopup = New-Object -ComObject Wscript.Shell
#��������� �������
$fzKvitsMask = "UVDIFM_044030772*.xml"
$fzArchRepMask = "ZRFM_044030772*.ARJ"
$fzRepMask = "PI_RFM_044030772*.xml"

$ozKvitsMask = "UVARHKRFM_044030772*.xml"
$ozArchMask = "RRFM_044030772*.ARJ"
$ozCryptedArchMask = "RFM_044030772*"

#������� ����������� � �����. ���� ����(����������� �� ����) ��� ���� � ������,
#������ �� ����������, ���� ��� �� ����������, ���� ���� ���� � ��� �� ������
#�� ������ ����� ��������� .new
function script:Copy-Arc($source, $destination)
{
    if ((Test-Path $destination) -and ((Get-FileHash $source).hash -ne (Get-FileHash $destination).hash))
        {
            Copy-Arc $source $destination".new"
        } elseif (-not (Test-Path $destination)){
            Copy-Item -Path $source -Destination $destination
        } else {
            Write-Host "���� ��� ��� ��������� � ����� �����"
        }
}

#������� �������� ���������� ��������, ���� ��� �� ����������.
#���� ������� ������������ ��������� �������, ����������� � ������������
#��� ��������, ��� �������������� ������ ���������� ������ �������
function script:Create-TempDir {
if (-not (Test-Path $tempdir)) {
    Write-Host "�������� ���������� ��� ��������� ������"
    New-Item $tempdir -ItemType Directory |Out-Null
} else {
    $popupText = "������� ��������� ���������� ���������� ��������� ������ `
������� �������� ��������� �����������. ���������� ���������� ������ �`
��������� ���� ��������� ������ ���������� ������?"
    Write-Host $popupText
    $ans = $askPopup.Popup($popupText,0,"������� ��������� ����������",0x121)
    switch ($ans) {
        1 {
            Write-Host "�������� ��������� ���������� ����������� ��������"
            Remove-Item $tempdir -Recurse -Force
            Write-Host "�������� ��������� ���������� ��� �������� ��������"
            New-Item $tempdir -ItemType Directory |Out-Null
        }
        2 {
            Write-Host "����������� ���������� �������"
            exit 3
        }
    }  
}    
}

#������� �������� ��� ����� � ��������������� ��������� $mask �� $workdir
#�� ��������� ����������, �������� ����� � �������� �����,
#����� ������������� �� ���� �� ����������� CAB(expand)
#� ����� ������� �������� ����� ������ ��������������� $mask
#���������� $true ���� � $work ���� $mask ����� $false
function script:Receive-CabPackage($mask) {
    Set-Location $workdir
    if (Test-Path $mask) {
        Write-Host "������� �������� ������� oz"
        $script:mess = Get-ChildItem $mask 
        foreach ($mes in $mess) {
            Write-Host $mes.name "����������� � �����"
            $dest= $currArcDir+"\"+$mes.name
            Copy-Arc $mes $dest
        }
    } else {
        return $false
    }
    
    foreach ($mes in $mess) {
        Copy-Item -Path $mes -Destination $tempdir
    }
    Set-Location $tempdir
    Write-Host "����������� ��� $mask ������ �� ��������� ����������"
    &expand $mask -R .
    Write-Host "��������� ��� $mask ����� �� ��������� ����������"
    Remove-Item $mask
    Set-Location $workdir
    return $true
}

#������� �������� ��������� �� �� � ������ $mask
function script:Parse-Kvits($mask) {
    if (Test-Path $mask) {
        $reps = Get-ChildItem $mask
        Write-Host "����������� ������ �� ��"
        $isErrReps = $false
        foreach ($rep in $reps) {
            Write-Host "������������� �����" $rep.name
            [xml]$xmlc = Get-Content $rep -Encoding UTF8
            Write-Host "����" $xmlc.UV.ARH $xmlc.UV.REZ_ARH
            if (($xmlc.UV.REZ_ARH).Trim(" ").Equals("������") -ne 0) {
                $isErrReps = $true
            }
        }
        if ($isErrReps) {
            $popupText = "���� ��� ��������� ��������� �������� � ���������� �������"
            Write-Host $popupText
            $askPopup.Popup($popupText,0,"���� ���������� ������",0x30) | Out-Null
            $globalErrReps = $true
        }
        Write-Host "����������� ����������� ������� �� �� � ������� ����������"
        Move-Item -Path $mask -Destination $workdir
        Write-Host "�������� ������� ������������� �������"
        
        Move-Item -Path $mask -Destination $workdir
    }   else {
        Write-Host "������� � ������ �� �� �� �������"
    }
    return $globalErrReps
}

################### ������� �������� ������� �� ��������������� ������ ###############
function script:Parse-Reports($mask) {
    if (Test-Path $mask) {
        $reps = Get-ChildItem $mask
        Write-Host "����������� ������ �� ��������������� ������"
        $isErrReps = $false
        foreach ($rep in $reps) {
            Write-Host "������������� �����" $rep.name
            [xml]$xmlc = Get-Content $rep -Encoding UTF8 | Select-String -Pattern '>$'
            Write-Host "����" $xmlc.KVIT.ES "������ � �����" $xmlc.KVIT.REZ_ES
            if (($xmlc.KVIT.REZ_ES).CompareTo("0") -ne 0) {
                $isErrReps = $true
            }
        }
        if ($isErrReps) {
            $popupText = "���� ��� ��������� ��������� ����������� �������������� �������"
            Write-Host $popupText
            $askPopup.Popup($popupText,0,"���� ������������� ���������",0x30) | Out-Null
            $globalErrReps = $true
        }
        Write-Host "����������� ����������� ������� �� ��������������� ������ � ������� ����������"
        Move-Item -Path $mask -Destination $workdir
        Write-Host "�������� ������� ������������� �������"
        Move-Item -Path $mask -Destination $workdir
    }   else {
        Write-Host "������� � ������ �� �� �� �������"
    }
    return $globalErrReps
}

function script:Ask-KeyPress {
    Write-Host "������� ����� ������� ��� ����������� ..."
    $x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
}

#begin Script

if (-not (Test-Path $currArcDir)) {
Write-Host "�������� ���������� ������ �� ����������� ����"
New-Item "$workdir\ARC\$year\$month\$day" -ItemType Directory |Out-Null
}

Create-TempDir
   
###################������ � oz*#####################
    
if (Receive-CabPackage oz*){
    Set-Location $tempdir
    
    ################### ������ � oz* ����������� ###############
    
    if (Parse-Kvits $ozKvitsMask) {
        $globalErrReps = $true
    }
    
    ##################### ������ � oz* �������� ################
    Set-Location $tempdir
    if (Test-Path $ozArchMask) {
        &$utildir\arj32.exe e $ozArchMask
        Remove-Item $ozArchMask
        Move-Item $ozCryptedArchMask $workdir
        
        Set-Location $workdir
    
        $cryptarcs = Get-ChildItem $ozCryptedArchMask
        $cryptarcsList = $cryptarcs.FullName -join "`n"
        Send-MailMessage -To $notifReceiverEmail `
        -from $userEmail `
        -Subject "�������� 407-�" `
        -Body "������ ����!`n `
����������� ������� �� `"407-�`" ��������� �����������.`n$cryptarcsList" `
        -SmtpServer caslb.sovbank.ru `
        -Encoding UTF8
    }
}

############################# ������ � fz* #################################
if (Receive-CabPackage fz*){
    Set-Location $tempdir
    ############################ ������ � fz* ����������� ######################
    if (Parse-Kvits $fzKvitsMask) {
        $globalErrReps = $true
    }
    ############################ ������ � fz* �������� �� ��������� ############
    Write-Host "���� ����� ������� �� ��������������� ������"
    if (Test-Path $fzArchRepMask) {
        &$utildir\arj32.exe e $fzArchRepMask
        Remove-Item $fzArchRepMask
        if (Parse-Reports $fzRepMask) {
            $globalErrReps = $true
        }
        Set-Location $workdir
    } else {
        Write-Host "������� �� ��������������� ������ �� �������"
        Write-Host $fzArchRepMask
    }
}
    
#############################
if (Test-Path $tempdir\*.*) {
    $popupText = "������� ����� �� ��������������� �������, ���������� �� �����"
    Write-Host $popupText
    $askPopup.Popup($popupText,0,"�������� ��� ��������� �����",0x30) | Out-Null
    Set-Location $tempdir
    if (-not(Test-Path $currErrDir)) {
        Write-Host "�������� ���������� ��� �������������"
        New-Item $currErrDir -ItemType Directory | Out-Null
    }
    $mess=Get-ChildItem
    foreach ($mes in $mess) {
        Write-Host $mes.name "����������� � �������������"
        $dest= $currErrDir+"\"+$mes.name
        Copy-Arc $mes $dest
        Remove-Item $mes
        }
    Set-Location $workdir
}

Write-Host "������ ��������� ������� ���������"

Set-Location $workdir
Remove-Item $tempdir
Remove-Item oz*
Remove-Item fz*
if ($globalErrReps) {
    Ask-KeyPress
}

#end