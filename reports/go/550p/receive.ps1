param (
    $workdir="R:\01_spb\550p\IN"
    )

#init block
$reglCode="550-�"
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$verbaScript = Join-Path $executingScriptDirectory "verbaScr.ps1"
$arc7z = Join-Path $executingScriptDirectory "7z.exe"
$askPopup = New-Object -ComObject Wscript.Shell

# ������ ������� ����������� ����� ��-�� ����, ��� ��������� ��� ������������� ���������
# ����������
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path $workdir "arc"
$errdir=Join-Path $workdir "err"
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}
$PackageMask="CB_550P_*_*.arj"
$FilesMask="CB_ES550P_*_*.xml"

if (-not (Test-Path $arcdir)) {
    New-Item $arcdir -ItemType Directory | Out-Null
}
#init block end

#functions block

function Show-Popup {
    param(
        $Text="",
        $PopupText="$reglCode",
        [switch]$Err
        )
    $pic=0x30
    if ($Err) {
        $pic = 0x10
    }
    $askPopup.Popup($Text,0,$PopupText,$pic) | Out-Null
}

# ������� ����� ��������� �������� �������, ������� ������� SmartCopyFile.ps1 � ����������
# �� �������� � ������� �����������
function Copy-ArcsToArchive {
    param(
        $arcs
        )
    $currArcDir=Get-CurArcDir
    if (-not (Test-Path $currArcDir -PathType Container)) {
        New-Item $currArcDir -ItemType Directory -force | Out-Null
    }
    foreach ($arc in $arcs) {
        Invoke-Expression "$SmartCopy $arc $currArcDir"
        #DOTO what if some not copied
    }
}

# ������� ��������� ���������� �� � ������ ����� � ��������� ��� � ��������� �����
# ������������ � Extract-IncPackage
function Check-Arc {
    param (
        $arc
        )
    $currArcDir=Get-CurArcDir
    $CheckinArc=Join-Path $currArcDir $arc.Name
    return (Test-Path $CheckinArc -PathType Leaf)
}

# ������� ��������� �������� ���� � ����� � ��� ������
# � ������ -Archive �� ��������� ���������� �� ��������� ������������ �������� �����
# ��� ������������ �������� � �����.
function Extract-IncPackage {
    param(
        [string]$Mask,
        [switch]$Archive
        )
    $arcsPath=Join-Path $tempdir $Mask
    $arcs=Get-ChildItem $arcsPath -Recurse
    foreach ($arc in $arcs) {
        $arcParentDir=Split-Path $arc.FullName -Parent
        $dst=Join-Path $arcParentDir $arc.BaseName.ToUpper()
        if (-not (Test-Path $dst -PathType Container)) {
            New-Item $dst -ItemType Directory -force | Out-Null
        }

        if ($arc.Extension.Equals(".772")) {
            Expand -R $arc.FullName $dst
        }
       
        if ($arc.Extension.ToLower().Equals(".arj")) {
            Start-Process $arj32 ("x {0} {1} -jv" -f $arc.FullName,$dst) -NoNewWindow -Wait
        }

        # �������������� ��������, ���� ����� ��������� ����� ��� � ������
        # �� �������� ���� �� ���������
        if ((Check-Arc $arc) -and ($Archive)) {
            Remove-Item $arc
        } elseif (-not $Archive) {
            Remove-Item $arc
        }
    }
}

# ��������� ��������� ������� � �����, � �������� ��������� ����������.
function Prepare-IncPackage {
    $received=$false
    $arcsPath=Join-Path $workdir $PackageMask
    $arcs=Get-ChildItem $arcsPath
    if ($arcs) {
        Copy-ArcsToArchive $arcs
        if (-not (Test-Path $tempdir)) {
            New-Item $tempdir -ItemType Directory | Out-Null
        }
        Move-Item $arcs $tempdir -Force
        $received=$true
    }
    return $received
}

# ������� ������� ��� ������������ ������ ������ ��� ���������� ���
# ������ ������� SCSignEx. ���������� SCSign-FileList
function UnVerba-Files {
    param (
        $Mask,
        [switch]$Decrypt,
        [switch]$Recurse
        )
    $incsPath= Join-Path $tempdir $Mask
    $ActionParam = "-Unsign"
    $RecurseParam = ""
    if ($Decrypt) {$ActionParam = "-Uncrypt"}
    if ($Recurse) {$RecurseParam = "-Recurse"}
    Invoke-Expression ("$verbaScript -Path $incsPath {0} {1}" -f $ActionParam,$RecurseParam)
}

function Spec-Unzip{
    param (
        $mask
        )
    $specPath=Join-Path $tempdir $mask
    $specArcs = Get-ChildItem $specPath -Recurse
    foreach ($specArc in $specArcs) {
        Start-Process $arc7z ("x {0} -o{1}" -f $specArc.FullName, $specarc.Directory.FullName) -NoNewWindow -Wait
    }
    Remove-Item $specArcs
    Get-ChildItem $tempdir\* -Recurse | Where-Object {($_.Extension.Equals("")) -and (!$_.PSIsContainer)} | Rename-Item -NewName {$_.BaseName+".xml"}
}

#functions block end

#script block
if (Prepare-IncPackage)
{
    Extract-IncPackage $PackageMask -Archive
    UnVerba-Files $PackageMask -Recurse
    UnVerba-Files $FilesMask -Decrypt -Recurse
    Spec-Unzip $FilesMask
    UnVerba-Files $FilesMask -Recurse
    #$incs=Get-ChildItem (Join-Path $tempdir $FilesMask) -Recurse | Rename-Item -NewName {$_.BaseName+".xml"} -PassThru | Copy-Item -Destination $workdir -PassThru
    Copy-Item $tempdir\* -Destination $workdir -Recurse
    if ($incs) {
        Show-Popup "��������� ��������� �� ���������� ������ �� $reglCode. ���������� � ����� $workdir"
    }
    try {
        Move-Item $tempdir\* -Destination (Get-CurArcDir) -Force -ErrorAction Stop
    } catch {
        Show-Popup "�� ������� ����������� ������ �� ��������� ���������� � �����. ���������� � ����� err" -Err
        if (-not (Test-Path $errdir)) {
            New-Item $errdir -ItemType Directory | Out-Null
        }
        Move-Item $tempdir\* -Destination $errdir\
    }
    Remove-Item $tempdir
}

#script block end
