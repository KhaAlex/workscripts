param (
    $workdir="R:\01_spb\550p\OUT",
    $senddir="K:\PTK PSD\Post\Post\ied"
    )
#init block
$reglCode="550-�"
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$verbaScript = Join-Path $executingScriptDirectory "verbaScr.ps1"

function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path $workdir "arc"
$errdir=Join-Path $workdir "err"
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}
$arcFolderRegexpMask = "ARH550P_0558_0000_\p{Nd}{8}_\p{Nd}{3}"

if (-not (Test-Path $arcdir)) {
    New-Item $arcdir -ItemType Directory | Out-Null
}
#init block end

#functions block
function Make-ArjPacket {
    param(
        $arcBaseName,
        $files
        )
    $params="m -c -e -y {0}\{1}.arj {2}" -f $tempdir,$arcBaseName,$files
    Start-Process -FilePath $arj32 -ArgumentList $params -NoNewWindow -Wait
    return (Get-Item (Join-Path $tempdir "$arcBaseName.arj"))
}

function Show-Popup {
    param(
        $Text="",
        $PopupText="$reglCode",
        [switch]$Err
        )
    $askPopup = New-Object -ComObject Wscript.Shell
    $pic=0x30
    if ($Err) {
        $pic = 0x10
    }
    $askPopup.Popup($Text,0,$PopupText,$pic) | Out-Null
    Remove-Variable askPopup
}

function Copy-ArcsToArchive {
    param(
        $arcs
        )
    $currArcDir=Get-CurArcDir
    if (-not (Test-Path $currArcDir -PathType Container)) {
        New-Item $currArcDir -ItemType Directory -force | Out-Null
    }
    foreach ($arc in $arcs) {
        Invoke-Expression "$SmartCopy $arc $currArcDir"
        #DOTO what if some not copied
    }
}

#functions block end

#script block
$foldersForSend=(Get-ChildItem $workdir -Directory | Where-Object {$_ -match $arcFolderRegexpMask}).FullName
if ($foldersForSend) {
    if (-not (Test-Path $tempdir)) {
        New-Item $tempdir -ItemType Directory | Out-Null
    }
    If (-not (Test-Path (Get-CurArcDir))) {
        New-Item (Get-CurArcDir) -ItemType Directory | Out-Null   
    }
    Write-Host $foldersForSend
    Copy-Item $foldersForSend -Destination (Get-CurArcDir) -Container -Recurse -ErrorAction Stop
    $forArcFolders = Move-Item $foldersForSend -Destination $tempdir -PassThru -ErrorAction Stop
    foreach ($folder in $forArcFolders) {
        Make-ArjPacket $folder.Name (Join-Path $folder.FullName "*.xml") | Out-Null
        Remove-Item $folder -verbose
    }
    $arcs = Join-Path $tempdir "*.arj"
    Write-Host "����������� ������" -fore Yellow
    Invoke-Expression ("$verbaScript -Path {0} -Sign" -f $arcs)
    Copy-ArcsToArchive (Get-ChildItem $arcs)
    try {
        Move-Item $arcs $senddir -ErrorAction Stop
    } catch {
        $mess="�� ������� ����������� ���� � $senddir."
        Show-Popup $mess -Err
        $logger.Write($mess, "ERROR")
    }
    Remove-Item $tempdir
}
