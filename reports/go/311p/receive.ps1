param (
    $workdir="R:\01_spb\311p\IN"
    )
$reglCode="311-�"
$dir211=Join-Path "R:\01_spb\211p\IN" (Get-Date -f yyyy\\MM\\dd)
$PackageMask="2z*.772"
$arjMasks=@("ON30772*.ARJ","NN30772*.ARJ","S30772*.arj","Y30772*.ARJ")
$incsMask="*.*"
$inc211Mask="ZA??4030772*.TXT"
$uv211Mask="UVZ30772*.xml"
$uvMask = "UV?N30772*.xml"

#init block
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$verbaScript = Join-Path $executingScriptDirectory "verbaScr.ps1"
$askPopup = New-Object -ComObject Wscript.Shell #����� ��� ������ ������� Show-popup

# ������ ������� ����������� ����� ��-�� ����, ��� ��������� ��� ������������� ���������
# ����������
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path $workdir "arc"
$errdir=Join-Path $workdir "err"
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}
#init block end

#functions block
#������� ���������� ���������� ����, �� �������� ��� �������� $askPopup
function Show-Popup {
    param(
        $Text="",
        $PopupText=$reglCode,
        [switch]$Err
        )
    $pic=0x30
    if ($Err) {
        $pic = 0x10
    }
    $askPopup.Popup($Text,0,$PopupText,$pic) | Out-Null
}

function Extract-IncPackage {
    param(
        [string]$Path
        )
    $arcs=Get-ChildItem $Path -Recurse
    foreach ($arc in $arcs) {
        $arcParentDir=Split-Path $arc.FullName -Parent
        $dst=Join-Path $arcParentDir $arc.BaseName
        if (-not (Test-Path $dst -PathType Container)) {
            New-Item $dst -ItemType Directory -force | Out-Null
        }

        if ($arc.Extension.Equals(".772")) {
            Expand -R $arc.FullName $dst
        }
       
        if ($arc.Extension.ToLower().Equals(".arj")) {
            Start-Process $arj32 ("x {0} {1} -jv" -f $arc.FullName,$dst) -NoNewWindow -Wait
        }

        Remove-Item $arc
    }
}

function Parse-UVs {
    param (
        $UVs
    )
    foreach ($UV in $UVs) {
        [xml]$UVxml = Get-Content $UV
        $message = "�������� ����������� � ������ {0}, � ��� ��� ����� {1} �� {2} ��������� �� ��������: {3}" -f `
        $UV.Name,$UVxml.UV.ARH,$UVxml.UV.DATE_ARH,$UVxml.UV.REZ_ARH
        $logger.Write($message)
        Show-Popup $message
        Move-Item (Split-Path $UV.FullName -Parent) (Get-CurArcDir)
    }
}

#functions block end

#script block
$incs = Get-ChildItem (Join-Path $workdir $PackageMask)
if ($incs) {
    $currArcDir = Get-CurArcDir
    
    if (-not (Test-Path $currArcDir)) {
        New-Item $currArcDir -ItemType Directory -force -ErrorAction Stop | Out-Null
    }
    
    foreach ($inc in $incs) {
        Invoke-Expression "$SmartCopy $inc $currArcDir"
    }

    if (-not (Test-Path $tempdir)) {
        New-Item $tempdir -ItemType Directory | Out-Null
    }

    Move-Item $incs $tempdir -ErrorAction Stop
    Extract-IncPackage (Join-Path $tempdir $PackageMask)
    foreach ($arjmask in $arjMasks) {
        Extract-IncPackage (Join-Path $tempdir $arjmask)
    }
    $UVs=Get-ChildItem (Join-Path $tempdir $uvMask) -Recurse
    if ($UVs) {
        Parse-UVs $UVs
    }

    $UV211s = Get-ChildItem (Join-Path $tempdir $uv211Mask) -Recurse
    if ($UV211s) {
        if (-not (Test-Path $dir211)) {
            New-Item $dir211 -ItemType Directory | Out-Null
        }
        Copy-Item $UV211s -Destination $dir211
        Move-Item $UV211s -Destination $currArcDir
        $message = "�������� ��������� �� 211-� ���������� � $dir211"
        $logger.Write($message)
        Show-Popup $message
    }

    Invoke-Expression ("$verbaScript -Path {0} -Unsign -Recurse" -f (Join-Path $tempdir $incsMask))
    $inc211 = Get-ChildItem (Join-Path $tempdir $inc211Mask) -Recurse 
    if ($inc211) {
        if (-not (Test-Path $dir211)) {
            New-Item $dir211 -ItemType Directory | Out-Null
        }
        Copy-Item $inc211 -Destination $dir211
        $message = "�������� ��������� �� 211-� ���������� � $dir211"
        $logger.Write($message)
        Show-Popup $message
    }
    Get-ChildItem (Join-Path $tempdir $incsMask) -Exclude $uvMask,$inc211Mask -Recurse | Copy-Item -Destination $workdir
    Move-Item $tempdir\* $currArcDir -ErrorAction Stop
    Remove-Item $tempdir
}
