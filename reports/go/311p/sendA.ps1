param (
    $workdir="R:\01_spb\311p\OUT\A",
    $senddir="K:\PTK PSD\Post\Post\ied",
    $cryptTo='2010941009;3978941009',
    $bik="30772"
    )
$reglCode="311-�"
$outsMask="SBC*.xml"

#init block
$resExistErr=[System.Management.Automation.ErrorCategory]::ResourceExists
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$verbaScript = Join-Path $executingScriptDirectory "verbaScr.ps1"
$askPopup = New-Object -ComObject Wscript.Shell #����� ��� ������ ������� Show-popup

# ������ ������� ����������� ����� ��-�� ����, ��� ��������� ��� ������������� ���������
# ����������
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path (Split-Path $workdir -Parent) "arc"
$errdir=Join-Path $workdir "err"
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}
#init block end

#functions block
#������� ���������� ���������� ����, �� �������� ��� �������� $askPopup
function Show-Popup {
    param(
        $Text="",
        $PopupText=$reglCode,
        [switch]$Err
        )
    $pic=0x30
    if ($Err) {
        $pic = 0x10
    }
    $askPopup.Popup($Text,0,$PopupText,$pic) | Out-Null
}

function Get-PackageNumber {
    $idsFile = Join-Path (Get-CurArcDir) "id.txt"
    [int]$curId = 0
    if (Test-Path $idsFile) {
        [int]$lastId = Get-Content $idsFile -last 1
        $curId = $lastId+1
        Add-Content $idsFile $curId
    } else {
        $curId = 1
        #����� ��������� ������� ������� � return, Out-Null ��������� ��� �������������� ������ ���������
        New-Item $idsFile -ItemType file | Out-Null
        Add-Content $idsFile $curId
    }
    return $curId
}

function Get-PacketDirName {
    $packnum = Get-PackageNumber
    if ($packnum -lt 1) {
        throw "�������� ����� �������"
    }
    return "AN{0}{1}{2:D4}" -f $bik,(Get-Date -f yyMMdd),$packnum
}

function Make-ArjPacket {
    param(
        $arcBaseName,
        $files
        )
    $params="m -c -e -y {0}\{1}.arj {2}" -f $tempdir,$arcBaseName,$files
    Start-Process -FilePath $arj32 -ArgumentList $params -NoNewWindow -Wait
    return (Get-Item "$tempdir\$arcBaseName.arj")
}

#functions block end

#script block
$outs = Get-ChildItem (Join-Path $workdir $outsMask) | Select-Object -First 1999
if ($outs) {
    $currArcDir = Get-CurArcDir
    try {
        New-Item $currArcDir -ItemType Directory -ErrorAction stop |Out-Null
    } catch  {
        if ($_.CategoryInfo.Category -ne $resExistErr) {
            $logger.Write("�������������� ������ ��� �������� ����� ������ $currArcDir","ERROR")
            exit
        }
    }

    $packetName = Get-PacketDirName
    $packetArcDir = Join-Path $currArcDir $packetName
    try {
        New-Item $packetArcDir -ItemType Directory -ErrorAction stop |Out-Null
    } catch  {
        if ($_.CategoryInfo.Category -eq $resExistErr) {
            $logger.Write("���������� ������� � ������ ��� ������� $packetArcDir","WARNING")
        } else {
            $_;exit
        }
    }
    
    foreach ($outFile in $outs) {
        Invoke-Expression "$SmartCopy $outFile $packetArcDir"
    }
    
    $packetTempDir=Join-Path $tempdir $packetName
    try {
        New-Item $packetTempDir -ItemType Directory -Force -ErrorAction stop |Out-Null
    } catch  {
        if ($_.CategoryInfo.Category -ne $resExistErr) {
            $logger.Write("�������������� ������ ��� �������� ���������� ���������� $packetTempDir","ERROR")
            exit
        }
    }
    Move-Item $outs $packetTempDir
    $filesInPacket = Join-Path $packetTempDir $outsMask
    Invoke-Expression "$verbaScript -Path $filesInPacket -Sign"
    Invoke-Expression "$verbaScript -Path $filesInPacket -Crypt $cryptTo"
    $packetSend = Make-ArjPacket $packetName $filesInPacket
    Invoke-Expression "$SmartCopy $packetSend $currArcDir" 
    Move-Item $packetSend $senddir
    Remove-Item $tempdir -Recurse
}

#script block end
