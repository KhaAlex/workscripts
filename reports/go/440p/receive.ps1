param (
    $workdir="R:\01_spb\440p\IN"
    )

#init block
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$verbaScript = Join-Path $executingScriptDirectory "verbaScr.ps1"
$askPopup = New-Object -ComObject Wscript.Shell

# ������ ������� ����������� ����� ��-�� ����, ��� ��������� ��� ������������� ���������
# ����������
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path $workdir "arc"
$errdir=Join-Path $workdir "err"
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}
$PackageMask="*.772"

if (-not (Test-Path $tempdir)) {
    New-Item $tempdir -ItemType Directory | Out-Null
}
if (-not (Test-Path $arcdir)) {
    New-Item $arcdir -ItemType Directory | Out-Null
}
#init block end

#functions block

function Show-Popup {
    param(
        $Text="",
        $PopupText="440-�",
        [switch]$Err
        )
    $pic=0x30
    if ($Err) {
        $pic = 0x10
    }
    $askPopup.Popup($Text,0,$PopupText,$pic) | Out-Null
}

# ������� ����� ��������� �������� �������, ������� ������� SmartCopyFile.ps1 � ����������
# �� �������� � ������� �����������
function Copy-ArcsToArchive {
    param(
        $arcs
        )
    $currArcDir=Get-CurArcDir
    if (-not (Test-Path $currArcDir -PathType Container)) {
        New-Item $currArcDir -ItemType Directory -force | Out-Null
    }
    foreach ($arc in $arcs) {
        Invoke-Expression "$SmartCopy $arc $currArcDir"
        #DOTO what if some not copied
    }
}

# ������� ��������� ���������� �� � ������ ����� � ��������� ��� � ��������� �����
# ������������ � Extract-IncPackage
function Check-Arc {
    param (
        $arc
        )
    $currArcDir=Get-CurArcDir
    $CheckinArc=Join-Path $currArcDir $arc.Name
    return (Test-Path $CheckinArc -PathType Leaf)
}

# ������� ��������� �������� ���� � ����� � ��� ������
# � ������ -Archive �� ��������� ���������� �� ��������� ������������ �������� �����
# ��� ������������ �������� � �����.
function Extract-IncPackage {
    param(
        [string]$Mask,
        [switch]$Archive
        )
    $arcsPath=Join-Path $tempdir $Mask
    $arcs=Get-ChildItem $arcsPath -Recurse
    foreach ($arc in $arcs) {
        $arcParentDir=Split-Path $arc.FullName -Parent
        $dst=Join-Path $arcParentDir $arc.BaseName
        if (-not (Test-Path $dst -PathType Container)) {
            New-Item $dst -ItemType Directory -force | Out-Null
        }

        if ($arc.Extension.Equals(".772")) {
            Expand -R $arc.FullName $dst
        }
       
        if ($arc.Extension.ToLower().Equals(".arj")) {
            Start-Process $arj32 ("x {0} {1} -jv" -f $arc.FullName,$dst) -NoNewWindow -Wait
        }

        # �������������� ��������, ���� ����� ��������� ����� ��� � ������
        # �� �������� ���� �� ���������
        if ((Check-Arc $arc) -and ($Archive)) {
            Remove-Item $arc
        } elseif (-not $Archive) {
            Remove-Item $arc
        }
    }
}

# ��������� ��������� ������� � �����, � �������� ��������� ����������.
function Prepare-IncPackage {
    $received=$false
    $arcsPath=Join-Path $workdir $PackageMask
    $arcs=Get-ChildItem $arcsPath
    if ($arcs) {
        Copy-ArcsToArchive $arcs
        Move-Item $arcs $tempdir -Force
        $received=$true
    }
    return $received
}

# ������� ������� ��� ������������ ������ ������ ��� ���������� ���
# ������ ������� SCSignEx. ���������� SCSign-FileList
function UnVerba-Files {
    param (
        $Mask,
        [switch]$Decrypt,
        [switch]$Recurse
        )
    $incsPath= Join-Path $tempdir $Mask
    $ActionParam = "-Unsign"
    $RecurseParam = ""
    if ($Decrypt) {$ActionParam = "-Uncrypt"}
    if ($Recurse) {$RecurseParam = "-Recurse"}
    Invoke-Expression ("$verbaScript -Path $incsPath {0} {1}" -f $ActionParam,$RecurseParam)
}

# ������� �������� ���1. ��������� ������� � ������� � ���,
# ���� ������� ������ ��������� �� email
function Parse-Izvs {
    $izvsPath= Join-Path $tempdir "IZV*.xml"
    $izvs=Get-ChildItem $izvsPath -Recurse
    foreach ($izv in $izvs) {
        $izvName=$izv.Name
        [xml]$izvXML=Get-Content $izv
        $FileNameinIzv=$izvXML.����.����������.��������
        $izvStatus=$izvXML.����.����������.���������
        if ([int]$izvXML.����.����������.�������������� -eq 1) {
            $logger.Write("���� $FileNameinIzv ������. ���1:$izvName")
            Show-Popup "�������� ��������� � ���, ��� ������������ ���� $FileNameinIzv ������ ��. ���1:$izvName"
        } else {
            $logger.Write("���� $FileNameinIzv �� ������. ���1:$izvName. ��������� �������� $izvStatus","WARNING")
            Show-Popup "���� $FileNameinIzv �� ������. ���1:$izvName`n��������� ��������: $izvStatus" -Err
        }
        $izvParent=Split-Path $izv.FullName -Parent
        Move-Item $izvParent (Get-CurArcDir) -Force
    }
}

#functions block end

#script block
if (Prepare-IncPackage)
{
    Extract-IncPackage $PackageMask -Archive
    UnVerba-Files "*.arj" -Recurse
    Extract-IncPackage "AFN*.arj"
    UnVerba-Files "*.vrb" -Decrypt -Recurse
    Get-ChildItem "$tempdir\*.vrb" -Recurse | Rename-Item -NewName {$_.BaseName+".xml"}
    UnVerba-Files "*.xml" -Recurse
    Parse-Izvs
    $incs=Get-ChildItem "$tempdir\*.xml" -Recurse | Rename-Item -NewName {$_.BaseName+".xml"} -PassThru | Copy-Item -Destination $workdir -PassThru
    if ($incs) {
        Show-Popup "��������� ��������� �� ���������� ������ �� 440-�. ���������� � ����� ��� ��������� � ���"
    }
    try {
        Move-Item $tempdir\* -Destination (Get-CurArcDir) -Force -ErrorAction Stop
    } catch {
        Show-Popup "�� ������� ����������� ������ �� ��������� ���������� � �����. ���������� � ����� err" -Err
        if (-not (Test-Path $errdir)) {
            New-Item $errdir -ItemType Directory | Out-Null
        }
        Move-Item $tempdir\* -Destination $errdir\
    }
    Remove-Item $tempdir
}

#script block end
