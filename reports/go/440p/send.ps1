param (
    $workdir="R:\01_spb\440p\OUT",
    $senddir="K:\PTK PSD\Post\Post\ied",
    $cryptTo="2010941009;3978941009",
    $bik="4030772"
    )

#init block
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1"
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$verbaScript = Join-Path $executingScriptDirectory "verbaScr.ps1"
$askPopup = New-Object -ComObject Wscript.Shell
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1"
$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path $workdir "arc"
$errdir=Join-Path $workdir "err"
$xmlToVrbMasks=@("BOS*.xml","BNS*.xml","BNP*.xml","BUV*.xml","BZ1*.xml","BV*.xml")
$forSendMask="*.xml"

function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
#init block end

if (-not (Test-Path $tempdir)) {
    New-Item $tempdir -ItemType Directory | Out-Null
}
if (-not (Test-Path $arcdir)) {
    New-Item $arcdir -ItemType Directory | Out-Null
}

#functions block

function Show-Popup {
    param(
        $Text="",
        $PopupText="440-�",
        [switch]$Err
        )
    $pic=0x30
    if ($Err) {
        $pic = 0x10
    }
    $askPopup.Popup($Text,0,$PopupText,$pic) | Out-Null
}

function Get-PackageNumber {
    $idsFile = Join-Path (Get-CurArcDir) "id.txt"
    [int]$curId = 0
    if (Test-Path $idsFile) {
        [int]$lastId = Get-Content $idsFile -last 1
        $curId = $lastId+1
        Add-Content $idsFile $curId
    } else {
        $curId = 1
        #����� ��������� ������� ������� � return, Out-Null ��������� ��� �������������� ������ ���������
        New-Item $idsFile -ItemType file | Out-Null
        Add-Content $idsFile $curId
    }
    return $curId
}

function Get-PacketDirName {
    $packnum = Get-PackageNumber
    if ($packnum -lt 1) {
        throw "�������� ����� �������"
    }
    $formattedDate = Get-Date -f yyyyMMdd
    return "AFN_{0}_MIFNS00_{1}_{2:D5}" -f $bik,$formattedDate,$packnum
}

function Copy-MessToArcive {
    param(
        $files
        )
    $currArcDir=Get-CurArcDir
    if (-not (Test-Path $currArcDir -PathType Container)) {
        New-Item $currArcDir -ItemType Directory -force | Out-Null
    }
    # Get-PacketDirName ������� ����� $currArcDir ��� ������
    $packageName = Get-PacketDirName
    $currArcPacketDir=Join-Path $currArcDir $packageName
    if (-not (Test-Path $currArcPacketDir)) {
            New-Item $currArcPacketDir -ItemType Directory | Out-Null
        }
    foreach ($file in $files) {
        $fileName=$file.FullName
        Invoke-Expression "$SmartCopy $fileName $currArcPacketDir"
        #DOTO what if some not copied
    }
    return $packageName
}

function Prepare-ForSend {
    $filesForSend = Get-ChildItem (Join-Path $workdir $forSendMask) | Select-Object -First 1999
    if ($filesForSend) {
        $packetName = Copy-MessToArcive $filesForSend
        Move-Item $filesForSend $tempdir -Force
    }
    return $packetName
}

function Make-ArjPacket {
    param(
        $arcBaseName,
        $files
        )
    $params="m -c -e -y {0}\{1}.arj {2}" -f $tempdir,$arcBaseName,$files
    Start-Process -FilePath $arj32 -ArgumentList $params -NoNewWindow -Wait
    return (Get-Item (Join-Path $tempdir "$arcBaseName.arj"))
}

#functions block end

#script block
while (Test-Path (Join-Path $workdir $forSendMask))
{
    $ArjPacketName = Prepare-ForSend
    if ($ArjPacketName) {
        $logger.Write("�������� ��� ������� $ArjPacketName")
    }
    $signPath = Join-Path $tempdir $forSendMask
    Write-Host "������� $signPath" -fore Yellow
    Invoke-Expression "$verbaScript -Path $signPath -Sign"
    $toVrb=$xmlToVrbMasks| ForEach-Object {Join-Path $tempdir $_}|Get-ChildItem 
    $toVrb|Rename-Item -NewName {$_.BaseName+".VRB"}
    $cryptPath=Join-Path $tempdir "*.vrb"
    Write-Host "���������� $cryptPath �� $cryptTo" -fore Yellow
    Invoke-Expression "$verbaScript -Path $cryptPath -Crypt $cryptTo"
    $arjPacket = Make-ArjPacket $ArjPacketName ("{0}\*.VRB {0}\{1}" -f $tempdir,$forSendMask)
    $signArj = Join-Path $tempdir $arjPacket.Name
    Write-Host "������� $signArj" -fore Yellow
    Invoke-Expression "$verbaScript -Path $signArj -Sign"
    Invoke-Expression ("{0} {1} {2}" -f $SmartCopy,$signArj,(Get-CurArcDir))
    if (Test-Path $senddir) {
        Move-Item $signArj $senddir
        Show-Popup "���� ��������� �� �������� � $senddir."
    } else {
        Move-Item $signArj $workdir
        Show-Popup "���������� ��� �������� $senddir ����������.`n�������������� ���� �������� � $workdir." -Err
    }
    
}

Remove-Item $tempdir

#script block end
