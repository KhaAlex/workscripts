param (
    $workdir="R:\06_mf\390p\TEST\IN",
    $emailsFrom="cbmf_info@sovbank.ru",
    $emailsTo="a.kharkovskij@sovbank.ru", #_390pMF@sovbank.ru
    $smtpServer="812-cas01.sovbank.ru",
    $emailsSubject="��������� � ������ ���������� (��������� 390-�)"
    )
$bic="4525540"

#$arcFile �������� ���� - ����, �������������� � ������� ��������� ���������� ARJ32
#� ���������� ������������� ����� � ��������� ���������, ��������������� ������ ����� (������� �����),
#���������������� ���������� ����� ������ (��� ������������� ����� ������, ����������� � �� �������),
#������������ ������� ���������� ����� ������ (��� ������� ���������� ����� ������),
#������� ������������� ���������� ����� ������ (����� � �����-1 ����� ������)
#���� �������������� ����� ������ (�������� �����),
#��������������� ����������� ����� ������ (����� � �� ����� ������),
#������������� ������� ���������� ����� ������ (����� � ��� ����� ������),
#�����-1 ����� ������.
#�������� ���� ���������� �� �� ����� ��������������� ��� ����� (������� �����),
#�� ����� ������, ��� ����� ������,  �����-1 ����� ������, ��������������� ������������� ��� ������.
$arcFileMask="AFT*.arj"

#$digDoc ����������� �������� - ����, ���������� ������� ����������� ������.
#������������� ���� - ����, �������������� �� ��������� ������������ ���������
#� ����������� ������ ������������� ������� ����������������� ������ ����������
#� ������� ����������� �������������� ������������ ��� ������������� �������
#����������������� ������ ����������, ������������� ������ ������.
#��� ������������ �������������� ����� ����������� �������� ���������� �� �� ����� �����������,
#� ����� ��������� ��� ����������.
#������������ �������������� ����� ������������� ������������ �����
#� ����������� ���������� � ������� ���������� �� "vrb".
$digDocMaskCrypt=@("TOO*.vrb","TPO*.vrb")
$digDocMask=@("TOO*.xml","TPO*.xml")

#$funcMess ��������� ��������� - ����, ���������� �������������, ���������,
#��������� ��� �����������, ��������������� ��������� ���������� � ������������ � ���������� � 390-�.
#��������� ��������� ���������� ����� �������������� (����� - ��) �� ����� �����������.
$funcMessFileMask=@("PT*.xml","IZVTTUB*.xml","KVT*.xml","UV*.xml")
$arcKvitsRegex="^KVT_AFT_4525540_FTS0000_\d{8}_\d{3}.XML$"
$arcIzvRegex="^IZVTTUB_AFT_4525540_FTS0000_\d{8}_\d{3}.xml$"

#init block
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$scsign = Join-Path $executingScriptDirectory "SCSign.ps1"
$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path $workdir "arc"
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}
$errdir=Join-Path $workdir "err"
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}

if (-not (Test-Path $arcdir)) {
    New-Item $arcdir -ItemType Directory | Out-Null
}
#init block end

#functions block

function Send-Email {
    param(
        $msg,
        [switch]$HighPriority
        )
    $priority="Normal"
    if ($HighPriority) {
        $priority="High"
    }
    Send-MailMessage `
-to $emailsTo `
-from $emailsFrom `
-subject $emailsSubject `
-body $msg `
-smtpServer $smtpServer `
-priority $priority `
-encoding Unicode
}

function Extract-IncPackage {
    param(
        [string]$Mask,
        [switch]$Archive
        )
    $arcsPath=Join-Path $tempdir $Mask
    $arcs=Get-ChildItem $arcsPath -Recurse
    $currArcDir=Get-CurArcDir
    if (-not (Test-Path $currArcDir -PathType Container)) {
        New-Item $currArcDir -ItemType Directory -force | Out-Null
    }
    foreach ($arc in $arcs) {
        $arcParentDir=Split-Path $arc.FullName -Parent
        $dst=Join-Path $arcParentDir $arc.BaseName
        if (-not (Test-Path $dst -PathType Container)) {
            New-Item $dst -ItemType Directory -force | Out-Null
        }

        if ($arc.Extension.Equals(".772")) {
            Expand -R $arc.FullName $dst
        }
       
        if ($arc.Extension.ToLower().Equals(".arj")) {
            Start-Process $arj32 ("x {0} {1} -jv" -f $arc.FullName,$dst) -NoNewWindow -Wait
        }
        
        if ($Archive) {
            Invoke-Expression ("$SmartCopy $arc {0}" -f (Get-CurArcDir))
        }
        Remove-Item $arc
        
    }
}

function Check-Log {
    $Errgx=[regex]"CODE=[1-9]\d{0,}"
    $errors=$false
    if (Test-Path $logger.LogPath) {
        $log=Get-Content $logger.LogPath
        $logfile=$logger.LogPath
        if ($lastLogLine -lt $log.Count) {
            $newLines=$log[$lastLogLine..$log.Count]
            $matchs=$Errgx.Matches($newLines)
            if ($matchs.Success) {
                Send-Email "���������� ������ �������, ��� ����������. ����������� � ���-����� $logfile." -HighPriority
                $errors = $true
            }
        }
    }
    return $errors
}

$resultCodes = @{
"01" = "������������� ��������� ��������";
"02" = "������������ ����� �� ���������";
"03" = "������������� ��������� �������� ��";
"04" = "��������� ������������ �����, ������������ ��������� �� ������������� ������������� �����������";
"05" = "������������� ��������� ���������������� �����";
"06" = "��� ���������� ����� ������, ��������� � ������������ �����, ����������� � ������� ����� ���������� ����� ������, �������������� ����� ����������� �� ��������� � 390-�";
"07" = "���� (������ �����) �������� �� ������� ���������� ������ ������������ ����������� (�����������), �������� ����������� ��� ������;`
����������� ���� ���������� ������� (�������������� ���������� � ��������), ��������������� ������� 1.8 ��������� � 390-�;`
��� ����� (������� �����), ������������� ����� ������, ��������� � ������������ �����, ����������� � ����������� ��� ������;`
� ������ ����������, ������������� ������������� ����� ������";
"08" = "����������� ����������� ������ ����������� � ������ (�������� �����), �������������� ����� ������, ����������� ����� ������ ���������� �������� ������������� ������������� ����";
"09" = "����������� ����������� ������ ����������� � ������ (�������� �����), ����������� ����� ������, �������������� ����� ������ ���������� ������������� ����������� ��������������";
"11" = "������������� ��������� ������������� �������������� �����";
"12" = "���� � ����� ��������� ����� �� ������������� �������";
"13" = "������ ������������ ��������� �� ������������� ������������� �����������";
"21" = "����������� �������� �������� ��������� ���������� ������� �� � ��� ���� (������ �����), ������������� ����� ������";
"22" = "� ����� (������� �����), ������������� ����� ������ ����������� ����� �����, ��������� � ����������� ���������, �������������� ���������� �������";
"23" = "������������ ������� �� ������������� ������ ����� �������, ���������� � ����������� ���������, �������������� ���������� �������";
"24" = "� ����� (������� �����), ������������� ����� ������ ����������� ������ � ��������� ���/���";
"31" = "� ������ �������"
}

function Parse-IzvsKvts {
    $izvsKvts = Get-ChildItem $tempdir -Recurse | Where-Object {($_ -match $arcIzvRegex) -or ($_-match $arcKvitsRegex)}
    $ftype = ""
    foreach ($iok in $izvsKvts) {
        [xml]$xmlC = Get-Content $iok.FullName
        $ftype=$xmlC.GetEnumerator().NextSibling.Name
        $repName=$iok.Name
        $RepFile= $xmlC.$ftype.��������
        [string]$resultCode = $xmlC.$ftype.��������������
        $report = $resultCodes[$resultCode]
        switch ($ftype) {
            "���������" {
                if ($resultCode.Equals("01")) {
                    $logger.Write("���� $RepFile ������ ���. ���1:$repName")
                    Send-Email "�������� ���������:`n$ftype � ���, ��� ������������ ���� $RepFile ������ ���. ���1:$repName"
                } else {
                    $logger.Write("���� $RepFile �� ������ ���. ���1:$repName. ��������� �������� $report","WARNING")
                    Send-Email "���� $RepFile �� ������ ���. ���1:$repName`n��������� ��������: $report"
                }
                $izvParent=Split-Path $iok.FullName -Parent
                Move-Item $izvParent (Get-CurArcDir) -Force
            }
            "���������" {
                if ($resultCode.Equals("01")) {
                    $logger.Write("���� $RepFile ������ ��. ���1:$repName")
                    Send-Email "�������� ���������:`n$ftype � ���, ��� ������������ ���� $RepFile ������ ��. ���1:$repName"
                } else {
                    $logger.Write("���� $RepFile �� ������ ��. ���1:$repName. ��������� �������� $report","WARNING")
                    Send-Email "���� $RepFile �� ������ ��. ���1:$repName`n��������� ��������: $report"
                }
                Move-Item $iok.FullName (Get-CurArcDir) -Force
            }
        }
        
    }
}


#script block
$incs = $funcMessFileMask+$arcFileMask | ForEach-Object {Join-Path $workdir $_} | Get-ChildItem
if ($incs) {
    $currArcDir=Get-CurArcDir
    if (-not (Test-Path $currArcDir -PathType Container)) {
        New-Item $currArcDir -ItemType Directory -force | Out-Null
    }
    if (-not (Test-Path $tempdir)) {
        New-Item $tempdir -ItemType Directory | Out-Null
    }
    Move-Item $incs $tempdir -ErrorAction Stop -Verbose
    $arcsTempPath= Join-Path $tempdir $arcFileMask
    if (Test-Path $arcsTempPath) {
        Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Check" -f $arcsTempPath,$logger.LogPath)
    }
    Extract-IncPackage $arcFileMask -Archive
    foreach ($cryptMask in $digDocMaskCrypt) {
        Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Uncrypt -Recurse" -f (Join-Path $tempdir $cryptMask),$logger.LogPath)    
    }
    $digDocMaskCrypt| ForEach-Object {Join-Path $tempdir $_} |Get-ChildItem -Recurse | Rename-Item -NewName {$_.BaseName+".xml"}
    Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Check -Recurse" -f (Join-Path $tempdir "*.xml"),$logger.LogPath)
    Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Unsign -Recurse" -f (Join-Path $tempdir "*.xml"),$logger.LogPath)
    if (Check-Log) {
        if (-not (Test-Path $errdir)) {
            New-Item $errdir -ItemType Directory | Out-Null
        }
        Get-ChildItem $tempdir | Move-Item -Destination $errdir
    } else {
        Parse-IzvsKvts
        Get-ChildItem (Join-Path $tempdir "*.xml") -Recurse | Copy-Item -Destination $workdir
        try {
            Move-Item $tempdir\* -Destination (Get-CurArcDir) -Force -ErrorAction Stop
        } catch {
            $message="�� ������� ����������� ������ �� ��������� ���������� � �����. ���������� � ����� err"
            Send-Email $message -HighPriority
            $logger.Write($message,"ERROR")
            if (-not (Test-Path $errdir)) {
                New-Item $errdir -ItemType Directory | Out-Null
            }
            Move-Item $tempdir\* -Destination $errdir
        }
        
    }
    Remove-Item $tempdir
}


#script block end
