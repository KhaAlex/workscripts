param (
    $workdir="R:\06_mf\390p\TEST\OUT",
    $emailsFrom="cbmf_info@sovbank.ru",
    $emailsTo="a.kharkovskij@sovbank.ru", #_390pMF@sovbank.ru
    $smtpServer="812-cas01.sovbank.ru",
    $emailsSubject="��������� � ������ ���������� (��������� 390-�)"
    )
$senddir=$workdir
$bic="4525540"

#$funcMess ��������� ��������� - ����, ���������� �������������, ���������,
#��������� ��� �����������, ��������������� ��������� ���������� � ������������ � ���������� � 390-�.
#��������� ��������� ���������� ����� �������������� (����� - ��) �� ����� �����������.
$outFuncMessFileMask=@("PT*.xml", "UV*.xml")

$forSendMask = "*.xml"

#init block
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$scsign = Join-Path $executingScriptDirectory "SCSign.ps1"
$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path $workdir "arc"
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}
$errdir=Join-Path $workdir "err"
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}

if (-not (Test-Path $arcdir)) {
    New-Item $arcdir -ItemType Directory | Out-Null
}
#init block end

#functions block

function Send-Email {
    param(
        $msg,
        [switch]$HighPriority
        )
    $priority="Normal"
    if ($HighPriority) {
        $priority="High"
    }
    Send-MailMessage `
-to $emailsTo `
-from $emailsFrom `
-subject $emailsSubject `
-body $msg `
-smtpServer $smtpServer `
-priority $priority `
-encoding Unicode
}

function Get-PackageNumber {
    $idsFile = Join-Path (Get-CurArcDir) "id.txt"
    [int]$curId = 0
    if (Test-Path $idsFile) {
        [int]$lastId = Get-Content $idsFile -last 1
        $curId = $lastId+1
        Add-Content $idsFile $curId
    } else {
        $curId = 1
        #����� ��������� ������� ������� � return, Out-Null ��������� ��� �������������� ������ ���������
        New-Item $idsFile -ItemType file | Out-Null
        Add-Content $idsFile $curId
    }
    return $curId
}

function Get-PacketDirName {
    $packnum = Get-PackageNumber
    if ($packnum -lt 1) {
        throw "�������� ����� �������"
    }
    return "AFT_{0}_FTS0000_{1}_{2:D3}" -f $bic,(Get-Date -f yyyyMMdd),$packnum
}

function Check-Log {
    $Errgx=[regex]"CODE=[1-9]\d{0,}"
    $errors=$false
    if (Test-Path $logger.LogPath) {
        $log=Get-Content $logger.LogPath
        $logfile=$logger.LogPath
        if ($lastLogLine -lt $log.Count) {
            $newLines=$log[$lastLogLine..$log.Count]
            $matchs=$Errgx.Matches($newLines)
            if ($matchs.Success) {
                Send-Email "���������� ������ �������, ��� ����������. ����������� � ���-����� $logfile." -HighPriority
                $errors = $true
            }
        }
    }
    return $errors
}

function Make-ArjPacket {
    param(
        $arcBaseName,
        $files
        )
    $params="m -c -e -y {0}\{1}.arj {2}" -f $tempdir,$arcBaseName,$files
    Start-Process -FilePath $arj32 -ArgumentList $params -NoNewWindow -Wait
    return (Get-Item (Join-Path $tempdir "$arcBaseName.arj"))
}

#script block
$outs = $outFuncMessFileMask | ForEach-Object {Join-Path $workdir $_} | Get-ChildItem
if ($outs) {
    $currArcDir=Get-CurArcDir
    if (-not (Test-Path $currArcDir -PathType Container)) {
        New-Item $currArcDir -ItemType Directory -force | Out-Null
    }
    if (-not (Test-Path $tempdir)) {
        New-Item $tempdir -ItemType Directory | Out-Null
    }
    $packetName = Get-PacketDirName
    $logger.Write("�������� ��� ������� $packetName")
    $packetArcDir = New-Item (Join-Path $currArcDir $packetName) -ItemType Directory -force
    foreach ($readyFile in $outs) {
        Invoke-Expression ("$SmartCopy {0} {1}" -f $readyFile.FullName,$packetArcDir.FullName)
    }
    Move-Item $outs $tempDir -ErrorAction Stop
    Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Sign" -f (Join-Path $tempdir $forSendMask),$logger.LogPath)
    $arjPacket = Make-ArjPacket $packetName ("{0}\{1}" -f $tempdir,$forSendMask)
    Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Sign" -f $arjPacket,$logger.LogPath)
    Invoke-Expression ("$SmartCopy {0} {1}" -f $arjPacket,$currArcDir)
    if (Check-Log) {
        if (-not (Test-Path $errdir)) {
            New-Item $errdir -ItemType Directory | Out-Null
        }
        $logger.Write("���������� ������ �������, ��� ����������.","ERROR")
        Move-Item $arjPacket $errdir
    } elseif (Test-Path $senddir) {
        Move-Item $arjPacket $senddir
    } else {
        $errMess="���������� ��� �������� $senddir ����������.`n�������������� ���� �������� � $workdir."
        Move-Item $arjPacket $workdir
        $logger.Write($errMess,"ERROR")
        Send-Email $errMess -HighPriority
    }
    Remove-Item $tempdir
}