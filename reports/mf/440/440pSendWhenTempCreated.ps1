param (
    $Source="R:\06_mf\440p\OUT",
    $sendScript="R:\06_mf\440p\OUT\work\send.ps1",
    $filter="*",
    [string]$TaskName="440pSendWhenTempCreated",
    $emailsFrom="cbmf_info@sovbank.ru",
    $emailsTo="_mf_receivefromcb@sovbank.ru",
    $smtpServer="812-cas01.sovbank.ru",
    $emailsSubject="��������� � ������ ���������� (��������� 440-�)"
    )

$host.ui.RawUI.WindowTitle = $TaskName

function Send-Email {
    param(
        $msg,
        [switch]$HighPriority
        )
    $priority="Normal"
    if ($HighPriority) {
        $priority="High"
    }
    Send-MailMessage `
-to $emailsTo `
-from $emailsFrom `
-subject $emailsSubject `
-body $msg `
-smtpServer $smtpServer `
-priority $priority `
-encoding Unicode
}

$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent

$Files = Join-Path $Source $filter

$fsw = New-Object IO.FileSystemWatcher $Source, $filter -Property @{IncludeSubdirectories = $false;NotifyFilter = [IO.NotifyFilters]'DirectoryName'}

Register-ObjectEvent $fsw Created -SourceIdentifier $TaskName
Register-ObjectEvent $fsw Error -SourceIdentifier "Error$TaskName"

$ReconnectTimer = New-Object System.Timers.Timer
$ReconnectTimer.Interval= 10000
$ReconnectTimer.AutoReset= $true

Register-ObjectEvent $ReconnectTimer Elapsed -SourceIdentifier "Reconnect$TaskName"

if (Test-Path $Files) {
        Invoke-Expression $sendScript
}

$continue = $true
while ($continue) {
    $ev=Wait-Event
    switch ($ev.SourceIdentifier) {
        $TaskName {
            $name = $ev.SourceEventArgs.Name
            $changeType = $ev.SourceEventArgs.ChangeType
            $timeStamp = $ev.TimeGenerated
            Write-Host "Directory '$name' was $changeType at $timeStamp" -fore green
            if ($name.Equals("temp")) {
                if (Test-Path ($Files)) {
                    Start-Sleep -s 3
                    Invoke-Expression $sendScript
                }
            }
            break
        }
        "Error$TaskName" {
            Write-Host ("Error. Directory {0} is lost. Time:{1}" -f $ev.SourceArgs.Path,$ev.TimeGenerated) `
            -fore red
            Send-Email ("���������� {0} ����������. �����:{1}" -f $ev.SourceArgs.Path,$ev.TimeGenerated)
            Unregister-Event -SourceIdentifier $TaskName
            Unregister-Event -SourceIdentifier "Error$TaskName"
            Remove-Variable fsw
            $ReconnectTimer.Start()
            break
        }
        "Reconnect$TaskName" {
            if (Test-Path $Source) {
                if (Test-Path $Files) {
                    Invoke-Expression $sendScript
                }
                $fsw = New-Object IO.FileSystemWatcher $Source, $filter -Property @{IncludeSubdirectories = $false;NotifyFilter = [IO.NotifyFilters]'DirectoryName'}
                Register-ObjectEvent $fsw Created -SourceIdentifier $TaskName 
                Register-ObjectEvent $fsw Error -SourceIdentifier "Error$TaskName" 
                Write-Host "Directory connection restored"
                Send-Email ("���������� {0} ����� ��������, ������� ��������� �����������. �����:{1}" -f $workdir,(Get-Date -f "hh:mm:ss"))
                $ReconnectTimer.Stop()
            }
            break
        }
    }
    Remove-Event -SourceIdentifier $ev.SourceIdentifier
}

#Get-EventSubscriber |Unregister-Event