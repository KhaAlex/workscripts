param (
    $workPath = $(throw "No work dir setted"),
    $filesMask = "*.arj",
    $repdate = $(throw "No rep date setted")
    )

[xml]$xmlc = '<?xml version="1.0" encoding="windows-1251"?>
<���������>
  <�������� ���="�� � ���">
    <����������� ��������="558/6" ������������="������ ���������� ��� ���� ���������" />
    <������ ������������="">
      <����� ���="" />
      <���� ���="" />
    </������>
  </��������>
</���������>'

$kfbFile = Get-ChildItem "$workPath\*.KFB"
$filesForSend = Get-ChildItem (Join-Path $workPath $filesMask)
if (($filesForSend) -and ($kfbFile) -and ($kfbFile.Count -eq 1)) {
    $xmlc.���������.��������.������.������������ = $repdate.ToString("yyyy-MM-dd") 
    $xmlc.���������.��������.������.�����.��� = $kfbFile.Name
    $clone = $xmlc.���������.��������.������.����.Clone()
    foreach ($file in $filesForSend) {
        if ($xmlc.���������.��������.������.����.���) {
                $fileNode = $clone.Clone()
                $fileNode.��� = $file.Name
                $xmlc.���������.��������.������.AppendChild($fileNode) | Out-Null
            } else {
                $xmlc.���������.��������.������.����.��� = $file.Name
            }
    }
} else {
    throw "Not correct set of files, can't build package"
}
$savePath = (Join-Path $workPath "���������.xml") 
$xmlc.Save($savePath)
return $savePath