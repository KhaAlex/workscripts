param (
    $workdir="R:\06_mf\VBK\test\OUT"
    )
$reglCode = "VBK Transfer"
$outFileMask = "*.arj"

function Show-Popup {
    param(
        $Text="",
        $PopupText=$reglCode,
        [switch]$Err
        )
    $askPopup = New-Object -ComObject Wscript.Shell
    $pic=0x30
    if ($Err) {
        $pic = 0x10
    }
    $askPopup.Popup($Text,0,$PopupText,$pic) | Out-Null
}

$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$pickdate = Join-Path $executingScriptDirectory "pickDate.ps1"
$outfiles = Get-ChildItem (Join-Path $workdir $outFileMask)
$nameDate = &$pickdate
if ($nameDate) {
    $packageFolderName = Join-Path $workdir (get-date $nameDate -f yyyy-MM-dd)
    if (-not (@(1,11,21) -contains $nameDate.Day)) {
        Show-Popup "���� ������� �������, ������ ����� ����� ��������� ������ �� 1,11,21" -Err
        exit
    } elseif (Test-Path $packageFolderName) {
        throw "Package folder already exist. Something go wrong"
        exit
    } elseif (-not ($outfiles)) {
        Show-Popup "��� ������ ��� ��������" -Err
        exit
    } else {
        New-Item $packageFolderName -ItemType Directory
        Get-ChildItem (Join-Path $workdir $outFileMask) | Move-Item -Destination $packageFolderName
    }
}
