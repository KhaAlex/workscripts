Param(
	[int]$SelectionCount = 0, # number of days that can be selected
	[datetime]$TodayDate = $(Get-Date), # sets default selected date
	[datetime]$DateSelected = $TodayDate, # sets default selected date
	$FirstDayofWeek = -1, # -1 used default - calendar dayofweek, NOT datetime
	[datetime[]]$Bold = @(), # Array of bolded dates to add
	[datetime[]]$YBold = @(), # annual bolded dates to add
	[datetime[]]$MBold = @((Get-Date $TodayDate -Day 01),`
                         (Get-Date $TodayDate -Day 11),`
                         (Get-Date $TodayDate -Day 21),`
                         (Get-Date $TodayDate -Day 01).AddMonths(1)), # monthly bolded dates to add
	[switch]$WeekNumbers, # Show numeric week of year on the display
	[string]$Title = "�������� ����",
	[switch]$NoTodayCircle,
	[datetime]$MinDate = [datetime]"1753-01-01",
	[datetime]$MaxDate = [datetime]"9998-12-31"
)

$diffToday = 30
foreach ($BDate in $MBold) {
    if ([math]::abs(($Bdate - $TodayDate).Days) -lt [math]::abs($diffToday)) {
        $diffToday = ($Bdate - $TodayDate).Days
    }
}
$DateSelected = $TodayDate.AddDays($diffToday)

#region assembly loading - we load required assemblies not present in PowerShell
Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing
#[reflection.assembly]::Load("mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089") | Out-Null
#[reflection.assembly]::Load("System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089") | Out-Null
#end region assembly loading
[System.Windows.Forms.Application]::EnableVisualStyles();
# Is this voodoo code, or not?
[System.Windows.Forms.Application]::DoEvents();

#region SetupCalendarProperties

$cal = New-Object Windows.Forms.MonthCalendar;
$cal.SetDate($DateSelected);
$cal.TodayDate = $TodayDate;
if($SelectionCount -lt 1){$SelectionCount = [int]::MaxValue;}
$cal.MaxSelectionCount = $SelectionCount;
$cal.MinDate = $MinDate;
$cal.MaxDate = $MaxDate;
$cal.ShowTodayCircle = $true;
$cal.BackColor=[System.Drawing.Color]::LightGray
if($FirstDayofWeek -eq -1){$FirstDayofWeek = [System.Windows.Forms.Day]::Default}
$cal.FirstDayofWeek = [System.Windows.Forms.Day]$FirstDayofWeek;
$cal.ShowWeekNumbers = $WeekNumbers;
if($NoTodayCircle){$cal.ShowTodayCircle = $False}
  
  #region add bolded dates
if($Bold){$cal.BoldedDates = $Bold}
if($YBold){$cal.AnnuallyBoldedDates = $YBold}
if($MBold){$cal.MonthlyBoldedDates = $MBold}
  #end region add bolded dates
  
#end region SetupCalendarProperties

#region SetupCalendarDisplay

$form = New-Object Windows.Forms.Form
$form.AutoSize = $form.TopMost = $form.KeyPreview = $True
$form.MaximizeBox = $form.MinimizeBox = $False
$form.AutoSizeMode = "GrowAndShrink"
$form.Controls.Add($cal)
$form.Text = $Title
#end region SetupCalendarDisplay
$OKButton = New-Object System.Windows.Forms.Button
$OKButton.Location = New-Object System.Drawing.Point($cal.Location.X,[int]($cal.Height+10))
$OKButton.Size = New-Object System.Drawing.Size([int]($cal.Size.Width/2-10),23)
$OKButton.Text = '�������'
$OKButton.DialogResult = [System.Windows.Forms.DialogResult]::OK
$form.AcceptButton = $OKButton
$form.Controls.Add($OKButton)

$CancelButton = New-Object System.Windows.Forms.Button
$CancelButton.Location = New-Object System.Drawing.Point([int]($OKButton.Location.X+$OKButton.Width),[int]($cal.Height+10))
$CancelButton.Size = New-Object System.Drawing.Size([int]($cal.Size.Width/2-10),23)
$CancelButton.Text = '������'
$CancelButton.DialogResult = [System.Windows.Forms.DialogResult]::Cancel
$form.CancelButton = $CancelButton
$form.Controls.Add($CancelButton)

#end region SetupEventHandlers

#region display form
# Ensures the form is on top, is active, and then shows it.
# After calling ShowDialog(), the script is blocked until
# the form is no longer visible.

#$form.Add_Shown({$form.Activate()}) 
$result = $form.ShowDialog()
#end region display form

#region ReturnOutput
if($result -eq [System.Windows.Forms.DialogResult]::OK){
    for(
        $day = $cal.SelectionRange.Start;
        $day -le $cal.SelectionRange.End;
        $day = $day.AddDays(1)
        )
    {
        $day
    }
}
#end region ReturnOutput


#region update notes
#
# 2009-08-27
# -initialized $Escaped and removed $ShowTodayCircle (thanks, tojo2000) 
# -modified $FirstDayOfWeek so casts don't occur until after Forms library loaded.
# 
#end region update notes