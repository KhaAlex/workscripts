param (
    $workPath = $(throw "No work dir setted"),
    $unfCurDate = (Get-Date), #(Get-Date -Year 2018 -Month 05 -Day 28 -Hour 16 -Minute 02 -Second 28),
    $unfRepDate = $(throw "No report date setted")#(Get-Date -Year 2018 -Month 05 -Day 11)
    )
$bnk_regn = 558
$bnk_filn = 6
$bnk_name = "������ ���������� ��� ���� ���������"
$bnk_bik = "044585540"
$exedate = "15.03.2018"
$exevers = "1.38"
$id = 1
$files=(Get-ChildItem $workPath)
$KFBName = "{0}_{1}_{2}-{3}-{4}.KFB" -f $bnk_regn,$bnk_filn,$unfRepDate.Year,$unfRepDate.Month,$unfRepDate.Day
$repdate = $unfRepDate.ToString("dd.MM.yyyy")
$curdate = $unfCurDate.ToString("dd.MM.yyyy")
$curtime = $unfCurDate.ToString("HH:mm")
$regn = $bnk_regn.ToString("0000")
$filn = $bnk_filn.ToString("0000")
$rekv = Get-Content "R:\06_mf\144T\test\rekv.txt"
$filesdata = $files | % {("`"{0}`",`"{1}`",`"{2}`",`"{3}`",`"`"" -f $id++,$_.name.ToLower(),$_.Extension.Trim("."),$_.Length).ToLower()}
$dtmo = $unfRepDate.ToString("yyyyMMdd")
$dtmf = $unfCurDate.ToString("yyyyMMddHHmmss")

Set-Content (Join-Path $workPath $KFBName) -Value `
"BEGIN_REPORT FFO $repdate $curdate $curtime $exedate $regn/$filn `'`'$bnk_name`'`'
BEGIN_FORM REKW FULL
`"BNK_MNSNUM`",`"`"
`"BNK_REGN`",`"$bnk_regn`"
`"BNK_N`",`"`"
`"BNK_FILN`",`"$bnk_filn`"
`"BNK_BIK`",`"$bnk_bik`"
`"BNK_NAME`",`"$bnk_name`"
`"BNK_SNAME`",`"$bnk_name`"
`"BNK_ADRJ`",`"����������� �����`"
`"BNK_ADRF`",`"�������� �����`"
`"BNK_OKPO`",`"12345678`"
`"BNK_SOATO`",`"45`"
`"BNK_TYPE`",`"B`"
`"BNK_STATE`",`"d`"
`"BNK_RUKDLG`",`"$($rekv[1])`"
`"BNK_RUK_FIO`",`"$($rekv[0])`"
`"BNK_BUHDLG`",`"$($rekv[3])`"
`"BNK_BUH_FIO`",`"$($rekv[2])`"
`"REP_OI_DOLG`",`"$($rekv[5])`"
`"REP_OI_FIO`",`"$($rekv[4])`"
`"REP_OI_PHONE`",`"$($rekv[6])`"
`"EXE_DATE`",`"$exedate`"
`"EXE_VERS`",`"$exevers`"
END_FORM REKW 0
BEGIN_FORM KFO_OP FULL
$($filesdata -join "`n")
END_FORM KFO_OP 0
BEGIN_FORM OED_FO FULL
`"OKUD`",`"PSVBK`"
`"OKATO`",`"45`"
`"OKPO`",`"12345678`"
`"RNKO`",`"$bnk_regn/$bnk_filn`"
`"BIKKO`",`"$bnk_bik`"
`"DTMO`",`"$dtmo`"
`"DTME`",`"$dtmo`"
`"DTMF`",`"$dtmf`"
END_FORM OED_FO 0
BEGIN_FORM KFO_PROT FULL
END_FORM KFO_PROT 0
END_REPORT 0" `
-Encoding OEM