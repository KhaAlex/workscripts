param (
    $workdir="R:\06_mf\XML\IN",
    $emailsFrom="cb_mfinfo@sovbank.ru",
    $emailsTo="_XMLpMF@sovbank.ru",
    $smtpServer="812-cas01.sovbank.ru",
    $emailsSubject="��������� � ��������� ���������� (XML ������)",
    $bik="4525540"
    )
$reglCode="XML"
$incsRegEx="^TK_\d{0,13}_{0,1}\p{Ll}+-\p{Nd}+-*\p{Nd}*_\d{4}-\d{2}-\d{2}T\d{2}-\d{2}-\d{2}.*\.xml$"
#            TK_1512994578924_  KO     -558    -6       _2017 -12   -11   T15   -14   -19   _1_F0409350_ies1.xml
#            TK_1516179819237_  TU     -45              _2018 -01   -17   T12   -02   -49   _1_F0409129_ies1.xml
#            TK_                TU     -45              _2018 -03   -07   T14   -13   -34                   .xml
$incsTuKvitRegEx="^TKvit_TU_45_\d{4}-\d{2}-\d{2}T\d{2}-\d{2}-\d{2}.*\.xml$"
#                  TKvit_TU_45_2017 -10   -24   T10   -28   -53   .xml

#init block
$resExistErr=[System.Management.Automation.ErrorCategory]::ResourceExists
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1"
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1"
$scsign = Join-Path $executingScriptDirectory "SCSign.ps1"
$base64 = Join-Path $executingScriptDirectory "base64.ps1"
$CPEncoder = Join-Path $executingScriptDirectory "ChangeEncoding.ps1"

# ������ ������� ����������� ����� ��-�� ����, ��� ��������� ��� ������������� ���������
# ����������
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path (Split-Path $workdir -Parent) "arc\IN"
$errdir=Join-Path (Split-Path -Path $workdir -Parent) ("err\"+(Get-Date -f yyyyMMdd))
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}
#init block end

function XmlCanonicalisation {
    param (
        $xmlcpath
        )
    Add-Type -AssemblyName System.Security
    $transform = New-Object System.Security.Cryptography.Xml.XmlDsigExcC14NTransform
    [xml]$xml=Get-Content $xmlcpath
    $transform.LoadInput($xml)
    $out = $transform.GetOutput()
    $sr = New-Object System.IO.StreamReader $out
    Set-Content $xmlcpath $sr.ReadToEnd() -NoNewline
}

function Send-FileToArchive {
    param()
    begin{
        $currarcdir = Get-CurArcDir
        try {
            New-Item $currarcdir -ItemType Directory -ErrorAction Stop | Out-Null
        } catch {
            if ($_.CategoryInfo.Category -ne $resExistErr) {
                $_
                exit
            }
        }
    }
    process{
        &$SmartCopy $_.FullName $currarcdir
    }
}

function Send-Email {
    param(
        $msg,
        [switch]$HighPriority
        )
    $priority="Normal"
    if ($HighPriority) {
        $priority="High"
    }
    Send-MailMessage `
-to $emailsTo `
-from $emailsFrom `
-subject $emailsSubject `
-body $msg `
-smtpServer $smtpServer `
-priority $priority `
-BodyAsHtml `
-encoding Unicode
}

function Check-Log {
    $Errgx=[regex]"CODE=[1-9]\d{0,}"
    $errors=$false
    if (Test-Path $logger.LogPath) {
        $log=Get-Content $logger.LogPath
        $logfile=$logger.LogPath
        if ($script:lastLogLine -lt $log.Count) {
            $newLines=$log[$lastLogLine..$log.Count]
            $matchs=$Errgx.Matches($newLines)
            if ($matchs.Success) {
                Send-Email "���������� ������ �������, ��� ����������. ����������� � ���-����� $logfile." -HighPriority
                $errors = $true
            }
        }
    }
    if (Test-Path $logger.LogPath) {
        $script:lastLogLine=(Get-Content $logger.LogPath).Count
    } else {
        $script:lastLogLine=0
    }
    return $errors
}
#functions block end

#script block
$origKvits = Get-ChildItem $workdir | Where-Object {$_.Name -match $incsTuKvitRegEx}
$origKvits | Send-FileToArchive
foreach ($origKvit in $origKvits) {
    $origKvitName=$origKvit.Name
    [xml]$xml=Get-Content $origKvit.FullName
    $kvitFile=$xml.�����.����
    $kvitDescr=$xml.�����.���������
    $kvitStamp=$xml.�����.�������
    $ka = $xml.�����.��."#text"
    New-Item $tempdir -ItemType Directory | Out-Null
    $kaFile = "$(Join-Path $tempdir $origKvitName).ca"
    Set-Content $kaFile $ka
    &$base64 -Decode $kaFile
    Get-Content $origKvit.FullName | ForEach-Object {$_ -replace "<��.*��>",''} | Set-Content (Join-Path $tempdir $origKvitName) -NoNewLine
    &$scsign (Join-Path $tempdir $origKvitName) -CheckSeparateKaFile $kaFile -LogPath $logger.LogPath
    if (Check-Log) {
        if (-not (Test-Path $errdir)) {
            New-Item $errdir -ItemType Directory -ErrorAction Stop | Out-Null
        }
        Copy-Item $origKvit.FullName $errdir
    }
    Remove-Item $tempdir -Recurse -Force
    Remove-Item $origKvit.FullName
    if ($kvitDescr) {
        $message="�������� ��������� �� �� �� ���� $kvitFile<br/>���� ��������� � ����������:$kvitDescr<br/>����� ���������:$kvitStamp"
        Send-Email $message -HighPriority
        $logger.Write($message.Replace("<br/>","`n"),"WARNING")
    } else {
        $message="�������� ��������� �� �� �� ���� $kvitFile<br/>���� ������.<br/>����� ���������:$kvitStamp"
        Send-Email $message
        $logger.Write($message.Replace("<br/>","`n"))
    }
}

$incs = Get-ChildItem $workdir | Where-Object {$_.Name -match $incsRegEx}
$incs | Send-FileToArchive
foreach ($inc in $incs) {
    $incName = $inc.Name
    $tempIncPacketPath = (Join-Path $tempdir $incName)
    [xml]$incXml = Get-Content $inc.FullName
    $packetKa = $incXml.��.��."#text"
    $esName=$incXml.��.��������.��.����������
    $esContent=$incXml.��.��������.��."#text"
    $esKA=$incXml.��.��������.��."#text"
    $tempEsPath = (Join-Path $tempdir $esName)
    $esKaFilePath = $tempEsPath+"_es.ca"

    New-Item $tempdir -ItemType Directory -ErrorAction Stop | Out-Null
    $packetKaFile = $tempIncPacketPath+"_packet.ca"
    Set-Content $packetKaFile $packetKa
    &$base64 -Decode $packetKaFile
    Get-Content $inc.FullName |`
    ForEach-Object {$_ -replace "</��������><��.*��>",'</��������>'}|`
    ForEach-Object {$_ -replace '<\?xml version="1.0" encoding="windows-1251"\?>',''} |`
    Set-Content $tempIncPacketPath -NoNewLine
    XmlCanonicalisation($tempIncPacketPath)
    &$CPEncoder -Path $tempIncPacketPath -From 1251 -toUTF8NoBOM
    &$scsign $tempIncPacketPath -CheckSeparateKaFile $packetKaFile -LogPath $logger.LogPath

    Set-Content $tempEsPath $esContent
    Set-Content $esKaFilePath $esKA
    &$base64 -Decode $tempEsPath
    &$base64 -Decode $esKaFilePath
    &$scsign $tempEsPath -CheckSeparateKaFile $esKaFilePath -LogPath $logger.LogPath
    if (Check-Log) {
        if (-not (Test-Path $errdir)) {
            New-Item $errdir -ItemType Directory -ErrorAction Stop | Out-Null
        }
        Copy-Item $inc.FullName,$tempEsPath,$esKaFilePath $errdir
        Remove-Item $tempdir -Recurse -Force
        Remove-Item $inc.FullName
        continue
    } else {
        $esInArch = Copy-Item $tempEsPath (Get-CurArcDir) -PassThru
        [xml]$esXml = Get-Content $tempEsPath
        [string]$message = ""
        $priority=$true
        if ($esName -match 'ies1') {
            $message = $esXml.���1.������� |ConvertTo-Html  ��������,�������,�������������,���������������������,�������������������� -As List -Fragment | Out-String
            $message += $esXml.���1 |ConvertTo-Html  �����������������,��������������,����������� -As List -Fragment | Out-String
            [boolean]$priority = (-not ($esXml.���1.��������������).Equals("0"))
        } elseif ($esName -match 'ies2') {
            $message = $esXml.���2.������� |ConvertTo-Html  ��������,�������,�������������,���������������������,�������������������� -As List -Fragment | Out-String
            $message += $esXml.���2.��������� |ConvertTo-Html  �����������������,��������������,����������� -As List -Fragment | Out-String
            [boolean]$priority = (-not ($esXml.���2.���������.��������������).Equals("0"))
        } else {
            $message = "<p>��� ������ �� ���������</p>"
            $message += $esXml.���1.������� |ConvertTo-Html  ��������,�������,�������������,���������������������,�������������������� -As List -Fragment | Out-String
            $message += $esXml.���1 |ConvertTo-Html  �����������������,��������������,����������� -As List -Fragment | Out-String
            [boolean]$priority = (-not ($esXml.���1.��������������).Equals("0"))
        }
        $message += "<br/>��������� ���������� � ����� $($esInArch.FullName)"
        Send-Email ($message -Replace "\n\s+\n","`n") -HighPriority:$priority
        Remove-Variable message
    }
    Remove-Item $tempdir -Recurse -Force
    Remove-Item $inc.FullName
}

#script block end
