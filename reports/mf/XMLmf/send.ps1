param (
    $workdir="R:\06_mf\XML\test\OUT",
    $senddir=$workdir, #"S:\S_All\send_as_XML",
    $cryptTo="0200",
    $emailsFrom="cb_mfinfo@sovbank.ru",
    $emailsTo="_XMLmf@sovbank.ru",
    $smtpServer="812-cas01.sovbank.ru",
    $emailsSubject="��������� �� �������� ���������� (XML ������)",
    $bik="4525540",
    $RegCode = "558",
    $FilCode = "6"
    )

$reglCode="XML"
$outFileMask = "*.xml"
$orgInMask = ({&$RegCode+"-"+$FilCode},{&$RegCode})[!$FilCode]

#init block
$resExistErr=[System.Management.Automation.ErrorCategory]::ResourceExists
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1"
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1"
$scsign = Join-Path $executingScriptDirectory "SCSign.ps1"
$base64 = Join-Path $executingScriptDirectory "base64.ps1"
$CPEncoder = Join-Path $executingScriptDirectory "ChangeEncoding.ps1"
$TKTemplate = Join-Path $executingScriptDirectory "TKtemplate.xml"

# ������ ������� ����������� ����� ��-�� ����, ��� ��������� ��� ������������� ���������
# ����������
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path (Split-Path $workdir -Parent) "arc\OUT"
$errdir=Join-Path (Split-Path -Path $workdir -Parent) ("err\"+(Get-Date -f yyyyMMdd))

$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0

if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}
#init block end

function XmlCanonicalisation {
    #c14n XML canonicalisation applying
    param (
        $xmlcpath
        )
    Add-Type -AssemblyName System.Security
    $transform = New-Object System.Security.Cryptography.Xml.XmlDsigExcC14NTransform
    [xml]$xml=Get-Content $xmlcpath
    $transform.LoadInput($xml)
    $out = $transform.GetOutput()
    $sr = New-Object System.IO.StreamReader $out
    Set-Content $xmlcpath $sr.ReadToEnd() -NoNewline
}

function Send-FileToArchive {
    param()
    begin{
        $currarcdir = Get-CurArcDir
        try {
            New-Item $currarcdir -ItemType Directory -ErrorAction Stop | Out-Null
        } catch {
            if ($_.CategoryInfo.Category -ne $resExistErr) {
                $_
                exit
            }
        }
    }
    process{
        &$SmartCopy $_.FullName $currarcdir
    }
}

function Send-Email {
    param(
        $msg,
        [switch]$HighPriority
        )
    $priority="Normal"
    if ($HighPriority) {
        $priority="High"
    }
    Send-MailMessage `
-to $emailsTo `
-from $emailsFrom `
-subject $emailsSubject `
-body $msg `
-smtpServer $smtpServer `
-priority $priority `
-BodyAsHtml `
-encoding Unicode
}

function Check-Log {
    $Errgx=[regex]"CODE=[1-9]\d{0,}"
    $errors=$false
    if (Test-Path $logger.LogPath) {
        $log=Get-Content $logger.LogPath
        $logfile=$logger.LogPath
        if ($script:lastLogLine -lt $log.Count) {
            $newLines=$log[$lastLogLine..$log.Count]
            $matchs=$Errgx.Matches($newLines)
            if ($matchs.Success) {
                Send-Email "���������� ������ �������, ��� ����������. ����������� � ���-����� $logfile." -HighPriority
                $errors = $true
            }
        }
    }
    if (Test-Path $logger.LogPath) {
        $script:lastLogLine=(Get-Content $logger.LogPath).Count
    } else {
        $script:lastLogLine=0
    }
    return $errors
}

#functions block end

$outs = Get-ChildItem (Join-Path $workdir $outFileMask)
foreach ($out in $outs) {
    $out | Send-FileToArchive
    if (-not (Test-Path $tempdir)) {
        New-Item $tempdir -ItemType Directory | Out-Null
    }
    $out = Move-Item $out $tempdir -Force -PassThru
    [xml]$newTK = Get-Content $TKTemplate
    $newTK.��.����������� = (get-date -f yyyy-MM-ddTHH:mm:ss).ToString()
    $newTK.��.�������� = ([guid]::NewGuid()).ToString()
    $newTK.��.�����������.������������ = ([guid]::NewGuid()).ToString()
    [xml]$outxml = Get-Content $out
    $newTk.��.��������.��.������ = $outxml.LastChild.�����������.������
    $newTk.��.��������.��.������ = $outxml.LastChild.�����������.������
    $newTk.��.��������.��.����� = $outxml.LastChild.Name
    $newTk.��.��������.��.���������� = $out.Name
    $newTk.��.��������.��.�������� = ([guid]::NewGuid()).ToString()
    Invoke-Expression ("$scsign -Path {0} -LogPath {1} -SignSeparate" -f $out.FullName,$logger.LogPath)
    $sepKAFile = $out.FullName+".ca"
    &$base64 -fileName $sepKAFile -Encode
    $b64KA= Get-Content $sepKAFile
    Remove-Item $sepKAFile
    $newTk.��.��������.��.InnerText = $b64KA
    Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Crypt {2}" -f $out.FullName,$logger.LogPath,$cryptTo) #crypt $out
    &$base64 -fileName $out.FullName -Encode
    $cryptedOutB64Content = Get-Content $out
    Remove-Item $out
    $newTk.��.��������.��.InnerText = $cryptedOutB64Content
    $ResultFilename = ("{0}\TK_KO-{1}{2}_{3}.xml_1.xml" -f $tempdir,$RegCode,"-$FilCode",(get-date -f yyyy-MM-ddTHH-mm-ss).ToString())
    $newTk.InnerXML |`
    ForEach-Object {$_ -replace "</��������><��.*��>",'</��������>'}|`
    ForEach-Object {$_ -replace '<\?xml version="1.0" encoding="windows-1251"\?>',''}|`
    Set-Content -Path $ResultFilename -NoNewLine
    XmlCanonicalisation $ResultFilename
    &$CPEncoder -Path $ResultFilename -From 1251 -toUTF8NoBOM
    Invoke-Expression ("$scsign -Path {0} -LogPath {1} -SignSeparate" -f $ResultFilename,$logger.LogPath)
    $sepKAFile = $ResultFilename+".ca"
    &$base64 -fileName $sepKAFile -Encode
    $b64KA= Get-Content $sepKAFile
    Remove-Item $sepKAFile
    Remove-Item $ResultFilename
    $newTk.��.��.InnerText = $b64KA
    $newTk.Save($ResultFilename)
    (Get-Item $ResultFilename) | Send-FileToArchive
    if (Check-Log) {
            if (-not (Test-Path $errdir)) {
                New-Item $errdir -ItemType Directory | Out-Null
            }
            Move-Item $ResultFilename $errdir
            $script:errors = $true
        } else {
            Move-Item $ResultFilename $senddir
        }
    Remove-Item $tempdir -ErrorAction Stop
}
If ($errors) {
    $msg = "���������� ������ ������� ��� ����������, ��������� � �������� ��������� ���������� � $errdir, ������ �����
    ����������� � $($logger.LogPath)"
    Send-Email $msg -HighPriority
    Write-Host $msg
}
