param (
    $workdir="R:\06_mf\365p\IN",
    $emailsFrom="cbmf_info@sovbank.ru",
    $emailsTo="_365pMF@sovbank.ru",
    $smtpServer="812-cas01.sovbank.ru",
    $emailsSubject="��������� � ������ ���������� (��������� 365-�)"
    )

#init block
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$scsign="`"C:\Program Files (x86)\SCSignEx\SCSignEx.exe`""
if (-not (Test-Path $scsign.Trim("`""))) {
    throw "No SCSignEx found. Impossible to execute script."
}
# ������ ������� ����������� ����� ��-�� ����, ��� ��������� ��� ������������� ���������
# ����������
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path $workdir "arc"
$errdir=Join-Path $workdir "err"
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}
$PackageMask="*.bin"

if (-not (Test-Path $tempdir)) {
    New-Item $tempdir -ItemType Directory | Out-Null
}
if (-not (Test-Path $arcdir)) {
    New-Item $arcdir -ItemType Directory | Out-Null
}
#init block end

#functions block
#������� ���������� �� ARJ32
function Extract-Arj {
param (
    [string]$arch,
    [string]$dest
    )
Invoke-Expression "$arj32 e $arch $dest -jv"  
}

function Send-Email {
    param(
        $msg,
        [switch]$HighPriority
        )
    $priority="Normal"
    if ($HighPriority) {
        $priority="High"
    }
    Send-MailMessage `
-to $emailsTo `
-from $emailsFrom `
-subject $emailsSubject `
-body $msg `
-smtpServer $smtpServer `
-priority $priority `
-encoding Unicode
}

# ������� ����� ��������� �������� �������, ������� ������� SmartCopyFile.ps1 � ����������
# �� �������� � ������� �����������
function Copy-ArcsToArchive {
    param(
        $arcs
        )
    $currArcDir=Get-CurArcDir
    if (-not (Test-Path $currArcDir -PathType Container)) {
        New-Item $currArcDir -ItemType Directory -force | Out-Null
    }
    foreach ($arc in $arcs) {
        Invoke-Expression "$SmartCopy $arc $currArcDir"
        #DOTO what if some not copied
    }
}

# � �������������� -Decrypt ������� ��������������, � ��� ���� ��������� � �������
# ������� � ����� ���������� SCSignEx ���������� �� �������� ������� � ���
function SCSign-FileList {
    param(
        $fileList,
        [switch]$Decrypt
        )
    
    if ($Decrypt) {
        $argums=("-d -l{0} -o{1} -b0 -gB:\ -iB:\ -b0" -f $fileList.FullName,$logger.LogPath)
        Start-Process $scsign $argums -wait    
    } else {
        $argums=("-c -l{0} -o{1} -b0 -gA:\ -iB:\ -b0" -f $fileList.FullName,$logger.LogPath)    
        Start-Process $scsign $argums -wait    
        $argums=("-r -l{0} -b0" -f $fileList.FullName)    
        Start-Process $scsign $argums -wait    
    }
    
    Remove-Item $filelist
}

# ������� ��������� ���������� �� � ������ ����� � ��������� ��� � ��������� �����
# ������������ � Extract-IncPackage
function Check-Arc {
    param (
        $arc
        )
    $currArcDir=Get-CurArcDir
    $CheckinArc=Join-Path $currArcDir $arc.Name
    return (Test-Path $CheckinArc -PathType Leaf)
}

# ������� ��������� �������� ���� � ����� � ��� ������
# � ������ -Archive �� ��������� ���������� �� ��������� ������������ �������� �����
# ��� ������������ �������� � �����.
function Extract-IncPackage {
    param(
        [string]$Mask,
        [switch]$Archive
        )
    $arcsPath=Join-Path $tempdir $Mask
    $arcs=Get-ChildItem $arcsPath -Recurse
    foreach ($arc in $arcs) {
        $arcParentDir=Split-Path $arc.FullName -Parent
        $dst=Join-Path $arcParentDir $arc.BaseName
        if (-not (Test-Path $dst -PathType Container)) {
            New-Item $dst -ItemType Directory -force | Out-Null
        }
        
        Extract-Arj $arc.FullName $dst

        # �������������� ��������, ���� ����� ��������� ����� ��� � ������
        # �� �������� ���� �� ���������
        if ((Check-Arc $arc) -and ($Archive)) {
            Remove-Item $arc
        } elseif (-not $Archive) {
            Remove-Item $arc
        }
    }
}

# ��������� ��������� ������� � �����, � �������� ��������� ����������.
function Prepare-IncPackage {
    $received=$false
    $arcsPath=Join-Path $workdir $PackageMask
    $arcs=Get-ChildItem $arcsPath
    if ($arcs) {
        Copy-ArcsToArchive $arcs
        Move-Item $arcs $tempdir -Force
    }
}

# ������� ������� ��� ������������ ������ ������ ��� ���������� ���
# ������ ������� SCSignEx. ���������� SCSign-FileList
function UnSCSign-FileList {
    param (
        $Mask,
        [switch]$Decrypt
        )
    $incsPath= Join-Path $tempdir $Mask
    $incs=Get-ChildItem $incsPath -Recurse
    $filelist=Join-Path $tempdir "unsignFileList.txt"
    Set-Content $filelist $incs.Fullname
    if ($Decrypt) {
        SCSign-FileList (Get-Item $filelist) -Decrypt
    } else {
        SCSign-FileList (Get-Item $filelist)
    }
}

# ������� �������� ���1. ��������� ������� � ������� � ���,
# ���� ������� ������ ��������� �� email
function Parse-Izvs {
    $izvsPath= Join-Path $tempdir "IZV*.txt"
    $izvs=Get-ChildItem $izvsPath -Recurse
    foreach ($izv in $izvs) {
        $izvName=$izv.Name
        $izvStr=Get-Content $izv
        $FileNameinIzv=$izvStr[0]
        if ($izvStr[1].Contains("01@@@")) {
            $logger.Write("���� $FileNameinIzv ������. ���1:$izvName")
            Send-Email "�������� ��������� � ���, ��� ������������ ���� $FileNameinIzv ������ ��. ���1:$izvName"
        } else {
            $logger.Write("���� $FileNameinIzv �� ������. ���1:$izvName","WARNING")
            Send-Email "���� $FileNameinIzv �� ������. ���1:$izvName"
        }
        $izvParent=Split-Path $izv.FullName -Parent
        Move-Item $izvParent (Get-CurArcDir) -Force
    }
}

# ������� �������� ������� ������� ������ � ����, �� ������� ������
# ������� ���������� ���������� $lastLogLine � �������������
function Check-Log {
    $Errgx=[regex]"CODE=[1-9]\d{0,}"
    $errors=$false
    if (Test-Path $logger.LogPath) {
        $log=Get-Content $logger.LogPath
        $logfile=$logger.LogPath
        if ($lastLogLine -lt $log.Count) {
            $newLines=$log[$lastLogLine..$log.Count]
            $matchs=$Errgx.Matches($newLines)
            if ($matchs.Success) {
                Send-Email "���������� ������ �������, ��� ����������. ����������� � ���-����� $logfile." -HighPriority
                $errors = $true
            }
        }
    }
    return $errors
}
#functions block end

#script block
Prepare-IncPackage
Extract-IncPackage "*.bin" -Archive
UnSCSign-FileList "*.arj"
Extract-IncPackage "AFN*.arj"
UnSCSign-FileList "*.vrb" -Decrypt
Get-ChildItem "$tempdir\*.vrb" -Recurse | Rename-Item -NewName {$_.BaseName+".txt"}
UnSCSign-FileList "*.txt"
#Check-Log return $true if found errors in signing or encrypting. If no errors we receive file.
#If errors found we store result in err folder.
if (Check-Log) {
    if (-not (Test-Path $errdir)) {
        New-Item $errdir -ItemType Directory | Out-Null
    }
    Get-ChildItem $tempdir | Move-Item -Destination $errdir
} else {
    Parse-Izvs
    $incs=Get-ChildItem "$tempdir\*.txt" -Recurse  | Copy-Item -Destination $workdir -PassThru
    if ($incs) {
        Send-Email "��������� ��������� �� ���������� ������ �� 365-�.`n���������:`n$workdir"
    }
    Move-Item $tempdir\* -Destination (Get-CurArcDir) -force
}

Remove-Item $tempdir

#script block end
