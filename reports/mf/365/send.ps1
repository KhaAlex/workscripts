param (
    $workdir="R:\06_mf\365p\TEST\OUT",
    $senddir="S:\S_All\send_to_FNS365"
    $cryptTo="7020",
    $emailsFrom="cb_mfinfo@sovbank.ru",
    $emailsTo="a.kharkovskij@sovbank.ru",
    $smtpServer="812-cas01.sovbank.ru",
    $emailsSubject="��������� �� �������� ���������� (��������� 365-�)"
    )

$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1"
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$scsign="`"C:\Program Files (x86)\SCSignEx\SCSignEx.exe`"" 

$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path $workdir "arc"
$errdir=Join-Path $workdir "err"
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1"

function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0

if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}

if (-not (Test-Path $tempdir)) {
    New-Item $tempdir -ItemType Directory | Out-Null
}
if (-not (Test-Path $arcdir)) {
    New-Item $arcdir -ItemType Directory | Out-Null
}

$txtToVrbMasks=@("BNS*.txt","BOS*.txt","BV*.txt")
$forSendMask="*.txt"
#ARJ Name _AFN_4525540_MIFNS00_%MYDATE%_0%lts%.arj
#Example  _AFN_4525540_MIFNS00_20170626_002.arj

function Send-Email {
    param(
        $msg,
        [switch]$HighPriority
        )
    $priority="Normal"
    if ($HighPriority) {
        $priority="High"
    }
    Send-MailMessage `
-to $emailsTo `
-from $emailsFrom `
-subject $emailsSubject `
-body $msg `
-smtpServer $smtpServer `
-priority $priority `
-encoding Unicode
}

function Get-PackageNumber {
    $idsFile = Join-Path (Get-CurArcDir) "id.txt"
    [int]$curId = 0
    if (Test-Path $idsFile) {
        [int]$lastId = Get-Content $idsFile -last 1
        $curId = $lastId+1
        Add-Content $idsFile $curId
    } else {
        $curId = 1
        #����� ��������� ������� ������� � return, Out-Null ��������� ��� �������������� ������ ���������
        New-Item $idsFile -ItemType file | Out-Null
        Add-Content $idsFile $curId
    }
    return $curId
}

function Get-PacketDirName {
    $packnum = Get-PackageNumber
    if ($packnum -lt 1) {
        throw "�������� ����� �������"
    }
    $formattedDate = Get-Date -f yyyyMMdd
    return "AFN_4525540_MIFNS00_{0}_{1:D3}" -f $formattedDate,$packnum
}

function Copy-MessToArcive {
    param(
        $files
        )
    $currArcDir=Get-CurArcDir
    if (-not (Test-Path $currArcDir -PathType Container)) {
        New-Item $currArcDir -ItemType Directory -force | Out-Null
    }
    # Get-PacketDirName ������� ����� $currArcDir ��� ������
    $packageName = Get-PacketDirName
    $currArcPacketDir=Join-Path $currArcDir $packageName
    if (-not (Test-Path $currArcPacketDir)) {
            New-Item $currArcPacketDir -ItemType Directory | Out-Null
        }
    foreach ($file in $files) {
        $fileName=$file.FullName
        Invoke-Expression "$SmartCopy $fileName $currArcPacketDir"
        #DOTO what if some not copied
    }
    return $packageName
}

function Prepare-ForSend {
    $filesForSend = Get-ChildItem (Join-Path $workdir $forSendMask) | Select-Object -First 1999
    if ($filesForSend) {
        $packetName = Copy-MessToArcive $filesForSend
        Move-Item $filesForSend $tempdir -Force
    }
    return $packetName
}

function SCSign-Files {
    param(
        $Mask,
        [switch]$Encrypt
        )
    if (Test-Path (Join-Path $tempdir $Mask)) {
        $forSendPath= Join-Path $tempdir $Mask
        $forSend=Get-ChildItem $forSendPath 
        $fileList=Join-Path $tempdir "ScsignFileList.lst"
        Set-Content $fileList $forSend.Fullname
        
        if ($Encrypt) {
            $argums=("-e -a{0} -l{1} -o{2} -b0 -gB:\ -iB:\" -f $cryptTo,$fileList,$logger.LogPath)
            Start-Process $scsign $argums -wait    
        } else {
            $argums=("-s -l{0} -o{1} -b0 -gA:\ -iA:\" -f $fileList,$logger.LogPath)    
            Start-Process $scsign $argums -wait  
        }
        
        Remove-Item $fileList
    }
}

function Make-ArjPacket {
    param(
        $arcBaseName,
        $files
        )
    $params="m -c -e -y {0}\{1}.arj {2}" -f $tempdir,$arcBaseName,$files
    Start-Process -FilePath $arj32 -ArgumentList $params -NoNewWindow -Wait
    return (Get-Item (Join-Path $tempdir "$arcBaseName.arj"))
}

function Check-Log {
    $Errgx=[regex]"CODE=[1-9]\d{0,}"
    $errors=$false
    if (Test-Path $logger.LogPath) {
        $log=Get-Content $logger.LogPath
        $logfile=$logger.LogPath
        if ($lastLogLine -lt $log.Count) {
            $newLines=$log[$lastLogLine..$log.Count]
            $matchs=$Errgx.Matches($newLines)
            if ($matchs.Success) {
                Send-Email "���������� ������ �������, ��� ����������. ����������� � ���-����� $logfile." -HighPriority
                $errors = $true
            }
        }
    }
    return $errors
}

while (Test-Path (Join-Path $workdir $filesForSend)) {
    $ArjPacketName = Prepare-ForSend
    if ($ArjPacketName) {
        $logger.Write("�������� ��� ������� $ArjPacketName")
    }
    SCSign-Files "*.txt"
    $toVrb=$txtToVrbMasks| ForEach-Object {Join-Path $tempdir $_}|Get-ChildItem 
    $toVrb|Rename-Item -NewName {$_.BaseName+".VRB"}
    SCSign-Files "*.vrb" -Encrypt
    $arjPacket = Make-ArjPacket $ArjPacketName ("{0}\*.VRB {0}\*.TXT" -f $tempdir)
    SCSign-Files $arjPacket.Name
    $signedArjPacket = Make-ArjPacket "_$ArjPacketName" $arjPacket.FullName
    Invoke-Expression ("{0} {1} {2}" -f $SmartCopy,$signedArjPacket.FullName,(Get-CurArcDir))
    #Check-Log return $true if found errors in signing or encrypting. If no errors we send file.
    #If errors found we store result in err folder.
    if (Check-Log) {
        if (-not (Test-Path $errdir)) {
            New-Item $errdir -ItemType Directory | Out-Null
        }
        Move-Item $signedArjPacket $errdir
    } elseif (Test-Path $senddir) {
        #Move-Item $signedArjPacket $senddir
    } else {
        Move-Item $signedArjPacket $workdir
        Send-Email "���������� ��� �������� $senddir ����������.`n�������������� ���� �������� � $workdir." -HighPriority
    }
}

Remove-Item $tempdir
