param (
    $workdir="R:\06_mf\VBK\4498U\OUT",
    $emailsFrom="cbmf_info@sovbank.ru",
    $emailsTo="a.kharkovskij@sovbank.ru", #_390pMF@sovbank.ru
    $smtpServer="812-cas01.sovbank.ru",
    $emailsSubject="��������� � ������ ���������� (�������� 4498-�)",
    $senddir="S:\308p\out"
    )

$bic="4525540"

$outFileMask=@("ARHOS*.arj", "ARHVBK*.arj","ARHSPD*.arj")
$packetName = "transfer.arj"

#init block
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$scsign = Join-Path $executingScriptDirectory "SCSign.ps1"
$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path (Split-Path $workdir -Parent) "arc\OUT"
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}
$errdir=Join-Path $workdir "err"
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}

if (-not (Test-Path $arcdir)) {
    New-Item $arcdir -ItemType Directory | Out-Null
}
#init block end

function Send-Email {
    param(
        $msg,
        [switch]$HighPriority
        )
    $priority="Normal"
    if ($HighPriority) {
        $priority="High"
    }
    Send-MailMessage `
-to $emailsTo `
-from $emailsFrom `
-subject $emailsSubject `
-body $msg `
-smtpServer $smtpServer `
-priority $priority `
-encoding Unicode
}

function Send-ToArchive {
    begin{
        $currarcdir = Get-CurArcDir
        $resExistErr=[System.Management.Automation.ErrorCategory]::ResourceExists
        try {
            New-Item $currarcdir -ItemType Directory -ErrorAction Stop | Out-Null
        } catch {
            if ($_.CategoryInfo.Category -ne $resExistErr) {
                $_
                exit
            }
        }
    }
    process{
        $item = Get-Item $_
        if ($item.PSIsContainer) {
                Copy-Item $item $currArcDir -Recurse -Force
            } else {
                &$SmartCopy $item.FullName $currarcdir
            }
    }
}

function Check-Log {
    if (Test-Path $logger.LogPath) {
        $script:lastLogLine=(Get-Content $logger.LogPath).Count
    } else {  
        return $true
    }
    $Errgx=[regex]"CODE=[1-9]\d{0,}"
    $errors=$false
    if (Test-Path $logger.LogPath) {
        $log=Get-Content $logger.LogPath
        $logfile=$logger.LogPath
        if ($lastLogLine -lt $log.Count) {
            $newLines=$log[$lastLogLine..$log.Count]
            $matchs=$Errgx.Matches($newLines)
            if ($matchs.Success) {
                Send-Email "���������� ������ �������, ��� ����������. ����������� � ���-����� $logfile." -HighPriority
                $errors = $true
            }
        }
    }
    return $errors
}

function Make-ArjPacket {
    param(
        $arcFullName,
        $file
        )
    $params=("m -c -e -y {0} {1}" -f $arcFullName,$file)
    Start-Process -FilePath $arj32 -ArgumentList $params -NoNewWindow -Wait
    return (Get-Item $arcFullName)
}

$outs = $outFileMask | ForEach-Object {Join-Path $workdir $_} | Get-ChildItem
Write-Host $outs
if ($outs) {
    $outs | Send-ToArchive
    if (-not (Test-Path $tempdir)) {
        New-Item $tempdir -ItemType Directory | Out-Null
    }
    foreach ($out in $outs) {
        $packageName = $out.BaseName 
        $packageTempDir = Join-Path $tempdir $packageName
        if (-not (Test-Path $packageTempDir)) {
            New-Item $packageTempDir -ItemType Directory | Out-Null
        } else {
            $logger.Write("�������� ����� $packageName � ��������� ���������� ��� ����������, ����� �������","WARNING")
            Remove-Item $packageTempDir -Recurse -Force
            New-Item $packageTempDir -ItemType Directory | Out-Null
        }
        &$scsign -Path $out.FullName -Crypt "0200" -LogPath $logger.LogPath
        &$scsign -Path $out.FullName -Sign -LogPath $logger.LogPath
        $arjPacket = Make-ArjPacket (Join-Path $packageTempDir $packetName) $out.FullName
        if (-not (Check-Log)) {
            $packageTempDir | Send-ToArchive
            for ($i=0;$i -lt 10;$i++) {
                $sendingfilepath = Join-Path $senddir $arjPacket.Name
                if (-not (Test-Path $sendingfilepath)) {
                    Move-Item $arjPacket $senddir
                    $logger.Write(("{0} ��������� � {1} � ��������� � {2}" -f $out.Name,$arjPacket.Name,$senddir))
                    break;
                }
                Start-Sleep -s 5
                if ($i -ge 9) {
                    $logmessage=("{0} ����������� � {1} �� ������� ��������� � {2}, ���� ���������� ��������� �� ������ �������" -f $out.Name,$arjPacket.Name,$senddir)
                    $logger.Write($logmessage,"ERROR")
                    Send-Email $logmessage -HighPriority
                    Remove-Item $arjPacket
                }
            }
        } else {
            if (-not (Test-Path $errdir)) {
                New-Item $errdir -ItemType Directory | Out-Null
            }
            $logmessage = ("���������� ������ �������, ��� ���������� {0}, ����� ���������� � {1}" -f $out.FullName,$errdir)
            $logger.Write($logmessage,"ERROR")
            Move-Item $arjPacket $errdir
        }
        Remove-Item $packageTempDir -Recurse -Force
    }
    Remove-Item $tempdir
}
