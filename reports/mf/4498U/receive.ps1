param (
    $workdir="R:\06_mf\VBK\4498U\IN",
    $emailsFrom="cbmf_info@sovbank.ru",
    $emailsTo="a.kharkovskij@sovbank.ru", #_390pMF@sovbank.ru
    $smtpServer="812-cas01.sovbank.ru",
    $emailsSubject="��������� � ������ ���������� (�������� 4498-�)"
    )

$inFileMask="*.bin"
$messageMask="UV*.xml"

#init block
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$scsign = Join-Path $executingScriptDirectory "SCSign.ps1"
$tempdir=Join-Path $workdir "temp"
$arcdir=Join-Path (Split-Path $workdir -Parent) "arc\IN"
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}
$errdir=Join-Path $workdir "err"
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}

if (-not (Test-Path $arcdir)) {
    New-Item $arcdir -ItemType Directory | Out-Null
}
#init block end

function Extract-IncPackage {
    param(
        [string]$Path
        )
    $arcs=Get-ChildItem $Path -Recurse
    foreach ($arc in $arcs) {
        $arcParentDir=Split-Path $arc.FullName -Parent
        $dst=Join-Path $arcParentDir $arc.BaseName
        if (-not (Test-Path $dst -PathType Container)) {
            New-Item $dst -ItemType Directory -force | Out-Null
        }
        Start-Process $arj32 ("x {0} {1} -jv" -f $arc.FullName,$dst) -NoNewWindow -Wait
        Remove-Item $arc
    }
}

function Send-ToArchive {
    begin{
        $currarcdir = Get-CurArcDir
        $resExistErr=[System.Management.Automation.ErrorCategory]::ResourceExists
        try {
            New-Item $currarcdir -ItemType Directory -ErrorAction Stop | Out-Null
        } catch {
            if ($_.CategoryInfo.Category -ne $resExistErr) {
                $_
                exit
            }
        }
    }
    process{
        $item = Get-Item $_
        if ($item.PSIsContainer) {
                Copy-Item $item $currArcDir -Recurse -Force
            } else {
                &$SmartCopy $item.FullName $currarcdir
            }
    }
}

function Check-Log {
    if (Test-Path $logger.LogPath) {
        $script:lastLogLine=(Get-Content $logger.LogPath).Count
    } else {  
        return $true
    }
    $Errgx=[regex]"CODE=[1-9]\d{0,}"
    $errors=$false
    if (Test-Path $logger.LogPath) {
        $log=Get-Content $logger.LogPath
        $logfile=$logger.LogPath
        if ($lastLogLine -lt $log.Count) {
            $newLines=$log[$lastLogLine..$log.Count]
            $matchs=$Errgx.Matches($newLines)
            if ($matchs.Success) {
                Send-Email "���������� ������ �������, ��� ����������. ����������� � ���-����� $logfile." -HighPriority
                $errors = $true
            }
        }
    }
    return $errors
}

function Send-Email {
    param(
        $msg,
        [switch]$HighPriority
        )
    $priority="Normal"
    if ($HighPriority) {
        $priority="High"
    }
    Send-MailMessage `
-to $emailsTo `
-from $emailsFrom `
-subject $emailsSubject `
-body $msg `
-smtpServer $smtpServer `
-priority $priority `
-encoding Unicode
}

$incs = Get-ChildItem (Join-Path $workdir $inFileMask)
if ($incs) {
    $incs | Send-ToArchive
    if (-not (Test-Path $tempdir)) {
        New-Item $tempdir -ItemType Directory | Out-Null
    }
    $incInTemp = Move-Item $incs $tempdir -PassThru
    Extract-IncPackage (Join-Path $tempdir $inFileMask)
    Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Check -Recurse" -f (Join-Path $tempdir $messageMask),$logger.LogPath)
    Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Unsign -Recurse" -f (Join-Path $tempdir $messageMask),$logger.LogPath)
    if (Check-Log) {
        if (-not (Test-Path $errdir)) {
            New-Item $errdir -ItemType Directory | Out-Null
        }
        Move-Item $tempdir\* $errdir
        $logmessage="���������� ������ ��� ����������� ����� ���������� � $errdir"
        $logger.Write($logmessage,"ERROR")
        Send-Email $logmessage -HighPriority
        Remove-Item $tempdir
        exit
    }
    Get-ChildItem "$tempdir\*.xml" -Recurse | Copy-Item -Destination $workdir
    $logmessage="������ ������ �� 4489-� ���������� � $arcdir"    
    $logger.Write($logmessage)
    Send-Email $logmessage
    Move-Item $tempdir\* (Get-CurArcDir)
    Remove-Item $tempdir
}
