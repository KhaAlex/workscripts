param (
    $workdir="C:\work\exg\311p\fromCFT",
    $senddir="C:\work\exg\311p\toCB",
    $cryptTo="2010",
    $bik="30715"
    )
$reglCode="311-�"
$outsMask="S?C*.xml"
$outsYURFldrName="out_a" # ����� ��� ������ ������� ���� (����� AN)
$outsFIZFldrName="out_b" # ����� ��� ����� ������� ���� (����� BN)

#init block
$resExistErr=[System.Management.Automation.ErrorCategory]::ResourceExists
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$scsign = Join-Path $executingScriptDirectory "SCSignCB.ps1"
$ScSignInit = Join-Path $executingScriptDirectory "ScSignInit.ps1"

# ������ ������� ����������� ����� ��-�� ����, ��� ��������� ��� ������������� ���������
# ����������
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$tempdir=Join-Path (Split-Path $workdir -Parent) "temp"
$arcdir=Join-Path (Split-Path $workdir -Parent) "arc\OUT"
$errdir=Join-Path (Split-Path $workdir -Parent) (Join-Path "err\OUT" (Get-Date -f yyyyMMdd))
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}
#init block end

function Get-PackageNumber {
    $idsFile = Join-Path (Get-CurArcDir) "id.txt"
    [int]$curId = 0
    if (Test-Path $idsFile) {
        [int]$lastId = Get-Content $idsFile -last 1
        $curId = $lastId+1
        Add-Content $idsFile $curId
    } else {
        $curId = 1
        #����� ��������� ������� ������� � return, Out-Null ��������� ��� �������������� ������ ���������
        New-Item $idsFile -ItemType file | Out-Null
        Add-Content $idsFile $curId
    }
    return $curId
}

function Get-YURPacketDirName {
    $packnum = Get-PackageNumber
    if ($packnum -lt 1) {
        throw "�������� ����� �������"
    }
    return "AN{0}{1}{2:D4}" -f $bik,(Get-Date -f yyMMdd),$packnum
}

function Get-FIZPacketDirName {
    $packnum = Get-PackageNumber
    if ($packnum -lt 1) {
        throw "�������� ����� �������"
    }
    return "BN{0}{1}{2:D4}" -f $bik,(Get-Date -f yyMMdd),$packnum
}

function Make-ArjPacket {
    param(
        $arcBaseName,
        $files
        )
    $params="m -c -e -y {0}\{1}.arj {2}" -f $tempdir,$arcBaseName,$files
    Start-Process -FilePath $arj32 -ArgumentList $params -NoNewWindow -Wait
    return (Get-Item "$tempdir\$arcBaseName.arj")
}

# ������� �������� ������� ������� ������ � ����, �� ������� ������
# ������� ���������� ���������� $lastLogLine � �������������
function Check-Log {
    $Errgx=[regex]"CODE=[1-9]\d{0,}"
    $errors=$false
    if (Test-Path $logger.LogPath) {
        $log=Get-Content $logger.LogPath
        $logfile=$logger.LogPath
        if ($lastLogLine -lt $log.Count) {
            $newLines=$log[$lastLogLine..$log.Count]
            $matchs=$Errgx.Matches($newLines)
            if ($matchs.Success) {
                Show-Popup "���������� ������ �������, ��� ����������. ����������� � ���-����� $logfile."
                $errors = $true
            }
        }
    }
    return $errors
}

#functions block end

if (-not (&$ScSignInit)) {
    $logger.Write("�� ������� ���������������� �������������","ERROR")
    exit
}

#script block
$outsYUR = Get-ChildItem (Join-Path $workdir $outsYURFldrName) -Filter $outsMask | Select-Object -First 1999
$outsFIZ = Get-ChildItem (Join-Path $workdir $outsFIZFldrName) -Filter $outsMask | Select-Object -First 1999
$outs = @()+$outsYUR+$outsFIZ
if ($outs) {
    $currArcDir = Get-CurArcDir
    try {
        New-Item $currArcDir -ItemType Directory -ErrorAction stop |Out-Null
    } catch  {
        if ($_.CategoryInfo.Category -ne $resExistErr) {
            $logger.Write("�������������� ������ ��� �������� ����� ������ $currArcDir","ERROR")
            exit
        }
    }

    #$packetName = Get-PacketDirName
    #$packetArcDir = Join-Path $currArcDir $packetName
    $packets=@{}
    if ($outsYUR) {
        $packets.YUR=@{}
        $packets.YUR.Name= Get-YURPacketDirName
        $packets.YUR.ArcPath = Join-Path $currArcDir $packets.YUR.Name
        $packets.YUR.TempDirPath = Join-Path $tempdir $packets.YUR.Name
        $packets.YUR.Files = $outsYUR
        $packets.YUR.ARJSendPath = Join-Path $packets.YUR.TempDirPath $outsMask
    }
    Start-Sleep -Seconds 1
    if ($outsFIZ) {
        $packets.FIZ=@{}
        $packets.FIZ.Name= Get-FIZPacketDirName
        $packets.FIZ.ArcPath = Join-Path $currArcDir $packets.FIZ.Name   
        $packets.FIZ.TempDirPath = Join-Path $tempdir $packets.FIZ.Name
        $packets.FIZ.Files = $outsFIZ
        $packets.FIZ.ARJSendPath = Join-Path $packets.FIZ.TempDirPath $outsMask
    }
    
    foreach ($packet in $packets.GetEnumerator()) {
        $packet
        Write-Host $packet.Value.ArcPath
        try {
            $packet.Value.ArcDirectory = New-Item $packet.Value.ArcPath -ItemType Directory -ErrorAction stop
        } catch  {
            if ($_.CategoryInfo.Category -eq $resExistErr) {
                $logger.Write("���������� ������� � ������ ��� ������� $packetArcDir","WARNING")
            } else {
                $_;exit
            }
        }

        foreach ($file in $packet.Value.Files) {
            Invoke-Expression ("{0} {1} {2}" -f $SmartCopy,$file.FullName,$packet.Value.ArcPath)
        }

        try {
            $packet.Value.TempDirectory = New-Item $packet.Value.TempDirPath -ItemType Directory -Force -ErrorAction stop
        } catch  {
            if ($_.CategoryInfo.Category -ne $resExistErr) {
                $logger.Write("�������������� ������ ��� �������� ���������� ���������� $packetTempDir","ERROR")
                exit
            }
        }

        $packet.Value.Files = Move-Item $packet.Value.Files.FullName $packet.Value.TempDirectory -PassThru

        $logger.Write("������� $($packet.Value.ARJSendPath)")
        Invoke-Expression ("$scsign -Path {0} -Sign -LogPath {1}" -f $packet.Value.ARJSendPath,$logger.LogPath)
        $logger.Write("���������� $($packet.Value.ARJSendPath) �� $cryptTo")
        Invoke-Expression ("$scsign -Path {0} -Crypt {1} -LogPath {2} " -f $packet.Value.ARJSendPath,$cryptTo,$logger.LogPath)

        $packetSend = Make-ArjPacket $packet.Value.Name $packet.Value.ARJSendPath

        Invoke-Expression ("{0} {1} {2}" -f $SmartCopy,$packetSend,(Split-Path $packet.Value.ArcPath -Parent))
        Move-Item $packetSend $senddir
    }
    
    Remove-Item $tempdir -Recurse
    Remove-Item (Join-Path $workdir $outsYURFldrName)
    Remove-Item (Join-Path $workdir $outsFIZFldrName)
}

#script block end
