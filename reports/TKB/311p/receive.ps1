param (
    $workdir="C:\work\exg\311p\fromCB",
    $destdir="C:\work\exg\311p\toCFT",
    $orgcode="715"
    )
$reglCode="311-�"
$PackageMask="2z*.$orgcode"
$arjMasks=@("ON30$orgcode*.ARJ","NN30$orgcode*.ARJ","S30$orgcode*.arj","Y30$orgcode*.ARJ")
$incsMask="*.*"
$uvMask = "UV?N30$orgcode*.xml"

#init block
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$scsign = Join-Path $executingScriptDirectory "SCSignCB.ps1"
$ScSignInit = Join-Path $executingScriptDirectory "ScSignInit.ps1"
$askPopup = New-Object -ComObject Wscript.Shell #����� ��� ������ ������� Show-popup

# ������ ������� ����������� ����� ��-�� ����, ��� ��������� ��� ������������� ���������
# ����������
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$tempdir=Join-Path (Split-Path $workdir -Parent) "temp"
$arcdir=Join-Path (Split-Path $workdir -Parent) "arc\IN"
$errdir=Join-Path (Split-Path $workdir -Parent) (Join-Path "err\IN" (Get-Date -f yyyyMMdd))
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName

$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}
#init block end

#functions block
function Extract-IncPackage {
    param(
        [string]$Path
        )
    $arcs=Get-ChildItem $Path -Recurse
    foreach ($arc in $arcs) {
        $arcParentDir=Split-Path $arc.FullName -Parent
        $dst=Join-Path $arcParentDir $arc.BaseName
        if (-not (Test-Path $dst -PathType Container)) {
            New-Item $dst -ItemType Directory -force | Out-Null
        }

        if ($arc.Extension.Equals(".$orgcode")) {
            Expand -R $arc.FullName $dst
        }
       
        if ($arc.Extension.ToLower().Equals(".arj")) {
            Start-Process $arj32 ("x {0} {1} -jv" -f $arc.FullName,$dst) -NoNewWindow -Wait
        }

        Remove-Item $arc
    }
}

function Parse-UVs {
    param (
        $UVs
    )
    foreach ($UV in $UVs) {
        [xml]$UVxml = Get-Content $UV
        $message = "�������� ����������� � ������ {0}, � ��� ��� ����� {1} �� {2} ��������� �� ��������: {3}" -f `
        $UV.Name,$UVxml.UV.ARH,$UVxml.UV.DATE_ARH,$UVxml.UV.REZ_ARH
        $logger.Write($message)
        $currUvArcDir=Join-Path (Get-CurArcDir) (Get-Item (Split-Path $UV.FullName -Parent)).Name
        if (Test-Path $currUvArcDir) {
            &$SmartCopy $UV $currUvArcDir
            Remove-Item (Split-Path $UV.FullName -Parent) -Recurse -Force
        } else {
            Move-Item (Split-Path $UV.FullName -Parent) (Get-CurArcDir)
        }
        if ($UVxml.UV.REZ_ARH.Trim(" ").Equals("������")) {

            } else {
                Write-Host "������� ����� � ����������!!!`n������� ����� ������� ��� �����������..." -Fore Red
                $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
            }
    }
}

# ������� �������� ������� ������� ������ � ����, �� ������� ������
# ������� ���������� ���������� $lastLogLine � �������������
function Check-Log {
    $Errgx=[regex]"CODE=[1-9]\d{0,}"
    $errors=$false
    if (Test-Path $logger.LogPath) {
        $log=Get-Content $logger.LogPath
        $logfile=$logger.LogPath
        if ($lastLogLine -lt $log.Count) {
            $newLines=$log[$lastLogLine..$log.Count]
            $matchs=$Errgx.Matches($newLines)
            if ($matchs.Success) {
                Show-Popup "���������� ������ �������, ��� ����������. ����������� � ���-����� $logfile."
                $errors = $true
            }
        }
    }
    return $errors
}

#functions block end

if (-not ($ScSignInit)) {
    $logger.Write("�� ������� ���������������� �������������","ERROR")
    exit
}

#script block
$incs = Get-ChildItem (Join-Path $workdir $PackageMask)
if ($incs) {
    $currArcDir = Get-CurArcDir
    
    if (-not (Test-Path $currArcDir)) {
        New-Item $currArcDir -ItemType Directory -force -ErrorAction Stop | Out-Null
    }
    
    foreach ($inc in $incs) {
        Invoke-Expression "$SmartCopy $inc $currArcDir"
    }

    if (-not (Test-Path $tempdir)) {
        New-Item $tempdir -ItemType Directory | Out-Null
    }

    Move-Item $incs $tempdir -ErrorAction Stop
    Extract-IncPackage (Join-Path $tempdir $PackageMask)
    foreach ($arjmask in $arjMasks) {
        Extract-IncPackage (Join-Path $tempdir $arjmask)
    }
    $UVs=Get-ChildItem (Join-Path $tempdir $uvMask) -Recurse
    if ($UVs) {
        Parse-UVs $UVs
    }

    Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Check -Recurse" -f (Join-Path $tempdir $incsMask),$logger.LogPath)
    Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Unsign -Recurse" -f (Join-Path $tempdir $incsMask),$logger.LogPath)
    if (Check-Log) {
            if (-not (Test-Path $errdir)) {
                New-Item $errdir -ItemType Directory
            }
            Move-Item $tempdir\* -Destination $errdir -Force -ErrorAction Stop
            $message="��� �������� �������, ��� ����������� �������� ������, ��������� ��������� ��������� � $errdir"
            $logger.Write($message,"ERROR")
        } else {
            Get-ChildItem (Join-Path $tempdir $incsMask) -Exclude $uvMask -Recurse | &$SmartCopy -Destination $destdir
            $logger.Write("C�������� �� ���������� ������ �� $reglCode ����������. ���������� � ����� ��� ��������� � ���")
            try {
                Move-Item $tempdir\* $currArcDir -Force -ErrorAction Stop
            } catch {
                $logger.Write("�� ������� ����������� ������ �� ��������� ���������� � �����. ���������� � ����� $errdir","ERROR")
                if (-not (Test-Path $errdir)) {
                    New-Item $errdir -ItemType Directory | Out-Null
                }
                Move-Item $tempdir\* -Destination $errdir\
            }
            Remove-Item $tempdir
        }
}