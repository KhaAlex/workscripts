param (
    $workdir="C:\work\exg\440p\fromCFT",
    $senddir="C:\work\exg\440p\toCB",
    $cryptTo="2010",
    $bik="4030715"
    )

#init block
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1"
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
$scsign = Join-Path $executingScriptDirectory "SCSignCB.ps1"
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1"
$ScSignInit = Join-Path $executingScriptDirectory "ScSignInit.ps1"
$tempdir=Join-Path (Split-Path $workdir -Parent) "temp"
$arcdir=Join-Path (Split-Path $workdir -Parent) "arc\OUT"
$errdir=Join-Path (Split-Path $workdir -Parent) (Join-Path "err\OUT" (Get-Date -f yyyyMMdd))
$xmlToVrbMasks=@("BOS*.xml","BNS*.xml","BNP*.xml","BUV*.xml","BZ1*.xml","BV*.xml")
$forSendMask="*.xml"
$vrbFileMask="*.vrb"

function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}
#init block end

if (-not ($ScSignInit)) {
    $logger.Write("�� ������� ���������������� �������������","ERROR")
    exit
}

if (-not (Test-Path $tempdir)) {
    New-Item $tempdir -ItemType Directory | Out-Null
}
if (-not (Test-Path $arcdir)) {
    New-Item $arcdir -ItemType Directory | Out-Null
}

#functions block

function Get-PackageNumber {
    $idsFile = Join-Path (Get-CurArcDir) "id.txt"
    [int]$curId = 0
    if (Test-Path $idsFile) {
        [int]$lastId = Get-Content $idsFile -last 1
        $curId = $lastId+1
        Add-Content $idsFile $curId
    } else {
        $curId = 1
        #����� ��������� ������� ������� � return, Out-Null ��������� ��� �������������� ������ ���������
        New-Item $idsFile -ItemType file | Out-Null
        Add-Content $idsFile $curId
    }
    return $curId
}

function Get-PacketDirName {
    $packnum = Get-PackageNumber
    if ($packnum -lt 1) {
        throw "�������� ����� �������"
    }
    $formattedDate = Get-Date -f yyyyMMdd
    return "AFN_{0}_MIFNS00_{1}_{2:D5}" -f $bik,$formattedDate,$packnum
}

function Copy-MessToArcive {
    param(
        $files
        )
    $currArcDir=Get-CurArcDir
    if (-not (Test-Path $currArcDir -PathType Container)) {
        New-Item $currArcDir -ItemType Directory -force | Out-Null
    }
    # Get-PacketDirName ������� ����� $currArcDir ��� ������
    $packageName = Get-PacketDirName
    $currArcPacketDir=Join-Path $currArcDir $packageName
    if (-not (Test-Path $currArcPacketDir)) {
            New-Item $currArcPacketDir -ItemType Directory | Out-Null
        }
    foreach ($file in $files) {
        $fileName=$file.FullName
        Invoke-Expression "$SmartCopy $fileName $currArcPacketDir"
        #DOTO what if some not copied
    }
    return $packageName
}

function Prepare-ForSend {
    $filesForSend = Get-ChildItem (Join-Path $workdir $forSendMask) | Select-Object -First 1999
    if ($filesForSend) {
        $packetName = Copy-MessToArcive $filesForSend
        Move-Item $filesForSend $tempdir -Force
    }
    return $packetName
}

function Make-ArjPacket {
    param(
        $arcBaseName,
        $files
        )
    $params="m -c -e -y {0}\{1}.arj {2}" -f $tempdir,$arcBaseName,$files
    Start-Process -FilePath $arj32 -ArgumentList $params -NoNewWindow -Wait
    return (Get-Item (Join-Path $tempdir "$arcBaseName.arj"))
}

# ������� �������� ������� ������� ������ � ����, �� ������� ������
# ������� ���������� ���������� $lastLogLine � �������������
function Check-Log {
    $Errgx=[regex]"CODE=[1-9]\d{0,}"
    $errors=$false
    if (Test-Path $logger.LogPath) {
        $log=Get-Content $logger.LogPath
        $logfile=$logger.LogPath
        if ($lastLogLine -lt $log.Count) {
            $newLines=$log[$lastLogLine..$log.Count]
            $matchs=$Errgx.Matches($newLines)
            if ($matchs.Success) {
                Show-Popup "���������� ������ �������, ��� ����������. ����������� � ���-����� $logfile."
                $errors = $true
            }
        }
    }
    return $errors
}

#functions block end

#script block
while (Test-Path (Join-Path $workdir $forSendMask))
{
    $ArjPacketName = Prepare-ForSend
    if ($ArjPacketName) {
        $logger.Write("�������� ��� ������� $ArjPacketName")
    }
    $signPath = Join-Path $tempdir $forSendMask
    Write-Host "������� $signPath" -fore Yellow
    #Invoke-Expression "$verbaScript -Path $signPath -Sign"
    Invoke-Expression ("$scsign -Path {0} -Sign -LogPath {1}" -f $signPath,$logger.LogPath)
    $toVrb=$xmlToVrbMasks| ForEach-Object {Join-Path $tempdir $_}|Get-ChildItem 
    $toVrb|Rename-Item -NewName {$_.BaseName+".VRB"}
    $cryptPath=Join-Path $tempdir "*.vrb"
    $logger.Write("���������� $cryptPath �� $cryptTo")
    #Invoke-Expression "$verbaScript -Path $cryptPath -Crypt $cryptTo"
    Invoke-Expression ("$scsign -Path {0} -Crypt {1} -LogPath {2} " -f $cryptPath,$cryptTo,$logger.LogPath)
    $arjPacket = Make-ArjPacket $ArjPacketName ("{0}\{1} {0}\{2}" -f $tempdir,$vrbFileMask,$forSendMask)
    $signArj = Join-Path $tempdir $arjPacket.Name
    $logger.Write("������� $signArj")
    #Invoke-Expression "$verbaScript -Path $signArj -Sign"
    Invoke-Expression ("$scsign -Path {0} -Sign -LogPath {1}" -f $signArj,$logger.LogPath)
    Invoke-Expression ("{0} {1} {2}" -f $SmartCopy,$signArj,(Get-CurArcDir))
    if (-not (Test-Path $senddir)) {
        Move-Item $signArj $workdir
        $logger.Write("���������� ��� �������� $senddir ����������.`n�������������� ���� �������� � $workdir.","ERROR")
    } elseif (Check-Log) {
        if (-not (Test-Path $errdir)) {
                New-Item $errdir -ItemType Directory
            }
        Move-Item $tempdir\* -Destination $errdir -Force -ErrorAction Stop
            $message="��� �������� �������, ��� ����������� �������� ������, ��������� ��������� ��������� � $errdir"
            $logger.Write($message,"ERROR")
            Show-Popup $message
    } else {
        Move-Item $signArj $senddir
        $logger.Write("���� ��������� �� �������� � $senddir.")
    }
    
}

Remove-Item $tempdir

#script block end
