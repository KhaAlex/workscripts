param (
    $workdir="C:\work\exg\440p\fromCB",
    $destdir="C:\work\exg\440p\toCFT"
    )

#init block
$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$SmartCopy = Join-Path $executingScriptDirectory "SmartCopyFile.ps1" 
$GetLogger = Join-Path $executingScriptDirectory "GetLogger.ps1" 
$arj32 = Join-Path $executingScriptDirectory "ARJ32.EXE"
#$verbaScript = Join-Path $executingScriptDirectory "verbaScr.ps1"
$scsign = Join-Path $executingScriptDirectory "SCSignCB.ps1"
$ScSignInit = Join-Path $executingScriptDirectory "ScSignInit.ps1"
$askPopup = New-Object -ComObject Wscript.Shell

# ������ ������� ����������� ����� ��-�� ����, ��� ��������� ��� ������������� ���������
# ����������
function Get-CurArcDir {
    return Join-Path $arcdir (Get-Date -f yyyy\\MM\\dd)
}

$tempdir=Join-Path (Split-Path $workdir -Parent) "temp"
$arcdir=Join-Path (Split-Path $workdir -Parent) "arc\IN"
$errdir=Join-Path (Split-Path $workdir -Parent) (Join-Path "err\IN" (Get-Date -f yyyyMMdd))
$LogFileName=(Get-Date -f yyyyMMdd)+".log"
$logfile = Join-Path (Get-CurArcDir) $LogFileName
$logger=&$GetLogger $logfile -Screen
$lastLogLine=0
if (Test-Path $logger.LogPath) {
    $lastLogLine=(Get-Content $logger.LogPath).Count
}
$PackageMask="*.715"
$arcFileMask="AFN*.arj"
$vrbFileMask="*.vrb"
$xmlFileMask="*.xml"
$izvFileMask="IZV*.xml"


if (-not ($ScSignInit)) {
    $logger.Write("�� ������� ���������������� �������������","ERROR")
    exit
}
if (-not (Test-Path $tempdir)) {
    New-Item $tempdir -ItemType Directory | Out-Null
}
if (-not (Test-Path $arcdir)) {
    New-Item $arcdir -ItemType Directory | Out-Null
}
#init block end

#functions block

function Show-Popup {
    param(
        $Text="",
        $PopupText="440-�",
        [switch]$Err
        )
    $pic=0x30
    if ($Err) {
        $pic = 0x10
    }
    $askPopup.Popup($Text,0,$PopupText,$pic) | Out-Null
}

# ������� ����� ��������� �������� �������, ������� ������� SmartCopyFile.ps1 � ����������
# �� �������� � ������� �����������
function Copy-ArcsToArchive {
    param(
        $arcs
        )
    $currArcDir=Get-CurArcDir
    if (-not (Test-Path $currArcDir -PathType Container)) {
        New-Item $currArcDir -ItemType Directory -force | Out-Null
    }
    foreach ($arc in $arcs) {
        Invoke-Expression "$SmartCopy $arc $currArcDir"
        #DOTO what if some not copied
    }
}

# ������� ��������� ���������� �� � ������ ����� � ��������� ��� � ��������� �����
# ������������ � Extract-IncPackage
function Check-Arc {
    param (
        $arc
        )
    $currArcDir=Get-CurArcDir
    $CheckinArc=Join-Path $currArcDir $arc.Name
    return (Test-Path $CheckinArc -PathType Leaf)
}

# ������� �������� ������� ������� ������ � ����, �� ������� ������
# ������� ���������� ���������� $lastLogLine � �������������
function Check-Log {
    $Errgx=[regex]"CODE=[1-9]\d{0,}"
    $errors=$false
    if (Test-Path $logger.LogPath) {
        $log=Get-Content $logger.LogPath
        $logfile=$logger.LogPath
        if ($lastLogLine -lt $log.Count) {
            $newLines=$log[$lastLogLine..$log.Count]
            $matchs=$Errgx.Matches($newLines)
            if ($matchs.Success) {
                Show-Popup "���������� ������ �������, ��� ����������. ����������� � ���-����� $logfile."
                $errors = $true
            }
        }
    }
    return $errors
}

# ������� ��������� �������� ���� � ����� � ��� ������
# � ������ -Archive �� ��������� ���������� �� ��������� ������������ �������� �����
# ��� ������������ �������� � �����.
function Extract-IncPackage {
    param(
        [string]$Mask,
        [switch]$Archive
        )
    $arcsPath=Join-Path $tempdir $Mask
    $arcs=Get-ChildItem $arcsPath -Recurse
    foreach ($arc in $arcs) {
        $arcParentDir=Split-Path $arc.FullName -Parent
        $dst=Join-Path $arcParentDir $arc.BaseName
        if (-not (Test-Path $dst -PathType Container)) {
            New-Item $dst -ItemType Directory -force | Out-Null
        }

        if ($arc.Extension.Equals(".715")) {
            Expand -R $arc.FullName $dst
        }
       
        if ($arc.Extension.ToLower().Equals(".arj")) {
            Start-Process $arj32 ("x {0} {1} -jv" -f $arc.FullName,$dst) -NoNewWindow -Wait
        }

        # �������������� ��������, ���� ����� ��������� ����� ��� � ������
        # �� �������� ���� �� ���������
        if ((Check-Arc $arc) -and ($Archive)) {
            Remove-Item $arc
        } elseif (-not $Archive) {
            Remove-Item $arc
        }
    }
}

# ��������� ��������� ������� � �����, � �������� ��������� ����������.
function Prepare-IncPackage {
    $received=$false
    $arcsPath=Join-Path $workdir $PackageMask
    $arcs=Get-ChildItem $arcsPath
    if ($arcs) {
        Copy-ArcsToArchive $arcs
        Move-Item $arcs $tempdir -Force
        $received=$true
    }
    return $received
}

# ������� �������� ���1. ��������� ������� � ������� � ���,
# ���� ������� ������ ��������� �� email
function Parse-Izvs {
    $izvsPath= Join-Path $tempdir $izvFileMask
    $izvs=Get-ChildItem $izvsPath -Recurse
    foreach ($izv in $izvs) {
        $izvName=$izv.Name
        [xml]$izvXML=Get-Content $izv
        $FileNameinIzv=$izvXML.����.����������.��������
        $izvStatus=$izvXML.����.����������.���������
        if ([int]$izvXML.����.����������.�������������� -eq 1) {
            $logger.Write("���� $FileNameinIzv ������. ���1:$izvName")
        } else {
            $logger.Write("���� $FileNameinIzv �� ������. ���1:$izvName. ��������� �������� $izvStatus","WARNING")
        }
        $izvParent=Split-Path $izv.FullName -Parent
        Move-Item $izvParent (Get-CurArcDir) -Force
    }
}

#functions block end

#script block
if (Prepare-IncPackage)
{
    Extract-IncPackage $PackageMask -Archive
    $arcsTempPath= Join-Path $tempdir $arcFileMask
    if (Test-Path $arcsTempPath) {
        Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Check" -f $arcsTempPath,$logger.LogPath)
    }
    Extract-IncPackage $arcFileMask
    $vrbTempPath= Join-Path $tempdir $vrbFileMask
    $vrbFiles=Get-ChildItem $vrbTempPath -Recurse
    if ($vrbFiles) {
        Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Uncrypt -Recurse" -f $vrbTempPath,$logger.LogPath)
        $vrbFiles| Rename-Item -NewName {$_.BaseName+".xml"}
    }
    $xmlTempPath= Join-Path $tempdir $xmlFileMask
    if (Get-ChildItem $xmlTempPath -Recurse) {
        Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Check -Recurse" -f $xmlTempPath,$logger.LogPath)
        Invoke-Expression ("$scsign -Path {0} -LogPath {1} -Unsign -Recurse" -f $xmlTempPath,$logger.LogPath)
    }
    if (Check-Log) {
            if (-not (Test-Path $errdir)) {
                New-Item $errdir -ItemType Directory
            }
            Move-Item $tempdir\* -Destination $errdir -Force -ErrorAction Stop
            $message="��� �������� �������, ��� ����������� �������� ������, ��������� ��������� ��������� � $errdir"
            $logger.Write($message,"ERROR")
        } else {
            Parse-Izvs
            Get-ChildItem "$tempdir\$xmlFilemask" -Recurse | Rename-Item -NewName {$_.BaseName+".xml"} -PassThru | &$SmartCopy -destination $destdir
            if (Test-Path $destdir\$xmlFilemask) {
                $logger.Write("C�������� �� ���������� ������ �� 440-� ����������. ���������� � ����� ��� ��������� � ���")
            }
            try {
                Move-Item $tempdir\* -Destination (Get-CurArcDir) -Force -ErrorAction Stop
            } catch {
                $logger.Write("�� ������� ����������� ������ �� ��������� ���������� � �����. ���������� � ����� err","ERROR")
                if (-not (Test-Path $errdir)) {
                    New-Item $errdir -ItemType Directory | Out-Null
                }
                Move-Item $tempdir\* -Destination $errdir\
            }
            Remove-Item $tempdir
        }
}

#script block end
